console.info('Loading Camera (v4l2).');

const childProcess = require('child_process');
const os = require('os');
const path = require('path');
const ConfigManager = require('modules/config-manager');


function camera(config)
{
  this._name = config.name;
  this._node = Node.init(this._name);
  this._enabled = 0;
  this._config = new ConfigManager(this,
  {
    device: config.device || '/dev/video0',
    port: config.port || 8081,
    path: config.path || '/camera',
    size:
    {
      value: config.size || '1280x720',
      options: [ '352x240', '480x360', '640x480', '858x480', '1024x768', '1280x720', '1920x1080' ]
    },
  });
  this._target = config.server;
  this._ip = config.ip || os.hostname();
  this._running = null;
  this._config.enable();
}

camera.prototype =
{  
  enable: function()
  {
    if (this._enabled++ === 0)
    {
      process.on('exit', () => {
        this._stop();
      });
      this._node.proxy({ service: `${this._target}/add_page` })({ from: this._config.get('path'), to: `http://${this._ip}:${this._config.get('port')}/camera` });
      this._node.unproxy({ service: `${this._target}/add_page` });
      this._start();
    }
    return this;
  },
  
  disable: function()
  {
    if (--this._enabled === 0)
    {
      this._node.proxy({ service: `${this._target}/remove_page` })({ from: this._config.get('path') });
      this._node.unproxy({ service: `${this._target}/remove_page` });
      this._stop();
    }
    return this;
  },

  reconfigure: function(changes)
  {
    const newpath = this._config.get('path');
    const oldpath = changes.path && changes.path.old || newpath;
  
    let stopped = false;
    if (('port' in changes || 'device' in changes || 'size' in changes) && this._enabled)
    {
      this._stop();
      stopped = true;
    }
    
    this._node.proxy({ service: `${this._target}/remove_page` })({ from: oldpath });
    this._node.unproxy({ service: `${this._target}/remove_page` });
    this._node.proxy({ service: `${this._target}/add_page` })({ from: newpath, to: `http://${this._ip}:${this._config.get('port')}/camera` });
    this._node.unproxy({ service: `${this._target}/add_page` });
  
    if (stopped)
    {
      this._start();
    }
  },
  
  _start: function()
  {
    this._stop();

    if (SIMULATOR)
    {
      return;
    }

    this._running = childProcess.spawn(
      `${path.dirname(module.filename)}/camera`,
      [
        this._config.get('device'),
        this._config.get('port'),
        this._config.get('size')
      ],
      {
        stdio: [ 'ignore', 'ignore', 'ignore' ]
      }
    );
  },

  _stop: function()
  {
    if (this._running)
    {
      this._running.kill();
      this._running = null;
    }
  }
}

module.exports = camera;
