#include "server_http.hpp"

class VideoServer {
private:

  SimpleWeb::Server<SimpleWeb::HTTP> server;
  unsigned char* image_data;
  unsigned int image_length;
  unsigned int active = 0;

public:

  void run(int port);
  void set_image(unsigned char* buffer, unsigned int length);
};
