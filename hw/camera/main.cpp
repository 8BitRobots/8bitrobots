#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/v4l2-common.h>
#include <linux/v4l2-controls.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <string.h>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>

#include "video_server.hpp"

using namespace std;

const int MAX_BUFFERS = 3;
static struct {
  v4l2_buffer buffer;
  unsigned char* data;
} buffers[MAX_BUFFERS];

int main(int /*argc*/, char* argv[])
{
  VideoServer server;

  thread server_thread([&server, &argv]() {
    server.run(atoi(argv[2]));
  });

  thread capture_thread([&server, &argv]() {

    int fd = open(argv[1], O_RDWR);
    if (fd < 0) {
      perror("Failed to open video device");
      return 1;
    }

    v4l2_capability capability;
    if (ioctl(fd, VIDIOC_QUERYCAP, &capability) < 0) {
      perror("Failed to get device capabilities, VIDIOC_QUERYCAP");
      return 1;
    }
    if ((capability.capabilities & V4L2_CAP_STREAMING) == 0) {
      perror("Streaming not supported, V4L2_CAP_STREAMING");
      return 1;
    }

    // Extract WIDTHxHEIGHT
    vector<string> sizes;
    boost::split(sizes, argv[3], boost::is_any_of("x"));

    v4l2_format imageFormat;
    imageFormat.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    imageFormat.fmt.pix.width = (unsigned int)atoi(sizes[0].c_str());
    imageFormat.fmt.pix.height = (unsigned int)atoi(sizes[1].c_str());
    imageFormat.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
    imageFormat.fmt.pix.field = V4L2_FIELD_NONE;
    if (ioctl(fd, VIDIOC_S_FMT, &imageFormat) < 0) {
      perror("Device could not set format, VIDIOC_S_FMT");
      return 1;
    }

    v4l2_requestbuffers requestBuffer = {};
    requestBuffer.count = MAX_BUFFERS;
    requestBuffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    requestBuffer.memory = V4L2_MEMORY_MMAP;
    if (ioctl(fd, VIDIOC_REQBUFS, &requestBuffer) < 0) {
      perror("Could not request buffer from device, VIDIOC_REQBUFS");
      return 1;
    }

    for (unsigned int i = 0; i < MAX_BUFFERS; i++) {
      buffers[i].buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
      buffers[i].buffer.memory = V4L2_MEMORY_MMAP;
      buffers[i].buffer.index = i;
      if (ioctl(fd, VIDIOC_QUERYBUF, &buffers[i].buffer) < 0) {
        perror("Device did not return the buffer information, VIDIOC_QUERYBUF");
        return 1;
      }
      buffers[i].data = (unsigned char*)mmap(NULL, buffers[i].buffer.length, PROT_READ|PROT_WRITE, MAP_SHARED, fd, buffers[i].buffer.m.offset);
      if (ioctl(fd, VIDIOC_QBUF, &buffers[i].buffer) < 0) {
        perror("Could not queue buffer, VIDIOC_QBUF");
        return 1;
      }
    }

    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_STREAMON, &type) < 0){
      perror("Could not start streaming, VIDIOC_STREAMON");
      return 1;
    }

    for (;;) {
      for (int i = 0; i < MAX_BUFFERS; i++) {
        if (ioctl(fd, VIDIOC_DQBUF, &buffers[i].buffer) < 0) {
          perror("Could not dequeue the buffer, VIDIOC_DQBUF");
          return 1;
        }

        // Send frame off to be viewed
        server.set_image(buffers[i].data, buffers[i].buffer.bytesused);

        if (ioctl(fd, VIDIOC_QBUF, &buffers[i].buffer) < 0) {
          perror("Could not queue buffer, VIDIOC_QBUF");
          return 1;
        }
      }
    }
  });

  server_thread.join();
  capture_thread.join();
}
