#include <boost/format.hpp>
#include "video_server.hpp"

using namespace std;
using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

const int MJPEG_BUFFER_SIZE = 100 * 1024;
const int MJPEG_BUFFER_OVERFLOW = 100;
const float FRAME_RATE = 60;
const chrono::milliseconds frame_time((int)(1000.0 / FRAME_RATE));

void VideoServer::run(int port)
{
  server.config.port = port;
  image_length = 0;
  image_data = NULL;

  server.resource["^/camera$"]["GET"] = [this](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> /*request*/) {
    thread work_thread([this, response] {
      response->set_send_buffer_size(MJPEG_BUFFER_SIZE);
      class MJPEG {
      public:
        static void send_image(VideoServer* server, const shared_ptr<HttpServer::Response> &response) {
          auto start = chrono::steady_clock::now();
          if (can_send(server, response)) {
            response->write_more("--my_video\r\n");
            response->write_more("Content-Type: image/jpeg\r\n");
            response->write_more(str(boost::format("Content-Length: %1%\r\n\r\n") % (int)server->image_length));
            response->write((const char*)server->image_data, (int)server->image_length);
            response->write_more("\r\n");
          }
          response->send([server, response, start](const SimpleWeb::error_code &ec) {
            if (!ec) {
              auto duration = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - start);
              if (duration < frame_time) {
                this_thread::sleep_for(frame_time - duration);
              }
              send_image(server, response);
            }
            else {
              // Done
              if (server->active > 0)
              {
                server->active--;
              }
            }
          });
        }

        static bool can_send(VideoServer* server, const shared_ptr<HttpServer::Response> &response) {
          static bool need_drain = false;
          int pending = response->get_send_buffer_pending_size();
          if (need_drain) {
            if (pending == 0) {
              need_drain = false;
              return true;
            }
            else {
              return false;
            }
          }
          else {
            if (MJPEG_BUFFER_SIZE - pending - MJPEG_BUFFER_OVERFLOW - (int)server->image_length > 0) {
              return true;
            }
            else {
              need_drain = true;
              return false;
            }
          }
        }
      };
      SimpleWeb::CaseInsensitiveMultimap headers;
      headers.emplace("Cache-Control", "no-cache, must-revalidate");
      headers.emplace("Content-Type", "multipart/x-mixed-replace;boundary=my_video");
      response->write(SimpleWeb::StatusCode::success_ok, headers);
      this->active++;
      MJPEG::send_image(this, response);
    });
    work_thread.detach();
  };

  
  server.resource["^/test$"]["GET"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> /*request*/) {
    response->write("<html><body><img width='1280' height='720' src='/video?" + to_string(rand()) + "'></body></html>");
  };

  server.on_error = [this](shared_ptr<HttpServer::Request> /*request*/, const SimpleWeb::error_code & /*ec*/) {
    // Handle errors here
    if (this->active > 0)
    {
      this->active--;
    }
  };

  server.start();
}

void VideoServer::set_image(unsigned char* buffer, unsigned int length)
{
  image_data = buffer;
  image_length = length;
}
