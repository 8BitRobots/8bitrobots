#! /bin/sh
mkdir -p build
cd build
cmake ..
make -j8
cp camera ..
cd ..
rm -rf build
