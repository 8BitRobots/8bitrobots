#if defined(linux)
extern "C"
{
#include <linux/i2c-dev.h>
}
#else
// Mocks
#define I2C_SLAVE 0
#define clock_nanosleep(A, B, C, D) sleep(1)
#endif
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <memory.h>
#include <uv.h>
#include <node.h>
#include "imu-fusion-madgwick.h"
#include "i2c-lock.h"

using v8::FunctionCallbackInfo;
using v8::Value;
using v8::Isolate;
using v8::Array;
using v8::Number;
using v8::Integer;
using v8::Local;

namespace ImuFusion {

//
// IMU Axis information:
//  X forward, Y to left, Z up
//  Each of these directions is +ve in the forward/left,uo direction
//  Rotation on each axis is clockwise as you look along the axis
//   in the direction above.
//

// Must match imu-fusion.js
#define SENSORS_LSM303DLHC_L3GD20       1
#define SENSORS_LSM9DS1                 2
#define SENSORS_MPU9250                 3

#define LSM303_ADDR_MAG     0x1E
#define LSM303_ADDR_ACCEL   0x19
#define L3GD20_ADDR_GYRO    0x6B

#define LSM9DS1_ADDR_MAG    0x1C
#define LSM9DS1_ADDR_ACCEL  0x6A
#define LSM9DS1_ADDR_GYRO   0x6A

#define MPU6500_ADDR        0x68
#define AK8963_ADDR         0X0C

typedef struct
{
  float x;
  float y;
  float z;
} Event;

typedef struct
{
  uint8_t type;
  uint8_t mag_addr;
  uint8_t accel_addr;
  uint8_t gyro_addr;

  float gyro_zero_offsets[3];
  float gyro_matrix[3][3];
  float mag_offsets[3];
  float mag_softiron_matrix[3][3];
  float accel_offsets[3];
  float accel_matrix[3][3];

  float ak8963_adjust[3];

  uv_mutex_t lock;
  float freq;
  volatile bool active;

  Madgwick filter;
} SensorFusion;

static void run(void* s);

#define MAX_SENSOR_FUSIONS  4
static int nextSensor = 0;
static SensorFusion sensors[MAX_SENSOR_FUSIONS];

static void init_sensors(SensorFusion* sensor);
static void read_magnetic(SensorFusion* sensor, Event* mag_event);
static void read_gyro(SensorFusion* sensor, Event* gyro_event);
static void read_accel(SensorFusion* sensor, Event* accel_event);
static void adjust_sensors(SensorFusion* sensor, Event* gyro_event, Event* mag_event, Event* accel_event);

#define FILTER_BETA                       (sqrtf(3.0f / 4.0f) * M_PI * 40.0f / 180.0f)

#define SENSORS_GRAVITY_STANDARD          9.80665 // Earth's gravity in m/s^2

// ----

#define LSM303_REGISTER_MAG_CRB_REG_M     0x01
#define LSM303_REGISTER_MAG_MR_REG_M      0x02
#define LSM303_REGISTER_MAG_OUT_X_H_M     0x03
#define LSM303_REGISTER_ACCEL_CTRL_REG1_A 0x20
#define LSM303_REGISTER_ACCEL_OUT_X_L_A   0x28

#define L3GD20_REGISTER_GYRO_CTRL_REG1    0x20
#define L3GD20_REGISTER_GYRO_CTRL_REG4    0x23
#define L3GD20_REGISTER_GYRO_OUT_X_L      0x28

#define LSM303_MAGGAIN_1_3                0x20  // +/- 1.3
#define LSM303_ACCEL_MAG_LSB              0.001
#define LSM303_MAG_MICROTESLA_LSB_XY      0.091
#define LSM303_MAG_MICROTESLA_LSB_Z       0.102
#define L3GD20_GYRO_SENSITIVITY_250DPS    0.00875

// ----

#define LSM9DS1_REGISTER_MAG_OUT_X_L      0x28
#define LSM9DS1_REGISTER_GYRO_OUT_X_L     0x18
#define LSM9DS1_REGISTER_ACCEL_OUT_X_L    0x28

#define LSM9DS1_REGISTER_CTRL_REG8        0x22
#define LSM9DS1_REGISTER_CTRL_REG1_G      0x10
#define LSM9DS1_REGISTER_CTRL_REG5_A      0x1F
#define LSM9DS1_REGISTER_CTRL_REG6_A      0x20
#define LSM9DS1_REGISTER_CTRL_REG1_M      0x20
#define LSM9DS1_REGISTER_CTRL_REG2_M      0x21
#define LSM9DS1_REGISTER_CTRL_REG3_M      0x22
#define LSM9DS1_REGISTER_CTRL_REG4_M      0x23

#define LSM9DS1_MAG_MICROTESLA_LSB        0.014 // 4gauss/400uT mode
#define LSM9DS1_GYRO_SENSITIVITY_245DPS   0.00875 // 245dps
#define LSM9DS1_ACCEL_MAG_LSB             0.000061 // 2g

// -----

#define MPU9250_SMPLRT_DIV                0x19
#define MPU9250_CONFIG                    0x1A
#define MPU9250_GYRO_CONFIG               0x1B
#define MPU9250_ACCEL_CONFIG              0x1C
#define MPU9250_ACCEL_CONFIG2             0x1D
#define MPU9250_I2C_MST_CTRL              0x24
#define MPU9250_INT_PIN_CFG               0x37
#define MPU9250_ACCEL_XOUT_H              0x3B
#define MPU9250_GYRO_XOUT_H               0x43
#define MPU9250_PWR_MGMT_1                0x6B

#define AK8963_ST1                        0x02
#define AK8963_XOUT_L                     0x03
#define AK8963_CNTL                       0x0A
#define AK8963_ASAX                       0x10

#define MPU9250_ACCEL_MAG_LSB             (  2.0 / 32768.0) // 2g mode
#define MPU9250_GYRO_SENSITIVITY_250DPS   (250.0 / 32768.0)
#define AK8963_MAG_MICROTESLA_LSB         0.15

// ----

static int i2c = -1;

void JS_start(const FunctionCallbackInfo<Value>& args)
{
  static float vec3zero[3] = { 0, 0, 0 };
  static float mat9ident[3][3] = {
    { 1, 0, 0 },
    { 0, 1, 0 },
    { 0, 0, 1 }
  };

  if (i2c == -1)
  {
    i2c = open("/dev/i2c-1", O_RDWR);
#if defined(linux)
    assert(i2c != -1);
#endif
  }

  int type = args[0].As<Integer>()->Value();
  int mag = args[1].As<Integer>()->Value();
  int accel = args[2].As<Integer>()->Value();
  int gyro = args[3].As<Integer>()->Value();
  float freq = (float)args[4].As<Number>()->Value();

  int id = nextSensor++;
  if (id >= MAX_SENSOR_FUSIONS)
  {
    id = -1;
  }
  else
  {
    SensorFusion* sensor = &sensors[id];

    sensor->type = type;
    sensor->mag_addr = mag;
    sensor->accel_addr = accel;
    sensor->gyro_addr = gyro;

    uv_mutex_init(&sensor->lock);

    sensor->filter.begin(freq, FILTER_BETA);

    memcpy(sensor->gyro_zero_offsets, vec3zero, sizeof(vec3zero));
    memcpy(sensor->gyro_matrix, mat9ident, sizeof(mat9ident));
    memcpy(sensor->mag_offsets, vec3zero, sizeof(vec3zero));
    memcpy(sensor->mag_softiron_matrix, mat9ident, sizeof(mat9ident));
    memcpy(sensor->accel_offsets, vec3zero, sizeof(vec3zero));
    memcpy(sensor->accel_matrix, mat9ident, sizeof(mat9ident));

    init_sensors(sensor);

    sensor->active = true;
    sensor->freq = freq;
    uv_thread_t tid;
    uv_thread_create(&tid, &run, sensor);
  }
  args.GetReturnValue().Set(Number::New(args.GetIsolate(), id));
}

void JS_stop(const FunctionCallbackInfo<Value>& args)
{
  int id = args[0].As<Integer>()->Value();
  SensorFusion* sensor = &sensors[id];
  sensor->active = false;
}

static void run(void* s)
{
  SensorFusion* sensor = (SensorFusion*)s;

  Event mag_event = {};
  Event gyro_event = {};
  Event accel_event = {};

  long delay = (long)(1000000000.0 / sensor->freq);
  struct timespec deadline = {};
  clock_gettime(CLOCK_MONOTONIC, &deadline);

  while (sensor->active)
  {
    uv_mutex_lock(&sensor->lock);

    read_gyro(sensor, &gyro_event);
    read_magnetic(sensor, &mag_event);
    read_accel(sensor, &accel_event);

    adjust_sensors(sensor, &gyro_event, &mag_event, &accel_event);

    sensor->filter.update(
      gyro_event.x, gyro_event.y, gyro_event.z,
      accel_event.x, accel_event.y, accel_event.z,
      mag_event.x, mag_event.y, mag_event.z
    );

    uv_mutex_unlock(&sensor->lock);

    deadline.tv_nsec += delay;
    if (deadline.tv_nsec >= 1000000000)
    {
      deadline.tv_nsec -= 1000000000;
      deadline.tv_sec++;
    }
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &deadline, NULL);
  }
}

void JS_getRaw(const FunctionCallbackInfo<Value>& args)
{
  Event mag_event = {};
  Event gyro_event = {};
  Event accel_event = {};

  int id = args[0].As<Integer>()->Value();
  SensorFusion* sensor = &sensors[id];

  read_gyro(sensor, &gyro_event);
  read_magnetic(sensor, &mag_event);
  read_accel(sensor, &accel_event);

  Isolate* isolate = args.GetIsolate();
  Local<Array> results = Array::New(isolate, 9);
  results->Set(0, Number::New(isolate, gyro_event.x));
  results->Set(1, Number::New(isolate, gyro_event.y));
  results->Set(2, Number::New(isolate, gyro_event.z));
  results->Set(3, Number::New(isolate, mag_event.x));
  results->Set(4, Number::New(isolate, mag_event.y));
  results->Set(5, Number::New(isolate, mag_event.z));
  results->Set(6, Number::New(isolate, accel_event.x));
  results->Set(7, Number::New(isolate, accel_event.y));
  results->Set(8, Number::New(isolate, accel_event.z));

  args.GetReturnValue().Set(results);
}

void JS_getAdjusted(const FunctionCallbackInfo<Value>& args)
{
  Event mag_event = {};
  Event gyro_event = {};
  Event accel_event = {};

  int id = args[0].As<Integer>()->Value();
  SensorFusion* sensor = &sensors[id];

  read_gyro(sensor, &gyro_event);
  read_magnetic(sensor, &mag_event);
  read_accel(sensor, &accel_event);

  adjust_sensors(sensor, &gyro_event, &mag_event, &accel_event);

  Isolate* isolate = args.GetIsolate();
  Local<Array> results = Array::New(isolate, 9);
  results->Set(0, Number::New(isolate, gyro_event.x));
  results->Set(1, Number::New(isolate, gyro_event.y));
  results->Set(2, Number::New(isolate, gyro_event.z));
  results->Set(3, Number::New(isolate, mag_event.x));
  results->Set(4, Number::New(isolate, mag_event.y));
  results->Set(5, Number::New(isolate, mag_event.z));
  results->Set(6, Number::New(isolate, accel_event.x));
  results->Set(7, Number::New(isolate, accel_event.y));
  results->Set(8, Number::New(isolate, accel_event.z));

  args.GetReturnValue().Set(results);
}

void JS_getQuaternion(const FunctionCallbackInfo<Value>& args)
{
  int id = args[0].As<Integer>()->Value();
  SensorFusion* sensor = &sensors[id];

  uv_mutex_lock(&sensor->lock);

  float qw, qx, qy, qz;
  sensor->filter.getQuaternion(&qw, &qx, &qy, &qz);

//float yaw = sensor->filter.getYaw();
//float roll = sensor->filter.getRoll();
//float pitch = sensor->filter.getPitch();
  uv_mutex_unlock(&sensor->lock);
//printf("p %f r %f y %f\n", pitch, roll, yaw);
//printf("w %f x %f y %f z %f\n", qw, qx, qy, qz);

  Isolate* isolate = args.GetIsolate();
  Local<Array> results = Array::New(isolate, 4);
  results->Set(0, Number::New(isolate, qw));
  results->Set(1, Number::New(isolate, qx));
  results->Set(2, Number::New(isolate, qy));
  results->Set(3, Number::New(isolate, qz));

  args.GetReturnValue().Set(results);
}

void JS_setBias(const FunctionCallbackInfo<Value>& args)
{
  int id = args[0].As<Integer>()->Value();
  SensorFusion* sensor = &sensors[id];
  Local<Array> offsets = Local<Array>::Cast(args[2]);
  Local<Array> matrix = Local<Array>::Cast(args[3]);
  switch (args[1].As<Integer>()->Value())
  {
    case 0: // Gryo
      for (int i = 0; i < 3; i++)
      {
        sensor->gyro_zero_offsets[i] = (float)offsets->Get(i).As<Number>()->Value();
      }
      for (int i = 0; i < 9; i++)
      {
        sensor->gyro_matrix[i % 3][i / 3] = (float)matrix->Get(i).As<Number>()->Value();
      }
      break;
    case 1: // Mag
      for (int i = 0; i < 3; i++)
      {
        sensor->mag_offsets[i] = (float)offsets->Get(i).As<Number>()->Value();
      }
      for (int i = 0; i < 9; i++)
      {
        sensor->mag_softiron_matrix[i % 3][i / 3] = (float)matrix->Get(i).As<Number>()->Value();
      }
      break;
    case 2: // Accel
      for (int i = 0; i < 3; i++)
      {
        sensor->accel_offsets[i] = (float)offsets->Get(i).As<Number>()->Value();
      }
      for (int i = 0; i < 9; i++)
      {
        sensor->accel_matrix[i % 3][i / 3] = (float)matrix->Get(i).As<Number>()->Value();
      }
      break;
    default:
      break;
  }
}

static void init_sensors(SensorFusion* sensor)
{
  I2CLock::lock();
  switch (sensor->type)
  {
    case SENSORS_LSM303DLHC_L3GD20:
    {
      // Enable the magnetometer
      ioctl(i2c, I2C_SLAVE, sensor->mag_addr);
      uint8_t cmd0[] = { LSM303_REGISTER_MAG_MR_REG_M, 0x00 };
      write(i2c, cmd0, sizeof(cmd0));
      // Set gain
      uint8_t cmd1[] = { LSM303_REGISTER_MAG_CRB_REG_M, LSM303_MAGGAIN_1_3 };
      write(i2c, cmd1, sizeof(cmd1));

      // Enable the accelerometer (100Hz)
      ioctl(i2c, I2C_SLAVE, sensor->accel_addr);
      uint8_t cmd2[] = { LSM303_REGISTER_ACCEL_CTRL_REG1_A, 0x57 };
      write(i2c, cmd2, sizeof(cmd2));

      // Enable the gyro
      ioctl(i2c, I2C_SLAVE, sensor->gyro_addr);
      uint8_t cmd3[] = { L3GD20_REGISTER_GYRO_CTRL_REG1, 0x00 };
      write(i2c, cmd3, sizeof(cmd3));
      uint8_t cmd4[] = { L3GD20_REGISTER_GYRO_CTRL_REG1, 0x0F };
      write(i2c, cmd4, sizeof(cmd4));
      uint8_t cmd5[] = { L3GD20_REGISTER_GYRO_CTRL_REG4, 0x00 }; // L3GD20_GYRO_RANGE_250DPS
      write(i2c, cmd5, sizeof(cmd5));
      break;
    }

    case SENSORS_LSM9DS1:
    {
      // Reset
      ioctl(i2c, I2C_SLAVE, sensor->gyro_addr);
      uint8_t cmd0[] = { LSM9DS1_REGISTER_CTRL_REG8, 0x05 };
      write(i2c, cmd0, sizeof(cmd0));
      ioctl(i2c, I2C_SLAVE, sensor->mag_addr);
      uint8_t cmd1[] = { LSM9DS1_REGISTER_CTRL_REG2_M, 0x0C };
      write(i2c, cmd1, sizeof(cmd1));

      usleep(10);

      ioctl(i2c, I2C_SLAVE, sensor->gyro_addr);
      uint8_t cmd2[] = { LSM9DS1_REGISTER_CTRL_REG1_G, 0xC0 | 0x00 }; // 245dps mode
      write(i2c, cmd2, sizeof(cmd2));
      uint8_t cmd3[] = { LSM9DS1_REGISTER_CTRL_REG5_A, 0x38 };
      write(i2c, cmd3, sizeof(cmd3));
      uint8_t cmd4[] = { LSM9DS1_REGISTER_CTRL_REG6_A, 0xC0 | 0x00 }; // 2g mode
      write(i2c, cmd4, sizeof(cmd4));

      ioctl(i2c, I2C_SLAVE, sensor->mag_addr);
      uint8_t cmd5[] = { LSM9DS1_REGISTER_CTRL_REG3_M, 0x00 };
      write(i2c, cmd5, sizeof(cmd5));
      uint8_t cmd6[] = { LSM9DS1_REGISTER_CTRL_REG2_M, 0x00 }; // 4gauss mode
      write(i2c, cmd6, sizeof(cmd6));
      uint8_t cmd7[] = { LSM9DS1_REGISTER_CTRL_REG1_M, 0xFC }; // ultrahigh-performance mode
      write(i2c, cmd7, sizeof(cmd7));
      uint8_t cmd8[] = { LSM9DS1_REGISTER_CTRL_REG4_M, 0x0C }; // ultrahigh-performance Z mode
      write(i2c, cmd8, sizeof(cmd8));
      break;
    }

    case SENSORS_MPU9250:
    {
      // Enable the accelerometer and gyro 
      ioctl(i2c, I2C_SLAVE, sensor->gyro_addr); // (gyro_addr == accel_addr)
      uint8_t cmd0[] = { MPU9250_PWR_MGMT_1, 0x80 }; // Reset
      write(i2c, cmd0, sizeof(cmd0));
      usleep(100);
      uint8_t cmd1[] = { MPU9250_PWR_MGMT_1, 0x00 }; // Idle
      write(i2c, cmd1, sizeof(cmd1));
      usleep(100);
      uint8_t cmd2[] = { MPU9250_PWR_MGMT_1, 0x01 }; // Clock == XGYRO
      write(i2c, cmd2, sizeof(cmd2));
      usleep(200);

      uint8_t cmd3[] = { MPU9250_CONFIG, 0x03 }; //
      write(i2c, cmd3, sizeof(cmd3));
      uint8_t cmd4[] = { MPU9250_SMPLRT_DIV, 0x04 }; // SAMPLE_RATE = Internal_Sample_Rate / (1 + SMPLRT_DIV)
      write(i2c, cmd4, sizeof(cmd4));

      uint8_t cmd5[] = { MPU9250_GYRO_CONFIG, 0x00 }; // +250dps
      write(i2c, cmd5, sizeof(cmd5));

      uint8_t cmd6[] = { MPU9250_ACCEL_CONFIG, 0x00 }; // 2G
      write(i2c, cmd6, sizeof(cmd6));
      uint8_t cmd7[] = { MPU9250_ACCEL_CONFIG2, 0x03 }; // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
      write(i2c, cmd7, sizeof(cmd7));

      usleep(100);

      ioctl(i2c, I2C_SLAVE, sensor->gyro_addr); // (gyro_addr == accel_addr)
      uint8_t cmd10[] = { MPU9250_I2C_MST_CTRL , 0x00 }; // Disable I2C master
      write(i2c, cmd10, sizeof(cmd10));
      uint8_t cmd11[] = { MPU9250_INT_PIN_CFG };
      write(i2c, cmd11, sizeof(cmd11));
      uint8_t c = 0;
      read(i2c, &c, sizeof(c));
      uint8_t cmd12[] = { MPU9250_INT_PIN_CFG, (uint8_t)(c | 0x02) }; // I2C master bypass mode
      write(i2c, cmd12, sizeof(cmd12));

      // Enable magnetometer
      ioctl(i2c, I2C_SLAVE, sensor->mag_addr); // (gyro_addr == accel_addr)
      uint8_t cmd13[] = { AK8963_CNTL, 0x00 }; // Reset
      write(i2c, cmd13, sizeof(cmd13));
      usleep(10);

      uint8_t cmd14[] = { AK8963_CNTL, 0x0F }; // ROM mode
      write(i2c, cmd14, sizeof(cmd14));
      uint8_t cmd15[] = { AK8963_ASAX }; // Read ROM
      uint8_t adjust[3];
      write(i2c, cmd15, sizeof(cmd15));
      read(i2c, adjust, sizeof(adjust));
      sensors->ak8963_adjust[0] = (float)((int8_t)adjust[0] - 128) / 256.0 + 1.0;
      sensors->ak8963_adjust[1] = (float)((int8_t)adjust[1] - 128) / 256.0 + 1.0;
      sensors->ak8963_adjust[2] = (float)((int8_t)adjust[2] - 128) / 256.0 + 1.0;

      write(i2c, cmd13, sizeof(cmd13)); // Reset again
      usleep(10);
      uint8_t cmd16[] = { AK8963_CNTL, 0x16 }; // 16 bits, 100Hz
      write(i2c, cmd16, sizeof(cmd16));
      usleep(10);
      break;
    }

    default:
      break;
  }
  I2CLock::unlock();
}

static void adjust_sensors(SensorFusion* sensor, Event* gyro_event, Event* mag_event, Event* accel_event)
{
  // Apply mag offset compensation (base values in uTesla)
  float mx = mag_event->x - sensor->mag_offsets[0];
  float my = mag_event->y - sensor->mag_offsets[1];
  float mz = mag_event->z - sensor->mag_offsets[2];

  // Apply mag soft iron error compensation
  float mmx = mx * sensor->mag_softiron_matrix[0][0] + my * sensor->mag_softiron_matrix[0][1] + mz * sensor->mag_softiron_matrix[0][2];
  float mmy = mx * sensor->mag_softiron_matrix[1][0] + my * sensor->mag_softiron_matrix[1][1] + mz * sensor->mag_softiron_matrix[1][2];
  float mmz = mx * sensor->mag_softiron_matrix[2][0] + my * sensor->mag_softiron_matrix[2][1] + mz * sensor->mag_softiron_matrix[2][2];

  // Apply gyro zero-rate error compensation
  float gx = gyro_event->x - sensor->gyro_zero_offsets[0];
  float gy = gyro_event->y - sensor->gyro_zero_offsets[1];
  float gz = gyro_event->z - sensor->gyro_zero_offsets[2];

  float mgx = gx * sensor->gyro_matrix[0][0] + gy * sensor->gyro_matrix[0][1] + gz * sensor->gyro_matrix[0][2];
  float mgy = gx * sensor->gyro_matrix[1][0] + gy * sensor->gyro_matrix[1][1] + gz * sensor->gyro_matrix[1][2];
  float mgz = gx * sensor->gyro_matrix[2][0] + gy * sensor->gyro_matrix[2][1] + gz * sensor->gyro_matrix[2][2];

  float ax = accel_event->x - sensor->accel_offsets[0];
  float ay = accel_event->y - sensor->accel_offsets[1];
  float az = accel_event->z - sensor->accel_offsets[2];

  float max = ax * sensor->accel_matrix[0][0] + ay * sensor->accel_matrix[0][1] + az * sensor->accel_matrix[0][2];
  float may = ax * sensor->accel_matrix[1][0] + ay * sensor->accel_matrix[1][1] + az * sensor->accel_matrix[1][2];
  float maz = ax * sensor->accel_matrix[2][0] + ay * sensor->accel_matrix[2][1] + az * sensor->accel_matrix[2][2];

  mag_event->x = mmx;
  mag_event->y = mmy;
  mag_event->z = mmz;

  gyro_event->x = mgx;
  gyro_event->y = mgy;
  gyro_event->z = mgz;

  accel_event->x = max;
  accel_event->y = may;
  accel_event->z = maz;
}

static void read_magnetic(SensorFusion* sensor, Event* mag_event)
{
  I2CLock::lock();
  switch (sensor->type)
  {
    case SENSORS_LSM303DLHC_L3GD20:
    {
      ioctl(i2c, I2C_SLAVE, sensor->mag_addr);
      uint8_t cmd[] = { LSM303_REGISTER_MAG_OUT_X_H_M };
      uint8_t data[6];
      write(i2c, cmd, sizeof(cmd));
      read(i2c, data, sizeof(data));
      mag_event->x = (float)(int16_t)(data[1] | (data[0] << 8)) * LSM303_MAG_MICROTESLA_LSB_XY;
      mag_event->y = (float)(int16_t)(data[5] | (data[4] << 8)) * LSM303_MAG_MICROTESLA_LSB_XY;
      mag_event->z = (float)(int16_t)(data[3] | (data[2] << 8)) * LSM303_MAG_MICROTESLA_LSB_Z;
      break;
    }

    case SENSORS_LSM9DS1:
    {
      ioctl(i2c, I2C_SLAVE, sensor->mag_addr);
      uint8_t cmd[] = { LSM9DS1_REGISTER_MAG_OUT_X_L };
      uint8_t data[6];
      write(i2c, cmd, sizeof(cmd));
      read(i2c, data, sizeof(data));
      mag_event->x = -(float)(int16_t)(data[0] | (data[1] << 8)) * LSM9DS1_MAG_MICROTESLA_LSB;
      mag_event->y = -(float)(int16_t)(data[2] | (data[3] << 8)) * LSM9DS1_MAG_MICROTESLA_LSB;
      mag_event->z = (float)(int16_t)(data[4] | (data[5] << 8)) * LSM9DS1_MAG_MICROTESLA_LSB;
      break;
    }

    case SENSORS_MPU9250:
    {
      ioctl(i2c, I2C_SLAVE, sensor->mag_addr);
      uint8_t cmd1[] = { AK8963_XOUT_L };
      uint8_t data[7];
      write(i2c, cmd1, sizeof(cmd1));
      read(i2c, data, sizeof(data));
      mag_event->y = (float)(int16_t)(data[0] | (data[1] << 8)) * sensors->ak8963_adjust[0] * AK8963_MAG_MICROTESLA_LSB;
      mag_event->x = (float)(int16_t)(data[2] | (data[3] << 8)) * sensors->ak8963_adjust[1] * AK8963_MAG_MICROTESLA_LSB;
      mag_event->z = -(float)(int16_t)(data[4] | (data[5] << 8)) * sensors->ak8963_adjust[2] * AK8963_MAG_MICROTESLA_LSB;
      break;
    }

    default:
      break;
  }
  I2CLock::unlock();
}

static void read_gyro(SensorFusion* sensor, Event* gyro_event)
{
  I2CLock::lock();
  switch (sensor->type)
  {
    case SENSORS_LSM303DLHC_L3GD20:
    {
      ioctl(i2c, I2C_SLAVE, sensor->gyro_addr);
      uint8_t cmd[] = { L3GD20_REGISTER_GYRO_OUT_X_L | 0x80 };
      uint8_t data[6];
      write(i2c, cmd, sizeof(cmd));
      read(i2c, data, sizeof(data));
      gyro_event->x = (float)(int16_t)(data[0] | (data[1] << 8)) * L3GD20_GYRO_SENSITIVITY_250DPS;
      gyro_event->y = (float)(int16_t)(data[2] | (data[3] << 8)) * L3GD20_GYRO_SENSITIVITY_250DPS;
      gyro_event->z = (float)(int16_t)(data[4] | (data[5] << 8)) * L3GD20_GYRO_SENSITIVITY_250DPS;
      break;
    }

    case SENSORS_LSM9DS1:
    {
      ioctl(i2c, I2C_SLAVE, sensor->gyro_addr);
      uint8_t cmd[] = { LSM9DS1_REGISTER_GYRO_OUT_X_L };
      uint8_t data[6];
      write(i2c, cmd, sizeof(cmd));
      read(i2c, data, sizeof(data));
      gyro_event->x = (float)(int16_t)(data[0] | (data[1] << 8)) * LSM9DS1_GYRO_SENSITIVITY_245DPS;
      gyro_event->y = -(float)(int16_t)(data[2] | (data[3] << 8)) * LSM9DS1_GYRO_SENSITIVITY_245DPS;
      gyro_event->z = (float)(int16_t)(data[4] | (data[5] << 8)) * LSM9DS1_GYRO_SENSITIVITY_245DPS;
      break;
    }

    case SENSORS_MPU9250:
    {
      ioctl(i2c, I2C_SLAVE, sensor->gyro_addr);
      uint8_t cmd[] = { MPU9250_GYRO_XOUT_H };
      uint8_t data[6];
      write(i2c, cmd, sizeof(cmd));
      read(i2c, data, sizeof(data));
      gyro_event->x = (float)(int16_t)(data[1] | (data[0] << 8)) * MPU9250_GYRO_SENSITIVITY_250DPS;
      gyro_event->y = (float)(int16_t)(data[3] | (data[2] << 8)) * MPU9250_GYRO_SENSITIVITY_250DPS;
      gyro_event->z = (float)(int16_t)(data[5] | (data[4] << 8)) * MPU9250_GYRO_SENSITIVITY_250DPS;
      break;
    }

    default:
      break;
  }
  I2CLock::unlock();
}

static void read_accel(SensorFusion* sensor, Event* accel_event)
{
  I2CLock::lock();
  switch (sensor->type)
  {
    case SENSORS_LSM303DLHC_L3GD20:
    {
      ioctl(i2c, I2C_SLAVE, sensor->accel_addr);
      uint8_t cmd[] = { LSM303_REGISTER_ACCEL_OUT_X_L_A | 0x80 };
      uint8_t data[6];
      write(i2c, cmd, sizeof(cmd));
      read(i2c, data, sizeof(data));
      accel_event->x = (float)((int16_t)(data[0] | (data[1] << 8)) >> 4) * LSM303_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      accel_event->y = (float)((int16_t)(data[2] | (data[3] << 8)) >> 4) * LSM303_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      accel_event->z = (float)((int16_t)(data[4] | (data[5] << 8)) >> 4) * LSM303_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      break;
    }

    case SENSORS_LSM9DS1:
    {
      ioctl(i2c, I2C_SLAVE, sensor->accel_addr);
      uint8_t cmd[] = { LSM9DS1_REGISTER_ACCEL_OUT_X_L };
      uint8_t data[6];
      write(i2c, cmd, sizeof(cmd));
      read(i2c, data, sizeof(data));
      accel_event->x = (float)(int16_t)(data[0] | (data[1] << 8)) * LSM9DS1_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      accel_event->y = -(float)(int16_t)(data[2] | (data[3] << 8)) * LSM9DS1_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      accel_event->z = (float)(int16_t)(data[4] | (data[5] << 8)) * LSM9DS1_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      break;
    }

    case SENSORS_MPU9250:
    {
      ioctl(i2c, I2C_SLAVE, sensor->accel_addr);
      uint8_t cmd[] = { MPU9250_ACCEL_XOUT_H };
      uint8_t data[6];
      write(i2c, cmd, sizeof(cmd));
      read(i2c, data, sizeof(data));
      accel_event->x = (float)(int16_t)(data[1] | (data[0] << 8)) * MPU9250_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      accel_event->y = (float)(int16_t)(data[3] | (data[2] << 8)) * MPU9250_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      accel_event->z = (float)(int16_t)(data[5] | (data[4] << 8)) * MPU9250_ACCEL_MAG_LSB * SENSORS_GRAVITY_STANDARD;
      break;
    }

    default:
      break;
  }
  I2CLock::unlock();
}

}
