{
    "targets":
    [
        {
            "target_name": "boardNative",
            "sources":
            [
                "i2c-lock.cc",
                "i2c-pwm-pca9685.cc",
                "imu-fusion.cc",
                "imu-fusion-madgwick.cc",
                "imu-fusion-calibration.cc",
                "export.cc"
            ]
        }
    ]
}
