console.info('Loading Software Fusion IMU sensors.');

const FS = require('fs');
const MATH = require('mathjs');
const native = require('.');
const ConfigManager = require('modules/config-manager');

const SERVICE_RESET = { service: 'reset', schema: {} };

const TOPIC_ORIENTATION = { topic: 'orientation', schema: { confidence: 'Number', x: 'Number', y: 'Number', z: 'Number', w: 'Number' } };
const TOPIC_CALIBRATION = { topic: 'calibration', schema: { confidence: 'Number', old: 'Hash', new: 'Hash' } };

const BIAS_GYRO  = 0;
const BIAS_MAG   = 1;
const BIAS_ACCEL = 2;

const TICK_FREQ = 200; // Number of time per second IMU is updated - Hz;
const TICK_FAST_CALIBRATION = 50; // Number of times per second calibration data is updated - Hz
const TICK_SLOW_CALIBRATION = 5; // Number of times per second calibration data is updated - Hz
const TICK_QUATERNION = 10; // Number of time per second we update the quaterion - Hz
const TICK_BIASES = 50; // Number of calibration ticks between biases updates.

const THETA_BUCKETS  = 16;
const PHI_BUCKETS    = 8;
const NR_SAMPLES     = 256;
const NR_BUCKETS = THETA_BUCKETS * PHI_BUCKETS;
const MAX_BUCKET_SIZE = Math.floor(NR_SAMPLES / NR_BUCKETS); // Samples per bucket (gyro, mag, accel)
const MIN_BUCKET_SIZE = Math.floor(MAX_BUCKET_SIZE / 2); // Samples per bucket before it is considered 'good'
const DEFAULT_OFFSETS = [ 0, 0, 0 ];
const IDENTITY_MATRIX = [ 1, 0, 0, 0, 1, 0, 0, 0, 1 ];

function imu(config)
{
  this._name = config.name;
  this._node = Node.init(config.name);
  this._enabled = 0;
  this._config = new ConfigManager(this,
  {
    friendlyName: '',
    magneticNorm: 48.224,
    accelNorm: 9.80665,
    quaternionHz: TICK_QUATERNION
  });
  this._id = 0;
  this._type = config.type;
  this._i2c = config.i2c;
  this._rebias = 0;
  this._resetAll = config.reset || false;
  this._resetGyro = config.resetGyro || false;
  this._resetMag = config.resetMag || false;
  this._resetAccel = config.resetAccel || false;
  
  this._buckets =
  {
    gyr: [],
    mag: new Array(NR_BUCKETS).fill().map(() => []),
    acc: new Array(NR_BUCKETS).fill().map(() => [])
  };

  this._biases =
  {
    gyr: [ DEFAULT_OFFSETS, IDENTITY_MATRIX ],
    mag: [ DEFAULT_OFFSETS, IDENTITY_MATRIX ],
    acc: [ DEFAULT_OFFSETS, IDENTITY_MATRIX ],
  };

  this._calibration =
  {
    mag: null,
    acc: null,
    gyr: null,
    sys: null
  };

  this._config.enable();
}

// Must match imu-fusion.cc
imu.LSM303DLHC_L3GD20 = 1;
imu.LSM9DS1 = 2;
imu.MPU9250 = 3;

imu.prototype =
{
  enable: function()
  {
    if (this._enabled++ === 0)
    {
      this._enable();
    }
    return this;
  },

  _enable: function()
  {
    const friendlyName = this._config.get('friendlyName');
    function friendlyTopic(topic)
    {
      return friendlyName ? Object.assign({ friendlyName: `${friendlyName} ${topic.topic}` }, topic) : topic;
    }

    this._adOrientation = this._node.advertise(friendlyTopic(TOPIC_ORIENTATION));
    this._adCalibration = this._node.advertise(friendlyTopic(TOPIC_CALIBRATION));

    this._id = native.imuFusion_start(this._type, this._i2c.mag, this._i2c.accel, this._i2c.gyro, TICK_FREQ);
    if (this._id === -1)
    {
      throw new Error(`Failed to start IMU: ${friendlyName}`);
    }
    if (!this._resetAll)
    {
      this._loadCalibration();
    }
    this._resetAll = false;
    this._resetGyro = false;
    this._resetMag = false;
    this._resetAccel = false;

    this._clock1 = setInterval(() => {
      this._updateQuaternion();
    }, 1000 / this._config.get('quaternionHz'));

    // Constantly improving the calibration
    // Start fast but slow once we have the system calibrated
    let fast = true;
    const cal = () => {
      this._calibrate();
      if (fast && this._calibration.sys >= 3)
      {
        clearInterval(this._clock2);
        this._clock2 = setInterval(cal, 1000 / TICK_SLOW_CALIBRATION);
        fast = false;
      }
      else if (!fast && this._calibration.sys < 3)
      {
        clearInterval(this._clock2);
        this._clock2 = setInterval(cal, 1000 / TICK_FAST_CALIBRATION);
        fast = true;
      }
    }
    this._clock2 = setInterval(cal, 1000 / TICK_FAST_CALIBRATION);

    this._node.service(SERVICE_RESET, (request) =>
    {
      this._resetCalibration();
    });
  },

  disable: function()
  {
    if (--this._enabled === 0)
    {
      this._disable();
    }
    return this;
  },

  _disable: function()
  {
    clearInterval(this._clock1);
    clearInterval(this._clock2);
    native.imuFusion_stop(this._id);

    this._node.unservice(SERVICE_RESET);
    this._node.unadvertise(TOPIC_ORIENTATION);
    this._node.unadvertise(TOPIC_CALIBRATION);

    this._saveCalibration();
  },

  reconfigure: function()
  {
    if (this._enabled)
    {
      clearInterval(this._clock1);
      this._clock1 = setInterval(() => {
        this._updateQuaternion();
      }, 1000 / this._config.get('quaternionHz'));
    }
  },

  _updateQuaternion: function()
  {
    const q = native.imuFusion_getQuaternion(this._id);
    this._adOrientation.publish(
    {
      confidence: this._calibration.sys,
      w: q[0],
      x: q[1],
      y: q[2],
      z: q[3]
    });
  },

  _calibrate: function()
  {
    const raw = native.imuFusion_getRaw(this._id);
//const fix = native.imuFusion_getAdjusted(this._id);
//console.log(JSON.stringify([ fix[3], fix[4], fix[5] ]));

    if (this._buckets.gyr.length < MIN_BUCKET_SIZE)
    {
      this._buckets.gyr.push([ raw[0], raw[1], raw[2] ]);
      if (this._buckets.gyr.length === MIN_BUCKET_SIZE)
      {
        this._calibrateGyro();
        this._updateConfidence();
      }
    }
    this._insertPointIntoBucket(this._biases.mag, this._buckets.mag, [ raw[3], raw[4], raw[5] ]);
    this._insertPointIntoBucket(this._biases.acc, this._buckets.acc, [ raw[6], raw[7], raw[8] ]);

    if ((this._rebias % TICK_BIASES) === 0)
    {
      this._calibrateMag();
      this._calibrateAccel();
      this._rebalanceBuckets(this._biases.mag, this._buckets.mag);
      this._rebalanceBuckets(this._biases.acc, this._buckets.acc);
      this._updateConfidence();
    }

    this._rebias++;
  },

  _calibrateGyro: function()
  {
    if (this._buckets.gyr.length)
    {
      this._biases.gyr = [ MATH.mean(this._buckets.gyr, 0), IDENTITY_MATRIX ];
    }
    else
    {
      this._biases.gyr = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];
    }
    native.imuFusion_setBias(this._id, BIAS_GYRO, this._biases.gyr[0], this._biases.gyr[1]);
  },

  _calibrateMag: function()
  {
    const mcal = native.imuFusion_calibrate(this._config.get('magneticNorm'), this._filterPoints(this._getBucketPoints(this._buckets.mag)));
    if (mcal)
    {
      this._biases.mag = mcal;
    }
    else
    {
      this._biases.mag = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];
    }
//console.log(JSON.stringify(this._biases.mag));
    native.imuFusion_setBias(this._id, BIAS_MAG, this._biases.mag[0], this._biases.mag[1]);
  },

  _calibrateAccel: function()
  {
    const acal = native.imuFusion_calibrate(this._config.get('accelNorm'), this._filterPoints(this._getBucketPoints(this._buckets.acc)));
    if (acal)
    {
      this._biases.acc = acal;
    }
    else
    {
      this._biases.acc = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];
    }
    native.imuFusion_setBias(this._id, BIAS_ACCEL, this._biases.acc[0], this._biases.acc[1]);
  },

  // Filter the points based on the vector length.
  _filterPoints: function(points)
  {
    if (points.length === 0)
    {
      return points;
    }
    const sqlengths = points.map((point) => {
      return point[0] * point[0] + point[1] * point[1] + point[2] * point[2];
    });
    const mean = MATH.mean(sqlengths);
    const std = MATH.std(sqlengths);
    const low = mean - std * 2;
    const high = mean + std * 2;
    const fpoints = points.filter((point, idx) => {
      return sqlengths[idx] > low && sqlengths[idx] < high;
    });
    return fpoints;
  },

  _insertPointIntoBucket: function(bias, buckets, point)
  {
    const adjustedpoint = [ point[0] - bias[0][0], point[1] - bias[0][1], point[2] - bias[0][2] ];
    const radius = Math.sqrt(adjustedpoint[0] * adjustedpoint[0] + adjustedpoint[1] * adjustedpoint[1] + adjustedpoint[2] * adjustedpoint[2]);
    if (radius == 0)
    {
      return; // Ignore
    }
    // spherical coordinate
    const theta = Math.atan2(adjustedpoint[0], adjustedpoint[2]); // equator angle around y-up axis -PI to +PI
    const phi = Math.acos(adjustedpoint[1] / radius); // polar angle 0 to +PI

    // Map to bucket
    const thetaB = Math.min(THETA_BUCKETS - 1, Math.floor(THETA_BUCKETS * (1 + theta / Math.PI) / 2));
    const phiB = Math.min(PHI_BUCKETS - 1, Math.floor(PHI_BUCKETS * (phi / Math.PI)));
    const bucket = buckets[thetaB * PHI_BUCKETS + phiB];

    if (bucket.length >= MAX_BUCKET_SIZE)
    {
      bucket[Math.floor(Math.random() * MAX_BUCKET_SIZE)] = point;
    }
    else
    {
      bucket.push(point);
    }
  },

  _rebalanceBuckets: function(bias, buckets)
  {
    const all = this._getBucketPoints(buckets);
    buckets.forEach((bucket) => {
      bucket.length = 0;
    });
    all.forEach((point) => {
      this._insertPointIntoBucket(bias, buckets, point);
    });
//console.log(JSON.stringify(buckets.map(bucket => bucket.length)));
  },

  _getBucketPoints: function(buckets)
  {
    return [].concat.apply([], buckets);
  },

  _updateConfidence: function()
  {
    const nconf = this._getConfidence();
    const old = this._calibration;
    for (let key in nconf)
    {
      if (nconf[key] !== old[key])
      {
        this._calibration = nconf;
        this._adCalibration.publish(
        {
          old: old,
          new: this._calibration,
          confidence: this._calibration.sys
        });
        break;
      }
    }
  },

  _getConfidence: function()
  {
    const nconf =
    {
      mag: Math.round(3 * this._buckets.mag.reduce((acc, val) => {
          return acc + !!val.length;
        }, 0) / NR_BUCKETS),
      acc: Math.round(3 * this._buckets.acc.reduce((acc, val) => {
          return acc + !!val.length;
        }, 0) / NR_BUCKETS),
      gyr: this._buckets.gyr.length >= MIN_BUCKET_SIZE ? 3 : 0
    };
    nconf.sys = Math.min(nconf.mag, nconf.acc, nconf.gyr);
    return nconf;
  },

  _saveCalibration: function()
  {
    FS.writeFileSync(`./saved/imu-calibration-data${this._name.replace(/\//g, '-')}.json`,
      JSON.stringify(
        {
          gyro: { bias: this._biases.gyr, points: this._buckets.gyr },
          mag: { bias: this._biases.mag, points: this._getBucketPoints(this._buckets.mag) },
          accel: { bias: this._biases.acc, points: this._getBucketPoints(this._buckets.acc) }
        }
      )
    );
  },

  _loadCalibration: function()
  {
    try
    {
      const data = JSON.parse(FS.readFileSync(`./saved/imu-calibration-data${this._name.replace(/\//g, '-')}.json`));
      
      this._buckets.gyr.length = 0;
      this._buckets.mag.forEach((bucket) => {
        bucket.length = 0;
      });
      this._buckets.acc.forEach((bucket) => {
        bucket.length = 0;
      });

      if (!this._resetGyro)
      {
        this._biases.gyr = data.gyro.bias;
        this._buckets.gyr = data.gyro.points;
        this._calibrateGyro();
      }
      if (!this._resetMag)
      {
        this._biases.mag = data.mag.bias;
        data.mag.points.forEach((point) => {
          this._insertPointIntoBucket(this._biases.mag, this._buckets.mag, point);
        });
        this._calibrateMag();
      }
      if (!this._resetAccel)
      {
        this._biases.acc = data.accel.bias;
        data.accel.points.forEach((point) => {
          this._insertPointIntoBucket(this._biases.acc, this._buckets.acc, point);
        });
        this._calibrateAccel();
      }

      this._rebias = 0;

      this._updateConfidence();
    }
    catch (_)
    {
      this._buckets.gyr.length = 0;
      this._buckets.mag.forEach((bucket) => {
        bucket.length = 0;
      });
      this._buckets.acc.forEach((bucket) => {
        bucket.length = 0;
      });
      this._rebias = 0;
      this._biases.gyr = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];
      this._biases.mag = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];
      this._biases.acc = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];

      native.imuFusion_setBias(this._id, BIAS_GYRO, this._biases.gyr[0], this._biases.gyr[1]);
      native.imuFusion_setBias(this._id, BIAS_MAG, this._biases.mag[0], this._biases.mag[1]);
      native.imuFusion_setBias(this._id, BIAS_ACCEL, this._biases.acc[0], this._biases.acc[1]);

      this._updateConfidence();
    }
  },

  _resetCalibration: function()
  {
    try
    {
      FS.unlinkSync(`./saved/imu-calibration-data${this._name.replace(/\//g, '-')}.json`);
    }
    catch (_)
    {
    }
    this._buckets.gyr.length = 0;
    this._buckets.mag.forEach((bucket) => {
      bucket.length = 0;
    });
    this._buckets.acc.forEach((bucket) => {
      bucket.length = 0;
    });
    this._rebias = 0;
    this._biases.gyr = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];
    this._biases.mag = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];
    this._biases.acc = [ DEFAULT_OFFSETS, IDENTITY_MATRIX ];

    native.imuFusion_setBias(this._id, BIAS_GYRO, this._biases.gyr[0], this._biases.gyr[1]);
    native.imuFusion_setBias(this._id, BIAS_MAG, this._biases.mag[0], this._biases.mag[1]);
    native.imuFusion_setBias(this._id, BIAS_ACCEL, this._biases.acc[0], this._biases.acc[1]);

    this._updateConfidence();
  }
};

module.exports = imu;
