module.exports = function()
{
  const DEFAULT_HTML_OPTIONS = { cache: 1 * 60 * 60, contentType: 'text/html; charset=utf-8' }; // cache for 1 hour
  const DEFAULT_JS_OPTIONS = { cache: 1 * 60 * 60, contentType: 'application/x-javascript' }; // cache for 1 hour
  const QUICK_JS_OPTIONS = { cache: 30, contentType: 'application/x-javascript' }; // cache for 30 seconds - just to rotate phone
  const ServerConfig = require('modules/server-config');
  return new ServerConfig(
  {
    name: '/ui/node',
    server: '/server',
    pages:
    {
      '/': { to: 'modules/ui/select.html', options: DEFAULT_HTML_OPTIONS },
      '/ui': { to: 'modules/ui/ui.html', options: DEFAULT_HTML_OPTIONS },
      '/js/8bit.js': { options: DEFAULT_JS_OPTIONS, to: [
        'modules/8bit.js',
        'modules/8bit-slave.js'
      ]},
      '/js/three.js': { to: 'node_modules/three/build/three.min.js', options: DEFAULT_JS_OPTIONS },
      '/js/ui.js': { options: DEFAULT_JS_OPTIONS, to: [
        'modules/ui/ui_core.js',
        'modules/ui/ui_types.js'
      ]},
      '/js/component_instances.js': { to: 'saved/component_instances.js', options: QUICK_JS_OPTIONS },
      '/js/touch_emulator.js': { to: 'modules/ui/touch_emulator.js', options: DEFAULT_JS_OPTIONS },
    }
  });
}
