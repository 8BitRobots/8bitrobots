module.exports = function()
{
  const Health = require('modules/health');
  return new Health(
  {
    name: '/health/node',
    battery:
    {
      topic: '/power/status', 
      batteryChemistry: 'DC',
      minV: 6.5
    }
  });
}
