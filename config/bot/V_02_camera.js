module.exports = function()
{
  const CONFIG =
  {
    id: 1,
    device: '/dev/video1',
    port: 8082,
    size: '1280x720'
  };

  if (require('fs').existsSync(CONFIG.device))
  {
    const Camera = require('hw/camera/camera');

    return new Camera(
    {
      name: `/camera/${CONFIG.id}/node`,
      server: '/server',
      path: `/camera/${CONFIG.id}`,
      device: CONFIG.device,
      port: CONFIG.port,
      size: CONFIG.size
    });
  }
};
