module.exports = function()
{
  const DEFAULT_OPTIONS = { cache: 24 * 60 * 60 }; // cache for 24 hours
  const DEFAULT_JS_OPTIONS = { cache: 24 * 60 * 60, contentType: 'application/x-javascript' };
  const ServerConfig = require('modules/server-config');
  return new ServerConfig(
  {
    name: '/blockly/node',
    server: '/server',
    pages:
    {
      '/edit': 'modules/ui/edit.html',
      '/blockly/blockly.js': { options: DEFAULT_JS_OPTIONS, to: [
        'modules/blockly/blockly_compressed.js',
        'modules/blockly/blocks_compressed.js',
        'modules/blockly/javascript_compressed.js'
      ]},
      '/blockly/msg/js/': { to: 'modules/blockly/msg/js/', options: DEFAULT_OPTIONS },
      '/blockly/8bitblocks.js': 'modules/blockly/8bitblocks.js',
      '/images/sprites.png': { to: 'modules/blockly/sprites.png', options: DEFAULT_OPTIONS },
      '/images/handdelete.cur': { to: 'modules/blockly/handdelete.cur', options: DEFAULT_OPTIONS },
    }
  });
}
