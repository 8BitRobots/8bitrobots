module.exports = function()
{
  const CONFIG =
  {
    id: 0,
    device: '/dev/video0',
    port: 8081,
    size: '1280x720'
  };

  if (require('fs').existsSync(CONFIG.device) || SIMULATOR)
  {
    const Camera = require('hw/camera/camera');

    return new Camera(
    {
      name: `/camera/${CONFIG.id}/node`,
      server: '/server',
      path: `/camera/${CONFIG.id}`,
      device: CONFIG.device,
      port: CONFIG.port,
      size: CONFIG.size
    });
  }
};
