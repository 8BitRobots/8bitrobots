console.info('Loading I2C');

const I2C = require('hw/board/raspberrypi/i2c');
global.I2C = [ 3, 1 ].reduce((bus, id) => {
  return bus || I2C.open(
  {
    bus: id
  });
}, null);

module.exports = function() {}
