module.exports = function()
{
  const AHRS = require('modules/ahrs');
  return new AHRS(
  {
    name: '/ahrs/node',
    monitor:
    [
      { type: 'imu', name: '/imu/body' }
    ],
    calibrationTimeout: 5000
  });
}
