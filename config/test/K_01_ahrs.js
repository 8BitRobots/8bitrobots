module.exports = function()
{
  const AHRS = require('modules/ahrs');
  return new AHRS(
  {
    name: '/ahrs/node',
    monitor:
    [
      { type: 'imu', name: '/imu1' },
      { type: 'imu', name: '/imu2' },
      { type: 'imu', name: '/imu3' },
      { type: 'air', name: '/atmos' }
    ],
    calibrationTimeout: 5000
  });
}
