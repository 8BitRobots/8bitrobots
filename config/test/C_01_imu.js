module.exports = function()
{
  const i2c1 = I2C.open(
  {
    address: 0x19
  });
  if (i2c1.valid())
  {
    const Imu = require('hw/board/raspberrypi/imu-fusion');
    return new Imu(
    {
      name: '/imu1/node',
      type: Imu.LSM303DLHC_L3GD20,
      i2c:
      {
        mag: 0x1e,
        accel: 0x19,
        gyro: 0x6b
      }
    });
  }
}

