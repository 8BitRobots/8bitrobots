module.exports = function()
{
  const i2c1 = I2C.open(
  {
    address: 0x68
  });
  if (i2c1.valid())
  {
    const Imu = require('hw/board/raspberrypi/imu-fusion');
    return new Imu(
    {
      name: '/imu2/node',
      type: Imu.MPU9250,
      i2c:
      {
        mag: 0x0c,
        accel: 0x68,
        gyro: 0x68
      }
    });
  }
}

