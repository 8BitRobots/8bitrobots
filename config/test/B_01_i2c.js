console.info('Loading I2C');

const I2c = require('hw/board/raspberrypi/i2c');
global.I2C = [ 3, 1 ].reduce((bus, id) => {
  return bus || I2c.open(
  {
    bus: id
  });
}, null);

module.exports = function() {}
