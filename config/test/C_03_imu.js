module.exports = function()
{
  const i2c1 = I2C.open(
  {
    address: 0x6b
  });
  const i2c2 = I2C.open(
  {
    address: 0x19 // Avoid clash with LSM303DLHC_L3GD20
  });
  if (i2c1.valid() && !i2c2.valid())
  {
    const Imu = require('hw/board/raspberrypi/imu-fusion');
    return new Imu(
    {
      name: '/imu3/node',
      type: Imu.LSM9DS1,
      i2c:
      {
        mag: 0x1e,
        accel: 0x6b,
        gyro: 0x6b
      }
    });
  }
}
