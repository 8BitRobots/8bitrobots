(function(){

  window.UI = {
    TYPE: {},
    active: {},
    mode: 'normal',
    isVisible: false,
  };  

  // -------------------------------
  // Helpers
  //
  function UUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  }

  //
  // -------------------------------

  const FONT_DEFAULT = 'sans-serif';

  // -------------------------------
  // UI setup
  //
  const grid = {
    root: null,
    properties: null,
    points: {
      width: [],
      height: []
    },
  };

  UI.setup = function(config) {
    const hstep = (config.height - config.horizontalMargin * 2) / config.horizontalSteps;
    const vstep = (config.width - config.verticalMargin * 2) / config.verticalSteps;

    grid.root = config.root;
    UI.mode = config.mode || 'normal';
    grid.verticalSteps = config.verticalSteps;
    grid.horizontalSteps = config.horizontalSteps;

    grid.points.width.push(0);
    for (let i = 0; i <= config.verticalSteps; i++) {
      grid.points.width.push(config.verticalMargin + i * vstep);
    }
    grid.points.width.push(config.width);
    grid.points.height.push(0);
    for (let i = 0; i <= config.horizontalSteps; i++) {
      grid.points.height.push(config.horizontalMargin + i * hstep);
    }
    grid.points.height.push(config.height);

    if (UI.mode === 'edit' || UI.mode === 'debug') {
  
      const canvas = document.createElement('canvas');
      grid.root.appendChild(canvas);
      canvas.setAttribute('width', config.width);
      canvas.setAttribute('height', config.height);

      const box = canvas.getBoundingClientRect();

      const LINE_WIDTH = 1;

      let ctx = canvas.getContext('2d');
      ctx.lineWidth = LINE_WIDTH;
      ctx.strokeStyle = 'grey';
      ctx.strokeRect(0, 0, config.width, config.height);

      ctx.setLineDash([ 5, 2 ]);
      ctx.beginPath();
      for (let i = 0; i <= config.verticalSteps; i++) {
        ctx.moveTo(config.verticalMargin + i * vstep, 0);
        ctx.lineTo(config.verticalMargin + i * vstep, config.height);
      }
      for (let i = 0; i <= config.horizontalSteps; i++) {
        ctx.moveTo(0, config.horizontalMargin + i * hstep);
        ctx.lineTo(config.width, config.horizontalMargin + i * hstep);
      }
      ctx.stroke();
    }
  
    if (UI.mode === 'edit') {

      // -------------------------------
      // Editor
      //
      const editCss = document.createElement('style');
      editCss.type = 'text/css';
      editCss.innerHTML = `
        .editing .edit { position: absolute; z-index: 1000; top: 0: left: 0; height: 100%; width: 100%; border: 1px solid green; color: green; cursor: grab; }
        .editing .edit .resize { position: absolute; right: 0; bottom: 0; width: 15px; height: 15px; cursor: crosshair; background-color: green; }
        .editing .edit .label { position: relative; top: -20px; font: 1em ${FONT_DEFAULT}; text-align: left; }
        .editing.selected .edit { color: red; border-color: red; }
        .editing.selected .edit .resize { background-color: red; }
        .ui_icon_row { width: 100%; height: 50%; clear: both; }
        .ui_icon { float: left; display: flex; align-items: center; justify-content: center; color: white; border: 2px solid white; font: 1.25em ${FONT_DEFAULT}; border-radius: 5px; width: 64px; height: 64px; margin: 4px; }
      `;
      document.head.appendChild(editCss);

      grid.properties = config.properties;
      grid.icons = config.icons;

      const edit = {
        mode: null,
        component: null,
        selected: null
      };

      function selectComponent(component) {
        if (edit.selected != edit.component) {
          edit.selected = edit.component;
          edit.selected.root.classList.add('selected');
          const title = grid.properties.querySelector('#title');
          title.innerHTML = edit.selected.type.title;
          let keyValue = grid.properties.querySelector('#properties').firstElementChild;
          for (let key in edit.selected.properties) {
            const value = edit.selected.properties[key];
  
            let type = 'text';
            let checked = '';
            if (typeof value === 'boolean') {
              type = 'checkbox';
              checked = value ? 'checked' : '';
            }
            keyValue.innerHTML = `<div class="key">${key}</div><input type="${type}" class="value" value="${value}" ${checked} data-key="${key}">`;
            keyValue.addEventListener('change', function(e) {
              const key = e.target.dataset.key;
              const ktype = typeof edit.selected.properties[key];
              if (ktype === 'boolean') {
                edit.selected.properties[key] = e.target.checked;
              }
              else if (ktype === 'number') {
                edit.selected.properties[key] = parseFloat(e.target.value);
              }
              else {
                edit.selected.properties[key] = e.target.value;
                if (key === 'name') {
                  // When we change the name, update the editing label and re-register
                  edit.selected.root.getElementsByClassName('label')[0].innerText = e.target.value;
                  if (edit.selected.grid.x !== -1) {
                    APP.unregisterComponent(WORKSPACE, edit.selected);
                    APP.registerComponent(WORKSPACE, edit.selected);
                  }
                }
              }
              edit.selected.type.draw(edit.selected);
            });
          
            keyValue = keyValue.nextElementSibling;
            if (!keyValue) {
              break;
            }
          }
          for (; keyValue; keyValue = keyValue.nextElementSibling) {
            keyValue.innerHTML = '';
          }
          grid.properties.querySelector('.buttons').classList.add('visible');
        }
      }
  
      document.body.addEventListener('mousedown', function(touch) {
        const target = touch.target;
        if (target.classList.contains('resize')) {
          edit.mode = 'resize';
          edit.component = UI.active[target.parentNode.getAttribute('componentId')];
          document.body.style.cursor = 'crosshair';
        }
        else if (target.classList.contains('edit')) {
          edit.mode = 'move';
          edit.component = UI.active[target.getAttribute('componentId')];
          document.body.style.cursor = 'grab';
        }
  
        if (edit.selected && edit.selected !== edit.component) {
          edit.selected.root.classList.remove('selected');
          edit.selected = null;
        }
  
        if (!edit.component) {
          edit.mode = null;
          document.body.style.cursor = '';
          return;
        }
        const style = getComputedStyle(edit.component.root);
        edit.start = { top: parseInt(style.top), left: parseInt(style.left), height: parseInt(style.height), width: parseInt(style.width) };
        edit.touch = { y: touch.clientY, x: touch.clientX };
  
        selectComponent(edit.component);
      });
  
      function snapToGrid(pos) {
        const npos = { x: 0, y: 0, gx: -1, gy: -1 };
        let diff = Number.MAX_SAFE_INTEGER;
        let i;
        for (i = 0; i < grid.points.width.length; i++) {
          const ndiff = Math.abs(pos.x - grid.points.width[i]);
          if (ndiff > diff) {
            npos.x = grid.points.width[i - 1];
            npos.gx = i - 1;
            break;
          }
          diff = ndiff;
        }
        if (i === grid.points.width.length) {
          npos.x = grid.points.width[i - 1];
          npos.gx = i - 1;
        }
        diff = Number.MAX_SAFE_INTEGER;
        for (i = 0; i < grid.points.height.length; i++) {
          const ndiff = Math.abs(pos.y - grid.points.height[i]);
          if (ndiff > diff) {
            npos.y = grid.points.height[i - 1];
            npos.gy = i - 1;
            break;
          }
          diff = ndiff;
        }
        if (i === grid.points.height.length) {
          npos.y = grid.points.height[i - 1];
          npos.gy = i - 1;
        }
        return npos;
      }
  
      document.body.addEventListener('mouseup', function() {
        if (!edit.mode) {
          return;
        }
        document.body.style.cursor = '';
  
        // Snap to grid
        const SNAP_MARGIN = 50;
        const style = getComputedStyle(edit.component.root);
  
        let npos = { x: parseInt(style.left), y: parseInt(style.top) };
        if (npos.x >= grid.points.width[0] - SNAP_MARGIN && npos.x < grid.points.width[grid.points.width.length - 1] + SNAP_MARGIN &&
            npos.y >= grid.points.height[0] - SNAP_MARGIN && npos.y < grid.points.height[grid.points.height.length - 1] + SNAP_MARGIN) {
  
          npos = snapToGrid(npos);
          const nsize = snapToGrid({ x: npos.x + parseInt(style.width), y: npos.y + parseInt(style.height) });
          nsize.x -= npos.x;
          nsize.y -= npos.y;
          nsize.gx -= npos.gx;
          nsize.gy -= npos.gy;
          if (edit.component.type.isSquare) {
            nsize.y = Math.min(nsize.y, nsize.x)
            nsize.x = nsize.y;
            nsize.gy = Math.min(nsize.gy, nsize.gx);
            nsize.gx = nsize.gy;
          }
  
          edit.component.grid.x = npos.gx;
          edit.component.grid.y = npos.gy;
          edit.component.grid.width = nsize.gx;
          edit.component.grid.height = nsize.gy;
  
          edit.component.root.style.left = npos.x + 'px';
          edit.component.root.style.top = npos.y + 'px';
          edit.component.root.style.width = nsize.x + 'px';
          edit.component.root.style.height = nsize.y + 'px';
          edit.component.type.draw(edit.component);
        }
        else {
          edit.component.grid = { x: -1, y: -1, width: 0, height: 0 };
        }
        edit.mode = null;

        APP.unregisterComponent(WORKSPACE, edit.selected);
        if (edit.selected.grid.x !== -1) {
          APP.registerComponent(WORKSPACE, edit.selected);
        }
      });
  
      document.body.addEventListener('mousemove', function(touch) {
        switch (edit.mode) {
          case 'move':
            edit.component.root.style.top = (edit.start.top + touch.clientY - edit.touch.y) + 'px';
            edit.component.root.style.left = (edit.start.left + touch.clientX - edit.touch.x) + 'px';
            break;
  
          case 'resize':
            let height = edit.start.height + touch.clientY - edit.touch.y;
            let width = edit.start.width + touch.clientX - edit.touch.x;
            if (edit.component.type.isSquare) {
              height = Math.min(height, width);
              width = height;
            }
            edit.component.root.style.height = height + 'px';
            edit.component.root.style.width = width + 'px';
            edit.component.type.draw(edit.component);
            break;
  
          default:
            break;
        }
      });

      grid.properties.querySelector('.buttons .button.delete').addEventListener('click', function() {
        grid.properties.querySelector('.buttons').classList.remove('visible');
        grid.properties.querySelector('#title').innerText = '';
        for (let keyValue = grid.properties.querySelector('#properties').firstElementChild; keyValue; keyValue = keyValue.nextElementSibling) {
          keyValue.innerHTML = '';
        }
        edit.component.root.remove();
        APP.unregisterComponent(WORKSPACE, edit.component);
        delete UI.active[edit.component.id];
      });

      UI.clear = function() {
        for (let id in UI.active) {
          const component = UI.active[id];
          component.root.remove();
          APP.unregisterComponent(WORKSPACE, component);
        }
        grid.properties.querySelector('#title').innerText = '';
        for (let keyValue = grid.properties.querySelector('#properties').firstElementChild; keyValue; keyValue = keyValue.nextElementSibling) {
          keyValue.innerHTML = '';
        }
        UI.active = {};
      }

      // -------------------------------
      // Serializer
      //
      UI.serialized = function() {
        const components = [];
        Object.values(UI.active).forEach((component) => {
          const serialized = {
            id: component.id,
            type: Object.keys(UI.TYPE).find((key) => { return UI.TYPE[key] === component.type; }),
            grid: component.grid,
            properties: component.properties,
          };
          if (serialized.grid.x !== -1) {
            components.push(serialized);
          }
        });
        return {
          grid: {
            layout: { verticalSteps: grid.verticalSteps, horizontalSteps: grid.horizontalSteps },
            components: components
          }
        };
      }

      document.addEventListener('DOMContentLoaded', function() {

        // Create icons
        const rows = [ document.createElement('div'), document.createElement('div') ];
        rows[0].className = 'ui_icon_row';
        rows[1].className = 'ui_icon_row';
        grid.icons.appendChild(rows[0]);
        grid.icons.appendChild(rows[1]);
        let row = 0;
        Object.keys(UI.TYPE).forEach(function(key) {
          const icon = document.createElement('div');
          icon.className = 'ui_icon';
          icon.innerText = UI.TYPE[key].icon;
          rows[row].appendChild(icon);
          row = 1 - row;
          icon.addEventListener('click', function() {
            edit.component = UI.embed({ type: key, pos: { x: 500, y: 500, width: 0, height: 0 } });
            selectComponent(edit.component);
          });
        });

        APP.registerComponent(WORKSPACE, {
          id: '/controller/Controller',
          properties: {
            name: 'Controller'
          },
          type: {
            properites: {},
            schemas: {
              events: {
                connect: { uuid: 'String' },
                tick: { timestamp: 'Number' }
              }
            }
          }
        });
      });
    }
    else {
      window.NODE = Node.init('/controller/node');
    }
  
    // Setup the loaded types
    UI_TYPE_CREATE();

    // -------------------------------

    // -------------------------------
    // Embed
    //
    const builder = document.createElement('div');

    UI.embed = function(config) {
      const type = UI.TYPE[config.type];

      if (type.css && !type._cssEnabled) {
        const css = document.createElement('style');
        css.type = 'text/css';
        css.innerHTML = type.css;
        document.head.appendChild(css);
        type._cssEnabled = true;
      }

      builder.innerHTML = type.html;
      const component = { id: config.id || UUID(), type: type, root: builder.firstElementChild, properties: {}, grid: { x: -1, y: -1, width: 0, height: 0 } };
      for (var key in config.properties) {
        component.properties[key] = config.properties[key];
      }

      let pos = config.pos || { x: 0, y: 0, width: 0, height: 0 };
      if (config.grid) {
        component.grid.x = config.grid.x;
        component.grid.y = config.grid.y;
        component.grid.width = config.grid.width;
        component.grid.height = config.grid.height;
        pos = { 
          x: grid.points.width[config.grid.x],
          y: grid.points.height[config.grid.y],
          width: grid.points.width[config.grid.x + config.grid.width] - grid.points.width[config.grid.x],
          height: grid.points.height[config.grid.y + config.grid.height] - grid.points.height[config.grid.y]
        };
      }
      component.root.style.position = 'absolute';
      component.root.style.left = pos.x + 'px';
      component.root.style.top = pos.y + 'px';
      component.root.style.width = pos.width + 'px';
      component.root.style.height = pos.height + 'px';
      grid.root.appendChild(component.root);

      for (let id in type.config) {
        if (!(id in component.properties)) {
          component.properties[id] = type.config[id];
        }
      }
      for (let id in type.properties) {
        if (!(id in component.properties)) {
          component.properties[id] = type.properties[id];
        }
      }

      type.setup(component);
      type.draw(component);

      switch (UI.mode) {
        case 'normal':
        case 'debug':
          if (type.schemas.events) {
            for (let topic in type.schemas.events) {
              const ad = NODE.advertise({ topic: `${component.properties.name}/${topic}`, schema: type.schemas.events[topic] });
              component[`publish_${topic}`] = function(value) {
                ad.publish(value);
              };
            }
          }
          const schema = {};
          const properties = component.type.schemas.action || component.type.properties;
          for (let prop in properties) {
            const tprop = typeof properties[prop];
            switch (tprop) {
              case 'number':
              case 'boolean':
              case 'string':
                schema[prop] = tprop.charAt(0).toUpperCase() + tprop.slice(1);
                break;
              default:
                break;
            }
          }
          if (Object.keys(schema).length > 0) {
            NODE.service({ service: `${component.properties.name}`, schema: schema }, function(request) {
              return type.action(component, request);
            });
          }
          type.enable(component);
          break;
        case 'edit':
          component.root.classList.add('editing');
          component.root.getElementsByClassName('edit')[0].setAttribute('componentId', component.id);
          component.root.getElementsByClassName('label')[0].innerText = component.properties.name;
          if (component.grid.x !== -1) {
            APP.registerComponent(WORKSPACE, component);
          }
          break;
        case 'inactive':
        default:
          break;
      }

      UI.active[component.id] = component;

      return component;
    }

    UI.connect = function() {
      NODE.advertise({ topic: 'Controller/connect', schema: { uuid: 'String' } }).publish({ uuid: UUID() });
      UI.visible(true);
    }

    let tick = {
      ad: null,
      timer: null
    };

    UI.visible = function(isVisible) {
      if (UI.isVisible != isVisible) {
        UI.isVisible = isVisible;
        if (UI.isVisible) {
          if (!tick.ad) {
            tick.ad = NODE.advertise({ topic: 'Controller/tick', schema: { timestamp: 'Number' } });
          }
          if (tick.timer) {
            clearInterval(tick.timer);
          }
          tick.timer = setInterval(function() {
            tick.ad.publish({ timestamp: Date.now() });
          }, 1000);
        }
        else {
          if (tick.timer) {
            clearInterval(tick.timer);
            tick.timer = null;
          }
        }
      }
    }
  }

})();