function UI_TYPE_CREATE()
{
  const MOVE_HTML = `${UI.mode === 'edit' ? '<div class="edit"><div class="label"></div></div>' : ''}`;
  const MOVE_RESIZE_HTML = `${UI.mode === 'edit' ? '<div class="edit"><div class="resize"></div><div class="label"></div></div>' : ''}`;

  const FONT_DEFAULT = 'sans-serif';
  const FONT_DEFAULT_AND_SIZE = `14px ${FONT_DEFAULT}`;

  function actionHelper(component, request) {
    for (let key in component.type.properties) {
      if (key in request) {
        component.properties[key] = request[key];
      }
    }
    component.type.draw(component);
  }

  // -------------------------------
  //
  // UI TYPES
  //
  // -------------------------------


  UI.TYPE.joystick = {
    html: `<div class="joystick">
      ${MOVE_RESIZE_HTML}
      <canvas class="pad"></canvas>
      <canvas class="stick" width="100" height="100"></canvas>
    </div>`,

    css: `
      .joystick { min-width: 120px; min-height: 120px; z-index: 1; }
      .joystick .stick { position: absolute }
    `,

    title: 'Joystick',
    icon: `Joy`,

    schemas: {
      events: {
        position: { x: 'Number', y: 'Number' },
      }
    },

    config: {
      name: 'Joystick',
    },

    properties: {
      autocenter: true,
      deadband: 0.1,
      color: 'white'
    },

    isSquare: true,

    setup: function(component) {
      component.pad = component.root.getElementsByClassName('pad')[0];
      component.stick = component.root.getElementsByClassName('stick')[0];
      component.lastMove = Date.now();
    },

    draw: function(component) {
      const jstyle = getComputedStyle(component.root);
      const jwidth = parseInt(jstyle.width);
      const jheight = parseInt(jstyle.height);
    
      component.pad.setAttribute('width', jwidth);
      component.pad.setAttribute('height', jheight);

      const sstyle = getComputedStyle(component.stick);
      const stop = (jheight - parseInt(sstyle.height)) / 2;
      const sleft = (jwidth - parseInt(sstyle.width)) / 2;
    
      component.stick.style.top = stop + 'px';
      component.stick.style.left = sleft + 'px';
    
      component.animate = {
        endtimer: null,
        movetimer: null,
        touchstart: { x: null, y: null },
        target: { left: sleft, top: stop },
        current: { left: sleft, top: stop },
        step: { left: null, top: null, count: null }
      };

      const LINE_WIDTH = 5;
      const STICK_RADIUS = 50;

      let ctx = component.pad.getContext('2d');
      ctx.beginPath();
      ctx.lineWidth = LINE_WIDTH;
      ctx.strokeStyle = component.properties.color || 'white';
      ctx.arc(jwidth / 2, jheight / 2, jwidth / 2 - LINE_WIDTH, 0, Math.PI * 2);
      ctx.stroke();

      ctx = component.stick.getContext('2d');
      ctx.beginPath();
      ctx.lineWidth = LINE_WIDTH;
      ctx.strokeStyle = component.properties.color || 'white';
      ctx.arc(STICK_RADIUS, STICK_RADIUS, STICK_RADIUS - LINE_WIDTH, 0, Math.PI * 2);
      ctx.stroke();
    },

    enable: function(component) {
      const active = component.animate.target.left;
      const animate = component.animate;
      const stick = component.stick;

      function getStickPos() {
        const style = getComputedStyle(stick);
        let x = parseInt(style.left) - animate.target.left;
        let y = animate.target.top - parseInt(style.top);
        x = Math.max(-active, Math.min(x, active)) / active;
        y = Math.max(-active, Math.min(y, active)) / active;
        if (Math.abs(x) < component.properties.deadband) {
          x = 0;
        }
        if (Math.abs(y) < component.properties.deadband) {
          y = 0;
        }
        return { x: x, y: y };
      }

      function moveStick(force) {
        const now = Date.now();
        if (now - component.lastMove > 100 || force) {
          component.lastMove = now;
          component.publish_position(getStickPos());
        }
      }

      stick.addEventListener('touchstart', function(e) {
        if (animate.endtimer) {
          clearInterval(animate.endtimer);
          animate.endtimer = null;
        }
        if (animate.movetimer) {
          clearInterval(animate.movetimer);
        }
        animate.movetimer = setInterval(function() {
          moveStick(false);
        }, 100);
        animate.touchstart.x = e.changedTouches[0].clientX;
        animate.touchstart.y = e.changedTouches[0].clientY;
        const style = getComputedStyle(stick);
        animate.current.top = parseInt(style.top);
        animate.current.left = parseInt(style.left);
      });

      stick.addEventListener('touchend', function(e) {
        if (animate.movetimer) {
          clearInterval(animate.movetimer);
          animate.movetimer = null;
        }
        if (!animate.endtimer) {
          const pos = getStickPos();
          if (component.properties.autocenter || (pos.x === 0 && pos.y === 0)) {
            var style = getComputedStyle(stick);
            animate.current.top = parseInt(style.top);
            animate.current.left = parseInt(style.left);
            animate.step.count = 20;
            animate.step.top = (animate.current.top - animate.target.top) / animate.step.count;
            animate.step.left = (animate.current.left - animate.target.left) / animate.step.count;
            animate.endtimer = setInterval(function() {
              if (--animate.step.count) {
                animate.current.top -= animate.step.top;
                animate.current.left -= animate.step.left;
                stick.style.top = animate.current.top + 'px';
                stick.style.left = animate.current.left + 'px';
                moveStick(false);
              }
              else {
                animate.current.top = animate.target.top;
                animate.current.left = animate.target.left;
                stick.style.top = animate.target.top + 'px';
                stick.style.left = animate.target.left + 'px';
                clearInterval(animate.endtimer);
                animate.endtimer = null;
                moveStick(true);
              }
            }, 10);
          }
        }
      });

      stick.addEventListener('touchmove', function(e) {
        const touch = { x: e.changedTouches[0].clientX - animate.touchstart.x, y: e.changedTouches[0].clientY - animate.touchstart.y };
        const style = getComputedStyle(stick);
        stick.style.top = (animate.current.top + touch.y) + 'px';
        stick.style.left = (animate.current.left + touch.x) + 'px';
        moveStick(false);
      });
    },

    action: actionHelper
  }

  UI.TYPE.joystickh = {
    html: `<div class="joystickh">
      ${MOVE_RESIZE_HTML}
      <canvas class="pad"></canvas>
      <canvas class="stick" width="100" height="100"></canvas>
    </div>`,

    css: `
      .joystickh { min-width: 120px; min-height: 120px; max-height: 120px; z-index: 1; }
      .joystickh .stick { position: absolute }
    `,

    title: 'Horizontal Joystick',
    icon: `Jyh`,

    schemas: {
      events: {
        position: { x: 'Number' }
      }
    },

    config: {
      name: 'Joystick'
    },

    properties: {
      autocenter: true,
      deadband: 0.1,
      color: 'white'
    },
  
    setup: function(component) {
      component.pad = component.root.getElementsByClassName('pad')[0];
      component.stick = component.root.getElementsByClassName('stick')[0];
      component.lastMove = Date.now();
    },
  
    draw: function(component) {
      const jstyle = getComputedStyle(component.root);
      const jwidth = parseInt(jstyle.width);
      const jheight = parseInt(jstyle.height);
    
      component.pad.setAttribute('width', jwidth);
      component.pad.setAttribute('height', jheight);

      const sstyle = getComputedStyle(component.stick);
      const stop = (jheight - parseInt(sstyle.height)) / 2;
      const sleft = (jwidth - parseInt(sstyle.width)) / 2;
    
      component.stick.style.top = stop + 'px';
      component.stick.style.left = sleft + 'px';
    
      component.animate = {
        endtimer: null,
        movetimer: null,
        touchstart: { x: null },
        target: { left: sleft },
        current: { left: sleft },
        step: { left: null, count: null }
      };

      const LINE_WIDTH = 5;
      const STICK_RADIUS = 50;

      let ctx = component.pad.getContext('2d');
      ctx.lineWidth = LINE_WIDTH;
      ctx.strokeStyle = component.properties.color || 'white';;

      ctx.beginPath();
      ctx.arc(jheight / 2, jheight / 2, jheight / 2 - LINE_WIDTH, Math.PI / 2, Math.PI * 1.5);
      ctx.stroke();

      ctx.beginPath();
      ctx.moveTo(jheight / 2, LINE_WIDTH);
      ctx.lineTo(jwidth - jheight / 2, LINE_WIDTH);
      ctx.moveTo(jheight / 2, jheight - LINE_WIDTH);
      ctx.lineTo(jwidth - jheight / 2, jheight - LINE_WIDTH);
      ctx.stroke();

      ctx.beginPath();
      ctx.arc(jwidth - jheight / 2, jheight / 2, jheight / 2 - LINE_WIDTH, Math.PI * 1.5, Math.PI / 2);
      ctx.stroke();

      ctx = component.stick.getContext('2d');
      ctx.lineWidth = LINE_WIDTH;
      ctx.strokeStyle = component.properties.color || 'white';;

      ctx.beginPath();
      ctx.arc(STICK_RADIUS, STICK_RADIUS, STICK_RADIUS - LINE_WIDTH, 0, Math.PI * 2);
      ctx.stroke();
    },
  
    enable: function(component) {
      const active = component.animate.target.left;
      const animate = component.animate;
      const stick = component.stick;

      function getStickPos() {
        const style = getComputedStyle(stick);
        let x = parseInt(style.left) - animate.target.left;
        x = Math.max(-active, Math.min(x, active)) / active;
        if (Math.abs(x) < component.properties.deadband) {
          x = 0;
        }
        return x;
      }

      function moveStick(force) {
        const now = Date.now();
        if (now - component.lastMove > 100 || force) {
          component.lastMove = now;
          component.publish_position({ x: getStickPos() });
        }
      }

      stick.addEventListener('touchstart', function(e) {
        if (animate.endtimer) {
          clearInterval(animate.endtimer);
          animate.endtimer = null;
        }
        if (animate.movetimer) {
          clearInterval(animate.movetimer);
        }
        animate.movetimer = setInterval(function() {
          moveStick(false);
        }, 100);
        animate.touchstart.x = e.changedTouches[0].clientX;
        const style = getComputedStyle(stick);
        animate.current.left = parseInt(style.left);
      });

      stick.addEventListener('touchend', function(e) {
        if (animate.movetimer) {
          clearInterval(animate.movetimer);
          animate.movetimer = null;
        }
        if (!animate.endtimer && (component.properties.autocenter || getStickPos() == 0)) {
          var style = getComputedStyle(stick);
          animate.current.left = parseInt(style.left);
          animate.step.count = 20;
          animate.step.left = (animate.current.left - animate.target.left) / animate.step.count;
          animate.endtimer = setInterval(function() {
            if (--animate.step.count) {
              animate.current.left -= animate.step.left;
              stick.style.left = animate.current.left + 'px';
              moveStick(false);
            }
            else {
              animate.current.left = animate.target.left;
              stick.style.left = animate.target.left + 'px';
              clearInterval(animate.endtimer);
              animate.endtimer = null;
              moveStick(true);
            }
          }, 10);
        }
      });

      stick.addEventListener('touchmove', function(e) {
        const touch = { x: e.changedTouches[0].clientX - animate.touchstart.x };
        const style = getComputedStyle(stick);
        stick.style.left = (animate.current.left + touch.x) + 'px';
        moveStick(false);
      });
    },

    action: actionHelper
  }

  UI.TYPE.joystickv = {
    html: `<div class="joystickv">
      ${MOVE_RESIZE_HTML}
      <canvas class="pad"></canvas>
      <canvas class="stick" width="100" height="100"></canvas>
    </div>`,

    css: `
      .joystickv { min-width: 120px; min-height: 120px; max-width: 120px; z-index: 1; }
      .joystickv .stick { position: absolute }
    `,

    title: 'Vertical Joystick',
    icon: `Jyv`,

    schemas: {
      events: {
        position: { y: 'Number' }
      }
    },

    config: {
      name: 'Joystick'
    },

    properties: {
      autocenter: true,
      deadband: 0.1,
      color: 'white'
    },
  
    setup: function(component) {
      component.pad = component.root.getElementsByClassName('pad')[0];
      component.stick = component.root.getElementsByClassName('stick')[0];
      component.lastMove = Date.now();
    },
  
    draw: function(component) {
      const jstyle = getComputedStyle(component.root);
      const jwidth = parseInt(jstyle.width);
      const jheight = parseInt(jstyle.height);
    
      component.pad.setAttribute('width', jwidth);
      component.pad.setAttribute('height', jheight);

      const sstyle = getComputedStyle(component.stick);
      const stop = (jheight - parseInt(sstyle.height)) / 2;
      const sleft = (jwidth - parseInt(sstyle.width)) / 2;
    
      component.stick.style.top = stop + 'px';
      component.stick.style.left = sleft + 'px';
    
      component.animate = {
        endtimer: null,
        movetimer: null,
        touchstart: { x: null, y: null },
        target: { left: sleft, top: stop },
        current: { left: sleft, top: stop },
        step: { left: null, top: null, count: null }
      };

      const LINE_WIDTH = 5;
      const STICK_RADIUS = 50;

      let ctx = component.pad.getContext('2d');
      ctx.lineWidth = LINE_WIDTH;
      ctx.strokeStyle = component.properties.color || 'white';

      ctx.beginPath();
      ctx.arc(jwidth / 2, jwidth / 2, jwidth / 2 - LINE_WIDTH, Math.PI, Math.PI * 2);
      ctx.stroke();

      ctx.beginPath();
      ctx.moveTo(LINE_WIDTH, jwidth / 2);
      ctx.lineTo(LINE_WIDTH, jheight - jwidth / 2);
      ctx.moveTo(jwidth - LINE_WIDTH, jwidth / 2);
      ctx.lineTo(jwidth - LINE_WIDTH, jheight - jwidth / 2);
      ctx.stroke();

      ctx.beginPath();
      ctx.arc(jwidth / 2, jheight - jwidth / 2, jwidth / 2 - LINE_WIDTH, Math.PI * 2, Math.PI);
      ctx.stroke();

      ctx = component.stick.getContext('2d');
      ctx.lineWidth = LINE_WIDTH;
      ctx.strokeStyle = component.properties.color || 'white';

      ctx.beginPath();
      ctx.arc(STICK_RADIUS, STICK_RADIUS, STICK_RADIUS - LINE_WIDTH, 0, Math.PI * 2);
      ctx.stroke();
    },
  
    enable: function(component) {
      const animate = component.animate;
      const active = animate.target.top;
      const stick = component.stick;

      function getStickPos() {
        const style = getComputedStyle(stick);
        let y = animate.target.top - parseInt(style.top);
        y = Math.max(-active, Math.min(y, active)) / active;

        if (Math.abs(y) < component.properties.deadband) {
          y = 0;
        }
        return y;
      }

      function moveStick(force) {
        const now = Date.now();
        if (now - component.lastMove > 100 || force) {
          component.lastMove = now;
          component.publish_position({ y: getStickPos() });
        }
      }

      stick.addEventListener('touchstart', function(e) {
        if (animate.endtimer) {
          clearInterval(animate.endtimer);
          animate.endtimer = null;
        }
        if (animate.movetimer) {
          clearInterval(animate.movetimer);
        }
        animate.movetimer = setInterval(function() {
          moveStick(false);
        }, 100);
        animate.touchstart.y = e.changedTouches[0].clientY;
        const style = getComputedStyle(stick);
        animate.current.top = parseInt(style.top);
      });

      stick.addEventListener('touchend', function(e) {
        if (animate.movetimer) {
          clearInterval(animate.movetimer);
          animate.movetimer = null;
        }
        if (!animate.endtimer && (component.properties.autocenter || getStickPos() == 0)) {
          var style = getComputedStyle(stick);
          animate.current.top = parseInt(style.top);
          animate.step.count = 20;
          animate.step.top = (animate.current.top - animate.target.top) / animate.step.count;
          animate.endtimer = setInterval(function() {
            if (--animate.step.count) {
              animate.current.top -= animate.step.top;
              stick.style.top = animate.current.top + 'px';
              moveStick(false);
            }
            else {
              animate.current.top = animate.target.top;
              stick.style.top = animate.target.top + 'px';
              clearInterval(animate.endtimer);
              animate.endtimer = null;
              moveStick(true);
            }
          }, 10);
        }
      });

      stick.addEventListener('touchmove', function(e) {
        const touch = { y: e.changedTouches[0].clientY - animate.touchstart.y };
        const style = getComputedStyle(stick);
        stick.style.top = (animate.current.top + touch.y) + 'px';
        moveStick(false);
      });
    },

    action: actionHelper
  }

  UI.TYPE.camera = {
    html: `<div class="camera">
      ${MOVE_RESIZE_HTML}
      <img class="camera_image" src="" onload="this.style.visibility=''" onerror="this.style.visibility='hidden'"/>
    </div>`,

    css: `
      .camera { min-width: 200px; min-height: 100px; z-index: 0; }
      .camera .camera_image { width: 100%; height: 100%; object-fit: contain; }
      .camera .camera_image.cover { object-fit: cover; }
      .editing .camera_outline { position: absolute; top: 3px; bottom: 3px; left: 3px; right: 3px; border: 1px solid grey; color: grey; font: 1.5em ${FONT_DEFAULT}; display: flex; align-items: center; justify-content: center; }
    `,

    title: 'Camera',
    icon: `Cam`,

    schemas: {
      events: {
        tap: { value: 'Boolean' }
      },
    },

    config: {
      name: 'Camera'
    },

    properties: {
      number: 0,
      letterbox: true
    },
  
    setup: function(component) {
      component.img = component.root.querySelector('.camera_image');
      if (UI.mode === 'edit') {
        const show = document.createElement('div');
        show.className = 'camera_outline';
        show.innerText = 'camera';
        component.root.appendChild(show);
      }
    },
  
    draw: function(component) {
      if (UI.mode !== 'edit') {
        component.img.src = `/camera/${component.properties.number}`;
      }
      component.img.classList.toggle('cover', !component.properties.letterbox);
    },
  
    enable: function(component) {
      component.root.addEventListener('touchstart', function() {
        component.publish_tap({ value: true });
      });
      component.root.addEventListener('touchend', function() {
        component.publish_tap({ value: false });
      });
    },

    action: actionHelper
  }

  UI.TYPE.meter = {
    html: `<div class="meter">
      ${MOVE_RESIZE_HTML}
      <div class="meter_body">
        <div class="barlabel"></div>
        <div class="level"><div class="bar"></div><div class="space"></div></div>
      </div>
    </div>`,

    css: `
      .meter { min-width: 50px; min-height: 10px; z-index: 0; }
      .meter .meter_body { display: flex; height: 100%; }
      .meter .level { display: flex; flex: 1; height: 100%; background-image: linear-gradient(to right, red, green 50%); text-align: left; }
      .meter .level.reverse { background-image: linear-gradient(to right, green 50%, red); }
      .meter .level .bar { display: inline-block; height: 100%; width: 100%; float: left; background: none; }
      .meter .level .space { display: inline-block; height: 100%; flex-grow: 1; background-color: black; }
      .meter .barlabel { display: flex; align-items: center; font: ${FONT_DEFAULT_AND_SIZE}; color: white; box-sizing: border-box; padding-right: 6px; }
      .meter .barlabel:empty { padding-right: 0px; }
      .editing .meter_outline { position: absolute; top: 3px; bottom: 3px; left: 3px; right: 3px; border: 1px solid grey; color: grey; font: 0.5em ${FONT_DEFAULT}; display: flex; align-items: center; justify-content: center; }
    `,

    title: 'Bar',
    icon: `Bar`,

    schemas: {
    },

    config: {
      name: 'Bar',
      font: FONT_DEFAULT_AND_SIZE,
      reverse: false,
      'label size': ''
    },

    properties: {
      label: '',
      level: 100,
      color: 'white'
    },
  
    setup: function(component) {
      component.bar = component.root.querySelector('.bar');
      component.label = component.root.querySelector('.barlabel');
      component.level = component.root.querySelector('.level');
    },
  
    draw: function(component) {
      component.bar.style.width = `${Math.min(Math.max(0, component.properties.level), 100)}%`;
      component.label.innerText = component.properties.label;
      component.label.style.color = component.properties.color;
      component.label.style.font = component.properties.font;
      component.label.style.width = component.properties['label size'];
      component.level.classList.toggle('reverse', component.properties.reverse);
    },

    enable: function() {
    },

    action: actionHelper
  }

  UI.TYPE.text = {
    html: `<div class="ui_text">
      ${MOVE_RESIZE_HTML}
      <div class="text"></div>
    </div>`,

    css: `
      .ui_text { min-width: 20px; min-height: 10px; }
      .text { display: flex; align-items: center; height: 100%; width: 100%; font: ${FONT_DEFAULT_AND_SIZE}; text-align: left; color: white; }
    `,

    title: 'Text',
    icon: `Txt`,

    schemas: {
    },

    config: {
      name: 'Text',
      font: FONT_DEFAULT_AND_SIZE
    },

    properties: {
      text: '...',
      color: 'white'
    },
  
    setup: function(component) {
      component.text = component.root.querySelector('.text');
    },
  
    draw: function(component) {
      component.text.innerText = component.properties.text;
      component.text.style.font = component.properties.font;
      component.text.style.color = component.properties.color;
    },

    enable: function() {
    },

    action: actionHelper
  }

  UI.TYPE.pushbutton = {
    html: `<div class="pushbutton">
      ${MOVE_RESIZE_HTML}
      <button class="button"></button>
    </div>`,

    css: `
      .pushbutton { min-width: 50px; min-height: 50px; }
      .pushbutton .button { -webkit-tap-highlight-color: white; -webkit-appearance: none; background: none; color: white; font: ${FONT_DEFAULT_AND_SIZE}; border: 2px solid white; border-radius: 10px; height: 100%; width: 100%; }
    `,

    title: 'Push Button',
    icon: `But`,

    schemas: {
      events: {
        tap: { value: 'Boolean' }
      }
    },

    config: {
      name: 'Button',
      font: FONT_DEFAULT_AND_SIZE
    },

    properties: {
      text: '...',
      color: 'white'
    },

    setup: function(component) {
      component.button = component.root.querySelector('.button');
    },

    draw: function(component) {
      component.button.innerText = component.properties.text;
      component.button.style.font = component.properties.font;
      component.button.style.color = component.properties.color;
      component.button.style.borderColor = component.properties.color;
      component.button.style.WebkitTapHighlightColor = component.properties.color;
    },

    enable: function(component) {
      component.root.addEventListener('touchstart', function() {
        component.publish_tap({ value: true });
      });
      component.root.addEventListener('touchend', function() {
        component.publish_tap({ value: false });
      });
    },

    action: actionHelper
  }

  UI.TYPE.toggle = {
    html: `<div class="togglebutton">
      ${MOVE_RESIZE_HTML}
      <button class="button"></button>
    </div>`,

    css: `
      .togglebutton { min-width: 50px; min-height: 50px; }
      .togglebutton .button { -webkit-appearance: none; background: none; color: white; font: ${FONT_DEFAULT_AND_SIZE}; border: 2px solid white; border-radius: 10px; height: 100%; width: 100%; }
      .togglebutton .button.active { background-color: white; color: black; }
      `,

    title: 'Toggle Button',
    icon: `Tog`,

    schemas: {
      events: {
        tap: { value: 'Boolean' }
      }
    },

    config: {
      name: 'Button',
      font: FONT_DEFAULT_AND_SIZE
    },

    properties: {
      text: '...',
      active: false,
      color: 'white'
    },
  
    setup: function(component) {
      component.button = component.root.querySelector('.button');
    },
  
    draw: function(component) {
      component.button.classList.toggle('active', component.properties.active);
      component.button.innerText = component.properties.text;
      component.button.style.font = component.properties.font;
      component.button.style.color = component.properties.active ? '' : component.properties.color;
      component.button.style.borderColor = component.properties.color;
      component.button.style.backgroundColor = component.properties.active ? component.properties.color : '';
    },

    enable: function(component) {
      component.root.addEventListener('touchstart', function() {
        component.properties.active = !component.properties.active;
        component.publish_tap({ value: component.toggle_value });
        component.type.draw(component);
      });
    },

    action: actionHelper
  }

  UI.TYPE.xboxgamepad = {
    html: `<div class="xgamepad">
      ${MOVE_HTML}
    </div>`,

    css: `
      .xgamepad { display: none; }
      .editing.xgamepad { display: block; min-width: 50px; min-height: 50px; }
    `,

    title: 'XBox Controller',
    icon: `Xbx`,

    schemas: {
      events: {
        // Buttons
        a: { pressed: 'Boolean', value: 'Number' },
        b: { pressed: 'Boolean', value: 'Number' },
        x: { pressed: 'Boolean', value: 'Number' },
        y: { pressed: 'Boolean', value: 'Number' },
        leftTop: { pressed: 'Boolean', value: 'Number' },
        rightTop: { pressed: 'Boolean', value: 'Number' },
        leftTrigger: { pressed: 'Boolean', value: 'Number' },
        rightTrigger: { pressed: 'Boolean', value: 'Number' },
        select: { pressed: 'Boolean', value: 'Number' },
        start: { pressed: 'Boolean', value: 'Number' },
        leftStick: { pressed: 'Boolean', value: 'Number' },
        rightStick: { pressed: 'Boolean', value: 'Number' },
        dpadUp: { pressed: 'Boolean', value: 'Number' },
        dpadDown: { pressed: 'Boolean', value: 'Number' },
        dpadLeft: { pressed: 'Boolean', value: 'Number' },
        dPadRight: { pressed: 'Boolean', value: 'Number' },
        // Joysticks
        leftStick: { x: 'Number', y: 'Number' },
        rightStick: { x: 'Number', y: 'Number' },
        dpad: { x: 'Number', y: 'Number' },
      }
    },

    config: {
      name: 'Gamepad',
      index: 0
    },

    properties: {
    },
  
    setup: function(component) {
    },
  
    draw: function(component) {
    },

    enable: function(component) {
    
      const buttonNames = [
        'a',
        'b',
        'x',
        'y',
        'leftTop',
        'rightTop',
        'leftTrigger',
        'rightTrigger',
        'select',
        'start',
        'leftStick',
        'rightStick',
        'dpadUp',
        'dpadDown',
        'dpadLeft',
        'dpadRight'
      ];

      let timer = null;
      window.addEventListener('gamepadconnected', function(e) {
        const gamepad = e.gamepad;
        if (gamepad.index !== component.properties.index) {
          return;
        }
        if (!timer) {
          const states = [];
          const jstates = [ { x: null, y: null }, { x: null, y: null }];
          timer = setInterval(function() {
            if (!gamepad.connected) {
              clearInterval(timer);
              timer = null;
              return;
            }

            function buttonState(idx) {
              const button = (gamepad.buttons[idx] || (gamepad.buttons[idx] = { button: null, value: null }));
              const state = states[idx];
              if (typeof button === 'object') {
                if (button.pressed !== state.pressed || button.value !== state.value) {
                  state.pressed = button.pressed;
                  state.value = button.value;
                  return state;
                }
                else {
                  return null;
                }
              }
              else {
                return { pressed: button == 1, value: button };
                if (button != state.value) {
                  state.pressed = button == 1;
                  state.value = button;
                  return state;
                }
                else {
                  return null;
                }
              }
            }

            const events = [];
            for (let i = 0; i < buttonNames.length; i++) {
              events[i] = buttonState(i);
            }
            // Generate button events if they're different
            for (let i = 0; i < buttonNames.length; i++) {
              if (events[i]) {
                component[`publish_${buttonNames[i]}`](events[i]);
              }
            }

            // Update the DPAD pseudo-joystick if it has changed
            if (events[12] || events[13] || events[14] || events[15]) {
              component.publish_dpad({ 
                x: (states[14].pressed ? -1 : 0) + (states[15].pressed ? 1 : 0),
                y: (states[12].pressed ? -1 : 0) + (states[13].pressed ? 1 : 0)
              });
            }

            // Update the joysticks if they changed
            if (gamepad.axes[0] !== jstates[0].x || gamespad.axes[1] !== jstates[0].y) {
              jstates[0].x = gamepad.axes[0];
              jstates[0].y = gamepad.axes[1];
              component.publish_leftStick(jstates[0]);
            }
            if (gamepad.axes[2] !== jstates[1].x || gamespad.axes[3] !== jstates[1].y) {
              jstates[1].x = gamepad.axes[2];
              jstates[1].y = gamepad.axes[3];
              component.publish_leftStick(jstates[1]);
            }
          }, 100);
        }
      });
    },

    action: actionHelper
  }

  UI.TYPE.logger = {
    html: `<div class="logger">
      ${MOVE_RESIZE_HTML}
      <div class="lines"></div>
    </div>`,

    css: `
      .logger { min-width: 50px; min-height: 50px; }
      .logger .lines { color: white; font: ${FONT_DEFAULT_AND_SIZE}; text-align: left; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }
    `,

    title: 'Logger',
    icon: `Log`,

    schemas: {
      action: { print: 'String' }
    },

    config: {
      name: 'Log',
      'nr lines': 3,
      font: FONT_DEFAULT_AND_SIZE,
      color: 'white'
    },

    properties: {
    },
  
    setup: function(component) {
      component.lines = component.root.querySelector('.lines');
    },
  
    draw: function(component) {
      component.lines.style.font = component.properties.font;
      component.lines.style.color = component.properties.color;
      while (component.lines.firstElementChild) {
        component.lines.firstElementChild.remove();
      }
      const len = component.properties['nr lines'];
      for (let i = 0; i < len; i++) {
        const text = document.createElement('div');
        component.lines.appendChild(text);
        if (UI.mode === 'edit') {
          text.innerText = '...';
        }
      }
    },

    enable: function(component) {
    },

    action: function(component, request) {
      if (request.print) {
        const len = component.properties['nr lines'];
        const children = component.lines.children;
        for (let i = 1; i < len; i++) {
          children[i - 1].innerText = children[i].innerText;
        }
        component.lines.lastElementChild.innerText = request.print;
      }
    }
  }

  // -------------------------------

}
