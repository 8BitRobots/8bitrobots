function motor(app, name, config)
{
  const node = Node.init(`/app/motor/${name}/node`);

  const config =
  {
    channel: 'channel' in params ? params.channel : null,
    rev: 'rev' in params ? params.rev : false,
    min: 'min' in params ? params.min : -1,
    max: 'max' in params ? params.max : 1
  }
  const state =
  {
    time: 0,
    func: null,
    duty: null,
    velocity: 0
  };
  const set_duty = config.channel ? app.getService(config.channel) : function(){};

  return function(args)
  {
    for (key in state)
    {
      if (args[key] !== undefined)
      {
        state[key] = args[key];
      }
    }

    let velocity = config.rev ? -state.velocity : state.velocity;
    velocity = Math.max(Math.min(velocity, config.max), config.min);

    const nduty = 1.5 + velocity * 0.5;
    if (nduty !== state.duty)
    {
      // Duty has changed, set the new duty output
      state.duty = nduty;
      set_duty({ duty: state.duty, time: state.time, func: state.func });
      app.update();
    }

    return state;
  }
}

module.exports = motor;
