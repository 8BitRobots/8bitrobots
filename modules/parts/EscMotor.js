function motor(app, name, params)
{
  const node = Node.init(`/app/esc-motor/${name}/node`);

  const config =
  {
    channel: 'channel' in params ? params.channel : null,
    rev: 'rev' in params ? params.rev : false,
    lowneutral: 'lowneutral' in params ? params.lowneutral : 1.5,
    highneutral: 'highneutral' in params ? params.highneutral : 1.5,
    maxforward: 'max' in params ? params.max : 2.0,
    maxbackward: 'min' in params ? params.min : 1.0,
    rate: 'rate' in params ? params.rate : 500,
    transition: 'transition' in params ? params.transition : 500,
  }
  const state =
  {
    time: 0,
    func: null,
    pulse: null,
    velocity: 0
  };
  const set_pulse = config.channel ? app.getService(config.channel) : function(){};
  const range = Math.abs(config.maxforward - config.maxbackward);
  const neutral = (config.lowneutral + config.highneutral) / 2;
  const running = { velocity: null, id: null };

  return function(args)
  {
    for (key in state)
    {
      if (args[key] !== undefined)
      {
        state[key] = args[key];
      }
    }

    // If current change hasn't expired, we cannot make a new one. We leave the updated
    // velocity in the state so it will be applied later.
    if (running.id)
    {
      return state;
    }

    const update = () => {

      // If velocity isn't changed, we do nothing.
      if (running.velocity === state.velocity)
      {
        return state;
      }

      let velocity = config.rev ? -state.velocity : state.velocity;
      velocity = Math.max(Math.min(velocity, 1), -1);

      // Update the pulse for the new velocity
      if (velocity === 0)
      {
        state.pulse = neutral;
      }
      else if (velocity > 0)
      {
        state.pulse = config.highneutral + velocity * (config.maxforward - config.highneutral);
      }
      else
      {
        state.pulse = config.lowneutral + velocity * (config.lowneutral - config.maxbackward);
      }

      // Calculate the minimum speed the pulse can change.
      let changeTime = Math.abs(running.velocity - state.velocity) * config.rate;
      // But allow it to be overridden
      if (state.time && changeTime < state.time)
      {
        changeTime = state.time;
      }

      // If the update crosses the neutral point, we pause momentarily there
      if (Math.sign(running.velocity) !== Math.sign(state.velocity))
      {
        let slowTime = Math.abs(running.velocity);
        let highTime = Math.abs(state.velocity);
        slowTime = changeTime * slowTime / (slowTime + highTime);
        highTime = changeTime * highTime / (slowTime + highTime);
        if (slowTime > 0)
        {
          set_pulse({ pulse: neutral, time: slowTime, func: state.func });
          set_pulse({ pulse: neutral, time: config.transition, func: 'linear' });
        }
        set_pulse({ pulse: state.pulse, time: highTime, func: state.func });
      }
      else
      {
        // Update the pulse
        set_pulse({ pulse: state.pulse, time: changeTime, func: state.func });
      }

      // Update the running state so we don't apply the next change until it's allowed.
      running.velocity = state.velocity;
      running.id = setTimeout(() => {
        running.id = null;
        update();
      }, changeTime);

      app.update();

      return state;
    }

    return update();
  }
}

module.exports = motor;
