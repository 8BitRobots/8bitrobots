function car(app, name, config)
{
  const state =
  {
    x: 0,
    y: 0,
    velocity: 0,
    angle: 0,
  };

  return function(args)
  {
    for (key in state)
    {
      if (args[key] !== undefined)
      {
        state[key] = args[key];
      }
    }
 
    let strafe = Math.min(Math.max(state.x || 0, -1), 1);
    let forward = Math.min(Math.max(state.y || 0, -1), 1);;

    // Make these ramp up for better slow speed control
    forward = (forward < 0 ? -1 : 1) * (forward * forward);
    strafe = (strafe < 0 ? -1 : 1) * (strafe * strafe);

    const angle = Math.atan2(strafe, 1) / Math.PI * 180;

    if (angle !== state.angle || forward !== state.velocity)
    {
      state.angle = angle;
      state.velocity = forward;

      app.update();
    }

    return state;
  }
}

module.exports = car;
