const StateManager = require('modules/state-manager');

function servo(app, name, params)
{
  const node = Node.init(`/app/servo/${name}/node`);
  const saved = new StateManager({ name: name });

  const config =
  {
    channel: 'channel' in params ? params.channel : null,
    rev: 'rev' in params ? params.rev : false,
    neutral: 'neutral' in params ? params.neutral : 1500,
    usdeg: 'usdeg' in params ? params.usdeg : 5.556,
    trim: 'trim' in params ? params.trim : 0,
    cw: 'cw' in params ? params.cw : -90,
    ccw: 'ccw' in params ? params.ccw : 90,
    persist: 'persist' in params ? params.persist : true,
  };
  const state =
  {
    angle: params.angle || 0,
    time: null,
    vstart: null,
    vend: null,
    func: null,
    pulse: null
  };
  if (config.persist)
  {
    const angle = saved.get('angle');
    if (angle !== undefined)
    {
      state.angle = angle;
    }
  }

  const set_pulse = config.channel ? app.getService(config.channel) : function(){};

  return function(args)
  {
    for (key in state)
    {
      if (args[key] !== undefined)
      {
        state[key] = args[key];
      }
    }
    if (config.persist && 'angle' in args)
    {
      saved.set('angle', args.angle);
    }

    // Calculate the actual angle based on the servo config.
    let angle = (config.rev ? -state.angle : state.angle) + config.trim
    angle = Math.max(Math.min(angle, config.ccw), config.cw);
    
    // Calculate the pulse (in ms)
    const npulse = (config.neutral - config.usdeg * angle) / 1000;
    if (npulse !== state.pulse)
    {
      // Pulse has changed, set the new pulse output
      state.pulse = npulse;
      set_pulse({ pulse: state.pulse, time: state.time, vstart: state.vstart, vend: state.vend, func: state.func });
      app.update();
    }

    return state;
  }
}

module.exports = servo;
