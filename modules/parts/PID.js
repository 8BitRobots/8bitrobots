const PID = require('../pid');

function pid(app, name, config)
{
  const state =
  {
    setpoint: 0,
    input: 0,
    output: 0,
  };

  const aPid = PID(
  {
    type: config.type || 'linear',
    Kp: config.P || 1,
    Ki: config.I || 0,
    Kd: config.D || 0,
    outMin: config.min || Number.MIN_SAFE_INTEGER,
    outMax: config.max || Number.MAX_SAFE_INTEGER,
    outNeutralMin: config.minNeutral || 0,
    outNeutralMax: config.maxNeutral || 0
  });

  return function(args)
  {
    if (typeof args.setpoint === 'number' && !isNaN(args.setpoint))
    {
      state.setpoint = args.setpoint;
      aPid.setSetpoint(state.setpoint);
      app.update();
    }
    if (typeof args.input === 'number' && !isNaN(args.input))
    {
      state.input = args.input;
      state.output = aPid(state.input);
      app.update();
    }
    return state;
  }
}

module.exports = pid;
