const IK = require('fullik');

function ikchain(app, name, params)
{
  const chain = new IK.Chain3D();

  const state =
  {
    error: 0,
    angles: []
  };

  const bones = params.bones;

  function calculateHingeReferenceAxis(boneIndex)
  {
    const bone = bones[boneIndex];
    // Attempt to find the hingeReferenceAxis. This is the center point around which the hinge rotate and the angle
    // of rotation is measured from. We calculate this by first finding a vector which is perpendicular to both the hinge
    // rotation axis and the direction of the bone (the reference vector should be in the same plane as the bone and perpendicular
    // to the hinge) and then finding a vector which is parallel to the hinge and the vector we first calculated.
    const boneDirection = new IK.V3(bone.direction.x, bone.direction.y, bone.direction.z).normalize();
    const hingeAxis = new IK.V3(bone.hingeAxis.x, bone.hingeAxis.y, bone.hingeAxis.z).normalize();
    const perpendicularToHingeAxisAndBoneDirection = hingeAxis.cross(boneDirection);
    if (perpendicularToHingeAxisAndBoneDirection.length() >= 0.01)
    {
      return perpendicularToHingeAxisAndBoneDirection.cross(hingeAxis);
    }
    // If the perpendicular vector length is (essentially) 0, then the hingeAxis and boneDirection were parallel which means we
    // cannot use them to determine where the hingeReferenceAxis will be.
    // Instead we'll use the direction of the next hinge to set a reference point. We find a vector which is perpendicular to the
    // current bone and the next hinge axis. This will point in a direction which seems like a logical center point for the current
    // bone's rotation.
    const nextBone = bones[boneIndex + 1];
    if (nextBone)
    {
      const nextHingeAxis = new IK.V3(nextBone.hingeAxis.x, nextBone.hingeAxis.y, nextBone.hingeAxis.z).normalize();
      const axis = hingeAxis.cross(nextHingeAxis);
      // If this axis is length 0, then this won't work either.
      if (axis.length() >= 0.01)
      {
        return axis;
      }
    }
    // At this point we're out of ideas.
    return new IK.V3();
  }

  bones.forEach((bone, boneIndex) => {
    switch (bone.type)
    {
      case 'HingeBone':
        const start = chain.numBones == 0 ? new IK.V3() : chain.getEffectorLocation();
        const boneDirection = new IK.V3(bone.direction.x, bone.direction.y, bone.direction.z).normalize();
        const hingeAxis = new IK.V3(bone.hingeAxis.x, bone.hingeAxis.y, bone.hingeAxis.z).normalize();
        const hingeReferenceAxis = calculateHingeReferenceAxis(boneIndex);
        const ikbone = new IK.Bone3D(
          start,
          null,
          boneDirection,
          bone.length
        );
        ikbone.joint.setHinge(
          IK.J_LOCAL,
          hingeAxis,
          -bone.cwLimit, 
          bone.ccwLimit,
          hingeReferenceAxis
        );
        chain.addBone(ikbone);
        if (chain.numBones === 1)
        {
          chain.setHingeBaseboneConstraint('global', hingeAxis, -bone.cwLimit, bone.ccwLimit, hingeReferenceAxis);
        }
        break;
      case 'Rotor':
      default:
        break;
    }
  });

  const len = chain.getLiveChainLength();
  chain.setSolveDistanceThreshold(len / 500);
  chain.setMaxIterationAttempts(chain.numBones * 25);
  chain.setMinIterationChange(len / 500);

  const mtx = new IK.M3();

  return function(args)
  {
    if ('target' in args)
    {
      state.error = chain.solveForTarget(args.target);
      const bones = chain.bones;
      for (let i = 0; i < chain.numBones; i++)
      {
        let hingeRotationAxis = bones[i].joint.getHingeRotationAxis();
        let hingeReferenceAxis = bones[i].joint.getHingeReferenceAxis();
        switch (bones[i].joint.type)
        {
          case IK.J_LOCAL:
          {
            if (i === 0)
            {
              hingeRotationAxis = chain.baseboneRelativeConstraintUV;
              hingeReferenceAxis = chain.baseboneRelativeReferenceConstraintUV;
            }
            else
            {
              const prevBoneInnerToOuterUV = bones[i-1].getDirectionUV();
              mtx.createRotationMatrix(prevBoneInnerToOuterUV);
              hingeRotationAxis = hingeRotationAxis.clone().applyM3(mtx);
              hingeReferenceAxis = hingeReferenceAxis.clone().applyM3(mtx);
            }
            break;
          }
          case IK.J_GLOBAL:
            break;
          default:
            throw new Error('Joint type not supported yet');
        }
        const boneInnerToOuterUV = bones[i].getDirectionUV();
        boneInnerToOuterUV.projectOnPlane(hingeRotationAxis);
        const angle = hingeReferenceAxis.getSignedAngle(boneInnerToOuterUV, hingeRotationAxis);
        state.angles[i] = angle / Math.PI * 180;
      }
    }
    return state;
  }
}

module.exports = ikchain;
