function tank(app, name, config)
{
  const state =
  {
    x: 0,
    y: 0,
    left: 0,
    right: 0,
  };

  return function(args)
  {
    for (key in state)
    {
      if (args[key] !== undefined)
      {
        state[key] = args[key];
      }
    }

    let strafe = Math.min(Math.max(state.x || 0, -1), 1);
    // Make these ramp up for better slow speed control
    strafe = 0.75 * (strafe < 0 ? -1 : 1) * (strafe * strafe);
  
    let forward = Math.min(Math.max(state.y || 0, -1), 1);
    // Make these ramp up for better slow speed control
    forward = (forward < 0 ? -1 : 1) * (forward * forward);

    const right = Math.min(Math.max(forward - strafe, -1), 1);
    const left = Math.min(Math.max(forward + strafe, -1), 1);

    if (left !== state.left || right !== state.right)
    {
      state.left = left;
      state.right = right;

      app.update();
    }

    return state;
  }
}

module.exports = tank;
