function motor(app, name, params)
{
  const node = Node.init(`/app/continuous-servo/${name}/node`);

  const config =
  {
    channel: 'channel' in params ? params.channel : null,
    rev: 'rev' in params ? params.rev : false,
    lowneutral: 'lowneutral' in params ? params.lowneutral : 1500,
    highneutral: 'highneutral' in params ? params.highneutral : 1500,
    maxbackward: 'min' in params ? params.min : 1000,
    maxforward: 'max' in params ? params.max : 2000
  }
  const state =
  {
    time: 0,
    func: null,
    pulse: null,
    velocity: 0,
  };
  const set_pulse = config.channel ? app.getService(config.channel) : function(){};

  return function(args)
  {
    for (key in state)
    {
      if (args[key] !== undefined)
      {
        state[key] = args[key];
      }
    }

    let velocity = config.rev ? -state.velocity : state.velocity;
    velocity = Math.max(Math.min(velocity, 1), -1);

    let npulse;
    if (velocity === 0)
    {
      npulse = (config.lowneutral + config.highneutral) / 2;
    }
    else if (velocity > 0)
    {
      npulse = config.highneutral + velocity * (config.maxforward - config.highneutral);
    }
    else
    {
      npulse = config.lowneutral + velocity * (config.lowneutral - config.maxbackward);
    }

    if (npulse !== state.pulse)
    {
      // Pulse has changed, set the new pulse output
      state.pulse = npulse;
      set_pulse({ pulse: state.pulse, time: state.time, func: state.func });
      app.update();
    }

    return state;
  }
}

module.exports = motor;
