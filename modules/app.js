'use strict';

console.info('Loading App Container.');

const VM = require('vm');
const FS = require('fs');
const ConfigManager = require('modules/config-manager');

const INSTANCES = './saved/component_instances.js';

function app(config)
{
  this._name = config.name;
  this._node = Node.init(this._name);
  this._config = new ConfigManager(this,
  {
    source: config.source || '',
    code: config.code || '',
    ui: config.ui || ''
  });
  this._enabled = 0;
  
  if (!FS.existsSync(INSTANCES)) {
    FS.writeFileSync(INSTANCES, '');
  }

  this._config.enable();
}

app.prototype =
{
  enable: function()
  {
    if (this._enabled++ === 0)
    {
      this._enable();
    }
    return this;
  },

  _enable: function()
  {
    this._proxies = {};
    this._topics = {};
    this._topicQ = [];
    this._activitiesPending = {};
    this._services = {};
    this._setups = [];
    this._activities = [];
    this._configurations = [];
    this._parts = {};
    this._generateUI();
    try
    {
      //console.log('Deploying code', this._config.get('code'));
      const code = this._config.get('code');
      VM.runInNewContext(
        code,
        {
          App:
          {
            registerSetup: (setup) => { this._registerSetup(setup); },
            registerActivity: (activityId, fn) => { this._registerActivity(activityId, fn); },
            registerConfiguration: (configuration) => { this._registerConfiguration(configuration); },
            run: () => { this._runApp(); },
            subscribeTopic: (activity, topicName) => { this._subscribeToTopic(activity, topicName); },
            subscribePart: (activity, partName, partInstance) => { this._subscribeToPart(activity, partName, partInstance); },
            subscribeTimer: (activity, name, time) => { this._subscribeToTimer(activity, name, time); }
          }
        }
      );
    }
    catch (e)
    {
      console.error(e);
    }
  },
  
  disable: function()
  {
    if (--this._enabled === 0)
    {
      this._disable();
    }
    return this;
  },

  _disable: function()
  {
    this._unsubscribeAllTopic();
    const pending = this._activitiesPending;
    this._activitiesPending = {};
    for (let id in pending)
    {
      pending[id] && pending[id]({});
    }
  },

  reconfigure: function(changes)
  {
    if (this._enabled && (changes.code || changes.ui))
    {
      this._disable();
      this._enable();
    }
  },

  _generateUI: function()
  {
    try
    {
      const ui = this._config.get('ui');
      FS.writeFileSync(INSTANCES, `UI.component_instances=${JSON.stringify(ui)}`);
    }
    catch (e)
    {
      console.error(e);
    }
  },

  _registerSetup: function(setup)
  {
    const activity =
    {
      service: (serviceName, options) => { return this._getService(serviceName, options); },
      part: (partName, instanceName, args) => { return this._partProcess(partName, instanceName, args); },
      print: (msg) => { this._debugMessage(msg); }
    };
    this._setups.push(() => {
      setup(activity);
    });
  },

  _registerActivity: function(activityId, fn)
  {
    const activity =
    {
      sync: () => { return this._syncTopicUpdates(activityId); },
      get: (topicName, propertyName) => { return this._getTopic(activityId, topicName, propertyName); },
      service: (serviceName, options) => { return this._getService(serviceName, options); },
      part: (partName, instanceName, args) => { return this._partProcess(partName, instanceName, args); },
      timer: (timerName) => { return this._getTimer(activityId, timerName); },
      print: (msg) => { this._debugMessage(msg); },
      terminated: () => { return !(activityId in this._activitiesPending); }
    };
    this._activitiesPending[activityId] = null;
    this._activities.push(async () => {
      await fn(activity);
      delete this._activitiesPending[activityId];
    });
  },

  _registerConfiguration: function(configuration)
  {
    const activity =
    {
      partConfig: (partName, instanceName, config) => { return this._partConfig(partName, instanceName, config); },
      service: (serviceName, options) => { return this._getService(serviceName, options); }
    };
    this._configurations.push(() => {
      configuration(activity);
    });
  },

  _runApp: function()
  {
    Promise.all(this._configurations.map((config) => {
      return config();
    })).then(() => {
      return Promise.all(this._setups.map((setup) => {
        return setup();
      }));
    }).then(() => {
      this._activities.forEach((activity) => {
        setImmediate(activity);
      });
    });
  },

  _getTopic: function(activityId, topicName, propertyName)
  {
    if (activityId)
    {
      const topic = this._topics[topicName];
      if (topic)
      {
        const ainfo = topic.activities[activityId];
        if (ainfo)
        {
          if (propertyName in ainfo.state)
          {
            return ainfo.state[propertyName];
          }
          else if (propertyName === undefined)
          {
            return ainfo.state;
          }
        }
      }
    }
    throw new Error(`Undefined topic property: ${topicName}: ${propertyName}`);
  },

  _subscribeToTopic: function(activity, topicName)
  {
    if (!activity)
    {
      return;
    }
    else if (!this._topics[topicName])
    {
      const info =
      {
        activities:
        {
          [activity]: { state: {}, callback: null, version: 0 }
        },
        state: {},
        version: 1
      };
      this._topics[topicName] = info;
    
      this._node.subscribe({ topic: topicName }, (event) => {
        Object.assign(info.state, event);
        info.version++;
        for (let id in info.activities)
        {
          const callback = this._activitiesPending[id];
          if (callback)
          {
            this._activitiesPending[id] = null;
            callback({});
          }
        }
      });
    }
    else
    {
      this._topics[topicName].activities[activity] = { state: {}, callback: null, version: 0 };
    }
  },

  _unsubscribeAllTopic: function()
  {
    for (let topic in this._topics)
    {
      this._node.unsubscribe({ topic: topic });
      const activities = this._topics[topic].activities;
      for (let id in activities)
      {
        if (activities[id].timeout)
        {
          clearTimeout(activities[id].timeout);
        }
        if (activities[id].interval)
        {
          clearInterval(activities[id].interval);
        }
      }
    }
    this._topics = {};
  },

  _syncTopicUpdates: function(id)
  {
    return new Promise((resolve) => {
      setImmediate(() => {
        if (!(id in this._activitiesPending))
        {
          resolve(false);
        }
        else
        {
          let success = false;
          for (let topicName in this._topics)
          {
            const topic = this._topics[topicName];
            const activity = topic.activities[id];
            if (activity && activity.version != topic.version)
            {
              Object.assign(activity.state, topic.state);
              activity.version = topic.version;
              success = true;
              activity.restartTimeout && activity.restartTimeout();
            }
          }
          if (success)
          {
            resolve(true);
          }
          else
          {
            this._activitiesPending[id] = (status) => {
              if (status.timeout)
              {
                resolve(true);
              }
              else
              {
                this._syncTopicUpdates(id).then(resolve);
              }
            }
          }
        }
      });
    });
  },

  _getService: function(service, options)
  {
    if (!this._proxies[service])
    {
      this._proxies[service] = this._node.proxy(Object.assign({ service: service }, options));
    }
    return this._proxies[service];
  },

  _unproxyAllServices: function()
  {
    for (let service in this._proxies)
    {
      this._node.unproxy({ service: service });
    }
    this._proxies = {};
  },

  _debugMessage: function(msg)
  {
    console.log(msg);
  },

  _partConfig: function(partName, instanceName, config)
  {
    const id = `/part/${partName}/${instanceName}`;
    try
    {
      const info =
      {
        activities: {},
        state: {},
        version: 1
      };
      this._topics[id] = info;
      const app =
      {
        getService: (name, options) => { return this._getService(name, options); },
        update: () => {
          info.version++;
          for (let aid in info.activities)
          {
            const callback = this._activitiesPending[aid];
            if (callback)
            {
              this._activitiesPending[aid] = null;
              callback({});
            }
          }
        }
      };
      this._parts[id] = require(`./parts/${partName}`)(app, instanceName, config);
    }
    catch (_)
    {
      console.error(`Missing App Part: ${partName}`);
    }
  },

  _partProcess: function(partName, instanceName, args)
  {
    const id = `/part/${partName}/${instanceName}`;
    const part = this._parts[id];
    if (part)
    {
      return part(args || {});
    }
    else
    {
      this._partConfig(partName, instanceName, {});
      return this._parts[id](args || {});
    }
  },

  _subscribeToPart: function(activity, partName, instanceName)
  {
    if (!activity)
    {
      return;
    }
    const id = `/part/${partName}/${instanceName}`;
    if (this._topics[id])
    {
      this._topics[id].activities[activity] = { state: {}, callback: null, version: 0 };
    }
  },

  _subscribeToTimer: function(activity, timerName, time)
  {
    if (!activity)
    {
      return;
    }
    if (timerName.indexOf('__timer') === 0)
    {
      // Period timer
      if (!this._topics[timerName])
      {
        this._topics[timerName] = {
          activities: {},
          state: {},
          version: 1
        };
      }
      const topic = this._topics[timerName];
      if (topic.activities[activity])
      {
        // Only one timer per activity per time period
        return;
      }
      const info =
      {
        state: {},
        callback: null,
        version: 1,
        tick: false,
        interval: setInterval(() => {
          info.version = 0;
          info.tick = true;
          const callback = this._activitiesPending[activity];
          if (callback)
          {
            this._activitiesPending[activity] = null;
            callback({});
          }
        }, time * 1000)
      }
      topic.activities[activity] = info;

    }
    else
    {
      // Event timeout
      const topic = this._topics[timerName];
      if (!topic || !topic.activities[activity])
      {
        // Cannot timeout on an event we're not looking for
        return;
      }
      const info = topic.activities[activity];
      if (info.restartTimeout)
      {
        // Only one timer per activity per event
        return;
      }
      info.restartTimeout = () => {
        info.tick = false;
        clearTimeout(info.timeout);
        info.timeout = setTimeout(() => {
          info.tick = true;
          const callback = this._activitiesPending[activity];
          if (callback)
          {
            this._activitiesPending[activity] = null;
            callback({ timeout: true });
          }
        }, time * 1000);
      }
      info.restartTimeout();
    }
  },

  _getTimer: function(activityId, timerName)
  {
    if (activityId)
    {
      const topic = this._topics[timerName];
      if (topic)
      {
        const info = topic.activities[activityId];
        if (info && info.tick)
        {
          return true;
        }
      }
    }
    return false;
  },
};

module.exports = app;
