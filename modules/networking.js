'use strict';

console.info('Loading Networking.');

const fs = require('fs');                     
const childProcess = require('child_process');
const os = require('os');
const StateManager = require('./state-manager');

const CMD_HOSTAPD = '/etc/init.d/hostapd';
const CMD_HOSTNAME = '/bin/hostname';
const CMD_IFUP = '/sbin/ifup';
const CMD_IFDOWN = '/sbin/ifdown';
const CMD_SOFTAP = '/etc/init.d/softap';
const CMD_DNSMASQ = '/etc/init.d/dnsmasq';
const CMD_IW = '/usr/sbin/iw';
const NET_INTERFACES_CONFIG = '/etc/network/interfaces';
const HOSTAPD_CONFIG = '/etc/hostapd.conf';
const WPA_SUPPLICANT_CONFIG = '/etc/wpa_supplicant.conf';
const DNSMASQ_CONFIG = '/etc/dnsmasq.conf';

const TRACK_CHANNEL_INTERVAL = 5000;

const SERVICE_CONFIG = { service: 'config', schema: 
{
  networkName: 'String',
  networkPassword: 'String',
  apNetworkName: 'String',
  apNetworkPassword: 'String',
  apNetworkAddress: 'String',
  apNetworkChannel: 'String',
  ethNetworkAddress: 'String'
}};

function networking(config)
{
  this._name = config.name;
  this._node = Node.init(config.name);
  this._enabled = 0;
  this._channelTracker = null;

  // eth0 not always present
  if (!('eth0' in os.networkInterfaces()))
  {
    delete SERVICE_CONFIG.ethNetworkAddress;
  }

  if ('networkName' in config && 'networkPassword' in config)
  {
    this._preset =
    {
      networkName: config.networkName,
      networkPassword: config.networkPassword
    };
  }
  this._state = new StateManager({ name: `config-${this._name.replace(/\//g, '_')}` });
}

networking.prototype =
{
  enable: function()
  {
    if (this._enabled++ === 0)
    {
      this._node.service(SERVICE_CONFIG, (request) => {
        if ('networkName' in request || 'networkPassword' in request)
        {
          this._state.set('networkUpdated', true);
        }
        const reply = this._updateNetwork(request);
        return reply;
      });
      if (this._preset && this._state.get('networkUpdated') !== true)
      {
        this._updateNetwork(this._preset);
      }
      this._channelTracker = this._trackChannel();
    }
    return this;
  },
  
  disable: function()
  {
    if (--this._enabled === 0)
    {
      this._node.unservice(SERVICE_CONFIG);
      clearInterval(this._channelTracker);
      this._channelTracker = null;
    }
    return this;
  },

  _updateNetwork: function(request)
  {
    let hostap = [];
    let wpa = [];
    let net = [];
    let dnsmasq = [];
    try
    {
      hostap = fs.readFileSync(HOSTAPD_CONFIG, { encoding: 'utf8' }).split('\n');
    }
    catch (_)
    {
      if (SIMULATOR)
      {
        hostap = `
ssid=8bitrobot network
wpa_passphrase=8bitrobot password
channel=1
`.split('\n');
      }
    }
    try
    {
      wpa = fs.readFileSync(WPA_SUPPLICANT_CONFIG, { encoding: 'utf8' }).split('\n');
    }
    catch (_)
    {
      if (SIMULATOR)
      {
        wpa = `
network={
  ssid="your local network"
  psk="your local password"
}
`.split('\n');
      }
    }
    try
    {
      net = fs.readFileSync(NET_INTERFACES_CONFIG, { encoding: 'utf8' }).split('\n');
    }
    catch (_)
    {
      if (SIMULATOR)
      {
        net = `
iface SoftAp0 static
  address 192.168.3.1
  netmask 255.255.255.0
  network 192.168.3.0
  broadcast 192.168.3.255

iface eth0 static
  address 192.168.4.1
  netmask 255.255.255.0
  network 192.168.4.0
  broadcast 192.168.4.255
`.split('\n');
      }
    }

    try
    {
      dnsmasq = fs.readFileSync(DNSMASQ_CONFIG, { encoding: 'utf8' }).split('\n');
    }
    catch (_)
    {
      if (SIMULATOR)
      {
        dnsmasq = `
interface=SoftAp0
dhcp-range=SoftAp0,192.168.3.100,192.168.3.199,4h
interface=eth0
dhcp-range=eth0,192.168.4.100,192.168.4.199,4h
`.split('\n');
      }
    }

    function findHostApIndex(key)
    {
      for (let i = 0; i < hostap.length; i++)
      {
        if (hostap[i].trim().indexOf(key) === 0)
        {
          return i;
        }
      }
      return -1;
    }
    function findWpaIndex(key)
    {
      for (let i = 0; i < wpa.length; i++)
      {
        if (wpa[i].trim().indexOf(key) === 0)
        {
          return i;
        }
      }
      return -1;
    }
    function findNetIndex(key, start)
    {
      if (start !== -1)
      {
        for (let i = start; i < net.length; i++)
        {
          if (net[i].trim().indexOf(key) === 0)
          {
            return i;
          }
        }
      }
      return -1;
    }
    function findDnsMasqIndex(key)
    {
      for (let i = 0; i < dnsmasq.length; i++)
      {
        if (dnsmasq[i].trim().indexOf(key) === 0)
        {
          return i;
        }
      }
      return -1;
    }

    let result = {};
    let idx;
    let hostapChange = false;
    let wpaChange = false;
    let apChange = false;
    let ethChange = false;
    let dnsmasqChange = false;
    
    idx = findHostApIndex('ssid=');
    if (idx !== -1)
    {
      if ('apNetworkName' in request)
      {
        hostap[idx] = `ssid=${request.apNetworkName}`;
        result.apNetworkName = request.apNetworkName;
        hostapChange = true;
      }
      else
      {
        result.apNetworkName = hostap[idx].split('=')[1];
      }
    }
    idx = findHostApIndex('wpa_passphrase=');
    if (idx !== -1)
    {
      if ('apNetworkPassword' in request)
      {
        hostap[idx] = `wpa_passphrase=${request.apNetworkPassword}`;
        result.apNetworkPassword = request.apNetworkPassword;
        hostapChange = true;
      }
      else
      {
        result.apNetworkPassword = hostap[idx].split('=')[1];
      }
      result.apNetworkPassword = result.apNetworkPassword.replace(/./g, '*');
    }
    idx = findHostApIndex('channel=');
    if (idx !== -1)
    {
      if ('apNetworkChannel' in request)
      {
        hostap[idx] = `channel=${request.apNetworkChannel}`;
        result.apNetworkChannel = request.apNetworkChannel;
        hostapChange = true;
      }
      else
      {
        result.apNetworkChannel = hostap[idx].split('=')[1];
      }
    }
    idx = findWpaIndex('ssid=');
    if (idx !== -1)
    {
      if ('networkName' in request)
      {
        wpa[idx] = `ssid="${request.networkName}"`;
        result.networkName = request.networkName;
        wpaChange = true;
      }
      else
      {
        result.networkName = wpa[idx].split('=')[1].replace(/"/g, '');
      }
    }
    idx = findWpaIndex('psk=');
    if (idx !== -1)
    {
      if ('networkPassword' in request)
      {
        wpa[idx] = `psk="${request.networkPassword}"`;
        result.networkPassword = request.networkPassword;
        wpaChange = true;
      }
      else
      {
        result.networkPassword = wpa[idx].split('=')[1].replace(/"/g, '');
      }
      result.networkPassword = result.networkPassword.replace(/./g, '*');
    }
    idx = findNetIndex('address', findNetIndex('iface SoftAp0', 0));
    if (idx !== -1)
    {
      if ('apNetworkAddress' in request)
      {
        const root = request.apNetworkAddress.split('.').slice(0, 3).join('.');
        net[idx + 0] = ` address ${request.apNetworkAddress}`;
        net[idx + 1] = ` netmask 255.255.255.0`;
        net[idx + 2] = ` network ${root}.0`;
        net[idx + 3] = ` broadcast ${root}.255`;
        apChange = true;
        result.apNetworkAddress = request.apNetworkAddress;
        idx = findDnsMasqIndex('dhcp-range=SoftAp0');
        if (idx !== -1)
        {
          dnsmasq[idx] = `dhcp-range=SoftAp0,${root}.100,${root}.199,4h`;
          dnsmasqChange = true;
        }
      }
      else
      {
        result.apNetworkAddress = net[idx].trim().split(' ')[1];
      }
    }
    if ('ethNetworkAddress' in SERVICE_CONFIG.schema)
    {
      idx = findNetIndex('iface eth0', 0);
      if (idx !== -1)
      {
        if ('ethNetworkAddress' in request)
        {
          if (request.ethNetworkAddress === 'dhcp')
          {
            net[idx + 0] = 'iface eth0 dhcp';
            net[idx + 1] = '';
            net[idx + 2] = '';
            net[idx + 3] = '';
            net[idx + 4] = '';
            idx = findDnsMasqIndex('dhcp-range=eth0');
            if (idx !== -1)
            {
              dnsmasq[idx] = `dhcp-range=eth0,0.0.0.0,0.0.0.0`;
              dnsmasqChange = true;
            }
          }
          else
          {
            const root = request.ethNetworkAddress.split('.').slice(0, 3).join('.');
            net[idx + 0] = `iface eth0 static`;
            net[idx + 1] = ` address ${request.ethNetworkAddress}`;
            net[idx + 2] = ` netmask 255.255.255.0`;
            net[idx + 3] = ` network ${root}.0`;
            net[idx + 4] = ` broadcast ${root}.255`;
            idx = findDnsMasqIndex('dhcp-range=eth0');
            if (idx !== -1)
            {
              dnsmasq[idx] = `dhcp-range=eth0,${root}.100,${root}.199,4h`;
              dnsmasqChange = true;
            }
          }
          result.ethNetworkAddress = request.ethNetworkAddress;
          ethChange = true;
        }
        else
        {
          result.ethNetworkAddress = net[idx + 1].trim().split(' ')[1];
        }
      }
    }

    if (!SIMULATOR)
    {
      if (hostapChange && hostap.length)
      {
        fs.writeFileSync(HOSTAPD_CONFIG, hostap.join('\n'), { encoding: 'utf8' });
        childProcess.spawn(CMD_HOSTAPD, [ 'restart' ], {});
        childProcess.spawn(CMD_HOSTNAME, [ result.apNetworkName ], {});
      }
      if (wpaChange && wpa.length)
      {
        fs.writeFileSync(WPA_SUPPLICANT_CONFIG, wpa.join('\n'), { encoding: 'utf8' });
        childProcess.spawn(CMD_IFDOWN, [ 'wlan0' ], {});
        childProcess.spawn(CMD_IFUP, [ 'wlan0' ], {});
      }
      if ((apChange || ethChange) && net.length)
      {
        fs.writeFileSync(NET_INTERFACES_CONFIG, net.join('\n'), { encoding: 'utf8' });
        if (ethChange)
        {
          childProcess.spawn(CMD_IFDOWN, [ 'eth0' ], {});
          childProcess.spawn(CMD_IFUP, [ 'eth0' ], {});
        }
        if (apChange)
        {
          childProcess.spawn(CMD_IFDOWN, [ 'SoftAp0' ], {});
          childProcess.spawn(CMD_IFUP, [ 'SoftAp0' ], {});
          childProcess.spawn(CMD_SOFTAP, [ 'restart' ], {});
        }
        if (dnsmasqChange)
        {
          fs.writeFileSync(DNSMASQ_CONFIG, dnsmasq.join('\n'), { encoding: 'utf8' });
          childProcess.spawn(CMD_DNSMASQ, [ 'restart' ], {});
        }
      }
    }
    else
    {
      hostapChange && console.log('hostAp', hostap);
      wpaChange && console.log('wpa', wpa);
      (apChange || ethChange) && console.log('net', net);
      dnsmasqChange && console.log('dnsmasq', dnsmasq);
    }

    return result;
  },

  _trackChannel: function(enable)
  {
    setInterval(() => {
      // Find current channel we're connected to
      const result = childProcess.spawnSync(CMD_IW, [ 'dev', 'wlan0', 'info' ]);
      const stdout = (result.stdout || '').toString('utf8').split('\n');
      let channel = null;
      for (let i = 0; i < stdout.length; i++)
      {
        if (stdout[i].trim().indexOf('channel') === 0)
        {
          channel = stdout[i].trim().split(' ')[1];
          break;
        }
      }
      if (!channel)
      {
        return;
      }
      // Find current hostap channel
      const hostap = fs.readFileSync(HOSTAPD_CONFIG, { encoding: 'utf8' }).split('\n');
      let hostchannelidx = null;
      for (let i = 0; i < hostap.length; i++)
      {
        if (hostap[i].trim().indexOf('channel=') === 0)
        {
          hostchannelidx = i;
          break;
        }
      }
      if (hostchannelidx === null || channel == hostap[hostchannelidx].split('=')[1])
      {
        return;
      }
      // Update hostap channel
      hostap[hostchannelidx] = `channel=${channel}`;
      fs.writeFileSync(HOSTAPD_CONFIG, hostap.join('\n'), { encoding: 'utf8' });
      childProcess.spawn(CMD_HOSTAPD, [ 'restart' ], {});
    }, TRACK_CHANNEL_INTERVAL);
  }
}

module.exports = networking;
