document.addEventListener('DOMContentLoaded', function()
{
  // --------------------------------------------------------------------------
  // Utility functions
  // --------------------------------------------------------------------------
  const UUID = function()
  {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  };

  function sort(arr)
  {
    arr.sort((a, b) => {
      return a.name > b.name;
    });
    return arr;
  }

  function filter(item)
  {
    const f =
    {
      '/app/config': true,
      '/list': true,
      '/server/add_page': true,
      '/networking/config': true
    };
    return !(f[item] || item.indexOf('/controller/') === 0);
  }

  // --------------------------------------------------------------------------
  // Configuration
  // --------------------------------------------------------------------------

  window.APP = {};
  Blockly.Field.prototype.maxDisplayLength = 100;
  Blockly.JavaScript.INFINITE_LOOP_TRAP = `if (activity.terminated()) throw new Error("Terminated");\n`;
  Blockly.JavaScript.addReservedWords('activity');
  // Override the default text_print.
  Blockly.JavaScript['text_print'] = function(block)
  {
    const msg = Blockly.JavaScript.valueToCode(block, 'TEXT', Blockly.JavaScript.ORDER_NONE) || "''";
    return `activity.print(${msg});\n`;
  };
  // Hide Parts from variables
  const oldVS = Blockly.Blocks['variables_set'].init;
  Blockly.Blocks['variables_set'].init = function()
  {
    oldVS.call(this);
    this.getField('VAR').variableTypes = [''];
  }
  const oldVG = Blockly.Blocks['variables_get'].init;
  Blockly.Blocks['variables_get'].init = function()
  {
    oldVG.call(this);
    this.getField('VAR').variableTypes = [''];
  }
  const oldMC = Blockly.Blocks['math_change'].init;
  Blockly.Blocks['math_change'].init = function()
  {
    oldMC.call(this);
    this.getField('VAR').variableTypes = [''];
  }

  const COLOR =
  {
    PROGRAM: 190,
    CONFIG: 240,
    ACTION: 0,
    EVENT: 55,
    PART:
    {
      BUTTON_CSS_CLASS: 'part-create-button',
      CONFIG: 240,
      SET: 0,
      GET: 55
    },
    KINEMATICS:
    {
      BUTTON_CSS_CLASS: 'kinematic-create-button',
      CONFIG: 240,
      CHAIN: 120,
      BONE: 220
    },
    CONTROL:
    {
      EVENT: 55,
      ACTION: 0,
    },
    LOGIC: '%{BKY_LOGIC_HUE}',
    MATH: '%{BKY_MATH_HUE}'
  };

  const TransitionFunctions = [ [ 'linear', 'linear' ], [ 'ease_in', 'ease_in' ], [ 'ease_inout', 'ease_inout' ], [ 'ease_out', 'ease_out' ], [ 'idle', 'idle' ] ];
  const VTransitionFunctions = [ [ 'linear', 'linear' ], [ 'ease_in', 'ease_in' ], [ 'ease_inout', 'ease_inout' ], [ 'ease_out', 'ease_out' ], [ 'velocity', 'velocity' ], [ 'idle', 'idle' ] ];
  const EmptyOptions = [ [ '<none>', '<none>' ] ];

  // --------------------------------------------------------------------------
  // Extra logic and math blocks
  // --------------------------------------------------------------------------

  Blockly.Blocks['math_extra_anglediff'] =
  {
    init: function()
    {
      this.jsonInit(
      {
        colour: COLOR.MATH,
        message0: `difference between angle %1 and %2`,
        args0:
        [
          {
            type: 'input_value',
            name: 'FIRST',
          },
          {
            type: 'input_value',
            name: 'SECOND',
          }
        ],
        output: null,
        inputsInline: true,
      });
    }
  };
  Blockly.JavaScript['math_extra_anglediff'] = function(block)
  {
    const f = Blockly.JavaScript.valueToCode(block, 'FIRST', Blockly.JavaScript.ORDER_NONE) || 0;
    const s = Blockly.JavaScript.valueToCode(block, 'SECOND', Blockly.JavaScript.ORDER_NONE) || 0;
    const fn = Blockly.JavaScript.provideFunction_(
      'util_anglediff',
      [ 'function ' + Blockly.JavaScript.FUNCTION_NAME_PLACEHOLDER_ + '(f, s) {',
        '  const diff = (f - s) / 180 * Math.PI;',
        '  return Math.atan2(Math.sin(diff), Math.cos(diff)) / Math.PI * 180;',
        '}'
      ]
    );
    const code = `${fn}(${f}, ${s})`;
    return [ code, Blockly.JavaScript.ORDER_FUNCTION_CALL ];
  };
  Blockly.Blocks['logic_extra_isvalid'] =
  {
    init: function()
    {
      this.jsonInit(
      {
        colour: COLOR.LOGIC,
        message0: `%1 is valid`,
        args0:
        [
          {
            type: 'input_value',
            name: 'EXPRESSION',
          }
        ],
        output: null,
        inputsInline: true,
      });
    }
  };
  Blockly.JavaScript['logic_extra_isvalid'] = function(block)
  {
    const expr = Blockly.JavaScript.valueToCode(block, 'EXPRESSION', Blockly.JavaScript.ORDER_NONE) || undefined;
    const code = `(function(){try{(${expr})}catch(_){return false;}return true;})()`;
    return [ code, Blockly.JavaScript.ORDER_NONE ];
  };

  // --------------------------------------------------------------------------
  // Program Blocks
  // --------------------------------------------------------------------------
  function buildProgramBlocks()
  {
    Blockly.Blocks['setup'] =
    {
      __category: 'Program',
      __baseblock: true,
      init: function()
      {
        this.jsonInit(
        {
          message0: 'Setup %1',
          args0:
          [
            {
              type: 'input_statement',
              name: 'SETUP'
            }
          ],
          colour: COLOR.PROGRAM
        });
      }
    };
    Blockly.JavaScript['setup'] = function(block)
    {
      Blockly.JavaScript._currentActivity = '';
      const code = Blockly.JavaScript.statementToCode(block, 'SETUP');
      if (code)
      {
        return `App.registerSetup(async function(activity)
        {
          try
          {
            ${code}
          }
          catch (e)
          {
            activity.print(e);
          }
        });\n`;
      }
      else
      {
        return '';
      }
    };
  
    Blockly.Blocks['activity'] =
    {
      __category: 'Program',
      __baseblock: true,
      init: function()
      {
        this.jsonInit(
        {
          message0: 'On activity',
          message1: 'do %1',
          args1:
          [
            {
              type: 'input_statement',
              name: 'ACTIVITY'
            }
          ],
          colour: COLOR.PROGRAM
        });
      }
    };
    Blockly.JavaScript['activity'] = function(block)
    {
      Blockly.JavaScript._currentActivity = UUID();
      Blockly.JavaScript._topics[Blockly.JavaScript._currentActivity] = {};
      Blockly.JavaScript._parts[Blockly.JavaScript._currentActivity] = {};
      Blockly.JavaScript._timers[Blockly.JavaScript._currentActivity] = {};
      const code = Blockly.JavaScript.statementToCode(block, 'ACTIVITY');
      if (code)
      {
        return `App.registerActivity('${Blockly.JavaScript._currentActivity}', async function(activity)
        {
          try
          {
            while (await activity.sync())
            {
              try
              {
                ${code}
              }
              catch (e)
              {
                if (e.message.indexOf('Undefined topic property:') !== 0 &&
                    e.message.indexOf('Disconnected: ') !== 0)
                {
                  activity.print(e);
                  throw e;
                }
              }
            }
          }
          catch (e)
          {
          }
        });\n`;
      }
      else
      {
        return '';
      }
    };
  }

  // --------------------------------------------------------------------------
  // Config blocks
  // --------------------------------------------------------------------------
  function buildConfigBlocks(services)
  {
    for (let name in Blockly.Blocks)
    {
      if (Blockly.Blocks[name].__category === 'Config')
      {
        delete Blockly.Blocks[name];
      }
    }

    return new Promise((resolve) => {
      let blocks = services.length;
      services.forEach((service) => {

        const name = service.name;
        const schema = service.schema;
        const config = {};

        const CONFIG = NODE.proxy({ service: name });
        CONFIG({}).then((newConfig) => {
          Object.assign(config, newConfig);
          let json =
          {
            message0: `Configure ${name.substr(0, name.length - 7)}`,
            colour: COLOR.CONFIG
          };
          let count = 1;
          for (let key in schema)
          {
            if (key !== '__return')
            {
              if (typeof schema[key] === 'object')
              {
                let def = schema[key].indexOf(config[key]);
                if (def !== -1)
                {
                  schema[key].splice(def, 1);
                  schema[key].unshift(config[key]);
                }
                json[`args${count}`] = [
                {
                  type: 'field_dropdown',
                  name: key,
                  check: 'String',
                  options: schema[key].map((value) => [ value, value ])
                }];
              }
              else
              {
                switch (schema[key])
                {
                  case 'String':
                  default:
                    json[`args${count}`] = [
                    {
                      type: 'field_input',
                      name: key,
                      text: key in config ? config[key] : ''
                    }];
                    break;
                  case 'Number':
                    json[`args${count}`] = [
                    {
                      type: 'field_number',
                      name: key,
                      value: key in config ? config[key] : 0
                    }];
                    break;
                  case 'Boolean':
                    json[`args${count}`] = [
                    {
                      type: 'field_checkbox',
                      name: key,
                      checked: key in config ? !!config[key] : false
                    }];
                    break;
                }
              }
              json[`message${count}`] = `${key == 'friendlyName' ? 'name' : key} %1`;
              count++;
            }
          }
      
          let changes = false;
          Blockly.Blocks[name] =
          {
            __category: 'Config',
            __baseblock: true,
            init: function()
            {
              this.jsonInit(json);
            },

            onchange: function(e)
            {
              switch (e.type)
              {
                case Blockly.Events.BLOCK_CREATE:
                {
                  const block = e.getEventWorkspace_().getBlockById(e.blockId);
                  if (block && block.type === name)
                  {
                    for (let key in config)
                    {
                      const field = block.getFieldValue(key);
                      if (!field)
                      {
                        block.setFieldValue(config[key], key);
                      }
                      else
                      {
                        config[key] = field;
                        changes = true;
                      }
                    }
                    if (changes)
                    {
                      CONFIG(config).then((newConfig) => {
                        changes = false;
                        Object.assign(config, newConfig);
                      });
                    }
                  }
                  break;
                }
                case Blockly.Events.BLOCK_CHANGE:
                  if (this.id === e.blockId)
                  {
                    config[e.name] = e.newValue;
                    changes = true;
                  }
                  break;
                case Blockly.Events.UI:
                  if (changes)
                  {
                    CONFIG(config).then((newConfig) => {
                      changes = false;
                      Object.assign(config, newConfig);
                      rebuildEventsActionBlocksParts(e.getEventWorkspace_());
                    });
                  }
                  break;
                default:
                  break;
              }
            }
          };
          Blockly.JavaScript[name] = function(block)
          {
            const code = `App.registerConfiguration(function(activity)
            {
              return activity.service('${name}')(${JSON.stringify(config)});
            });\n`;

            return code;
          };

          if (--blocks === 0)
          {
            resolve();
          }
        });
      });
    });
  }

  // --------------------------------------------------------------------------
  // Action blocks
  // --------------------------------------------------------------------------
  function buildActionBlocks(actions)
  {
    for (let name in Blockly.Blocks)
    {
      if (Blockly.Blocks[name].__category === 'Action')
      {
        delete Blockly.Blocks[name];
      }
    }

    actions.forEach((action) => {
      
      let json =
      {
        message0: `set ${action.friendlyName ? action.friendlyName : action.name}`,
        previousStatement: null,
        nextStatement: null,
        inputsInline: true,
        colour: COLOR.ACTION
      };
      let count = 0;
      for (let key in action.schema)
      {
        if (key !== '__return')
        {
          if (typeof action.schema[key] !== 'object')
          {
            json[`args${count}`]= [
            {
              type: 'input_value',
              name: key,
              check: action.schema[key],
              align: 'RIGHT'
            }];
          }
          else
          {
            json[`args${count}`] = [
            {
              type: 'field_dropdown',
              name: key,
              check: 'String',
              options: action.schema[key].map((value) => [ value, value ])
            }];
          }
          if (count === 0)
          {
            json.message0 = `for ${action.friendlyName ? action.friendlyName : action.name} set ${key} to %1`;

          }
          else
          {
            json[`message${count}`] = `${key} to %1`;
          }
          count++;
        }
      }
      
      Blockly.Blocks[action.name] =
      {
        __category: 'Action',
        __friendlyName: action.friendlyName,
        init: function()
        {
          this.jsonInit(json);
        }
      }
      Blockly.JavaScript[action.name] = function(block)
      {
        let args = [];
        for (let key in json)
        {
          if (key.indexOf('args') === 0)
          {
            const name = json[key][0].name;
            switch (json[key][0].type)
            {
              case 'input_value':
                const value = Blockly.JavaScript.valueToCode(block, name, Blockly.JavaScript.ORDER_NONE);
                if (value)
                {
                  args.push(`${name}: ${value}`);
                }
                break;
              case 'field_dropdown':
                args.push(`${name}: '${block.getFieldValue(name)}'`);
                break;
              default:
                break;
            }
          }
        }
        const code = `await activity.service('${action.name}')({${args.join(', ')}});\n`;
        return code;
      }
    });
  }

  // --------------------------------------------------------------------------
  // Event blocks
  // --------------------------------------------------------------------------
  function buildEventBlocks(events)
  {
    for (let name in Blockly.Blocks)
    {
      if (Blockly.Blocks[name].__category === 'Event')
      {
        delete Blockly.Blocks[name];
      }
    }

    events.forEach((event) => {

      Blockly.Blocks[event.name] =
      {
        __category: 'Event',
        __friendlyName: event.friendlyName,
        init: function()
        {
          this.jsonInit(
          {
            message0: `${event.friendlyName ? event.friendlyName : event.name} value %1`,
            args0:
            [
              {
                type: 'field_dropdown',
                name: 'PROPERTY',
                options: Object.keys(event.schema).map((key) => [ key, key ])
              }
            ],
            output: null,
            colour: COLOR.EVENT
          });
        }
      };
      Blockly.JavaScript[event.name] = function(block)
      {
        const property = block.getFieldValue('PROPERTY');
        const code = `activity.get('${event.name}', '${property}')`;
        if (Blockly.JavaScript._currentActivity)
        {
          const topics = Blockly.JavaScript._topics[Blockly.JavaScript._currentActivity];
          if (!topics[event.name])
          {
            topics[event.name] = { name: event.name };
          }
        }
        return [ code, Blockly.JavaScript.ORDER_ADDITION ];
      }
    });

    // Heartbeat event block
    const hearbeatoptions = Object.keys(Blockly.Blocks).filter((name) => {
      return Blockly.Blocks[name].__category === 'Event' || (Blockly.Blocks[name].__category === 'Controls' && Blockly.Blocks[name].__subcategory === 'Event');
    }).map((name) => {
      return [ Blockly.Blocks[name].__friendlyName || name, name ];
    });
    Blockly.Blocks['heatbeat'] =
    {
      __category: 'Event',
      init: function()
      {
        this.jsonInit(
        {
          message0: `heartbeat of %1 is %2 %3 seconds`,
          args0:
          [
            {
              type: 'field_dropdown',
              name: 'EVENT_NAME',
              options: hearbeatoptions
            },
            {
              type: 'field_dropdown',
              name: 'COMPARISON',
              options: [ [ 'less than', '!' ], [ 'greater than', '' ] ]
            },
            {
              type: 'field_number',
              name: 'LIMIT',
              value: 5
            }
          ],
          output: null,
          colour: COLOR.EVENT
        });
      }
    };
    Blockly.JavaScript['heatbeat'] = function(block)
    {
      const eventName = block.getFieldValue('EVENT_NAME');
      const comparison = block.getFieldValue('COMPARISON');
      const limit = block.getFieldValue('LIMIT');
      const code = `${comparison}activity.timer('${eventName}')`;
      if (Blockly.JavaScript._currentActivity)
      {
        const topics = Blockly.JavaScript._topics[Blockly.JavaScript._currentActivity];
          if (!topics[eventName])
          {
            topics[eventName] = { name: eventName };
          }
        const timers = Blockly.JavaScript._timers[Blockly.JavaScript._currentActivity];
        if (!timers[eventName])
        {
          timers[eventName] = { name: eventName, time: limit };
        }
      }
      return [ code, Blockly.JavaScript.ORDER_NONE ];
    }

    // Timer event
    Blockly.Blocks['timer'] =
    {
      __category: 'Event',
      init: function()
      {
        this.jsonInit(
        {
          message0: `%1 second timer`,
          args0:
          [
            {
              type: 'field_number',
              name: 'TIME',
              value: 5
            }
          ],
          output: null,
          colour: COLOR.EVENT
        });
      }
    };
    Blockly.JavaScript['timer'] = function(block)
    {
      const time = block.getFieldValue('TIME');
      const eventName = `__timer${time}`;
      const code = `activity.timer('${eventName}')`;
      if (Blockly.JavaScript._currentActivity)
      {
        const timers = Blockly.JavaScript._timers[Blockly.JavaScript._currentActivity];
        if (!timers[eventName])
        {
          timers[eventName] = { name: eventName, time: time };
        }
      }
      return [ code, Blockly.JavaScript.ORDER_NONE ];
    }

  }

  // --------------------------------------------------------------------------
  // Part blocks
  // --------------------------------------------------------------------------
  function buildPartBlocks(workspace)
  {
    const partTypes = [];
    
    function registerPart(type, workspace)
    {
      partTypes.push(type);
      workspace.registerButtonCallback(`CALLBACK_${type.name}`, (button) => {
        Blockly.Variables.createVariable(button.getTargetWorkspace(), null, type.name);
      });
    }

    workspace.registerToolboxCategoryCallback('Part', (workspace) => {
      const xml = [];
      partTypes.forEach((type) => {
        xml.push(Blockly.Xml.textToDom(`<button web-class="${COLOR.PART.BUTTON_CSS_CLASS}" text="Create ${type.name}..." callbackKey="CALLBACK_${type.name}"></button>`));
        const variables = workspace.getVariablesOfType(type.name);
        if (variables.length)
        {
          if (Blockly.Blocks[`${type.name}_CONFIG`])
          {
            xml.push(Blockly.Xml.textToDom('<sep gap="8"></sep>'));
            xml.push(Blockly.Xml.textToDom(`<block type="${type.name}_CONFIG"><field variabletype='${type.name}' name="NAME">${variables[0].name}</field></block>`));
          }
          if (Blockly.Blocks[`${type.name}_SET`] && Blockly.Blocks[`${type.name}_GET`])
          {
            variables.forEach((v) => {
              xml.push(Blockly.Xml.textToDom('<sep gap="8"></sep>'));
              xml.push(Blockly.Xml.textToDom(`<block type="${type.name}_SET"><field variabletype='${type.name}' name="NAME">${v.name}</field></block>`));
            });
            xml.push(Blockly.Xml.textToDom('<sep gap="8"></sep>'));
            xml.push(Blockly.Xml.textToDom(`<block type="${type.name}_GET"><field variabletype='${type.name}' name="NAME">${variables[0].name}</field></block>`));
          }
          else if (Blockly.Blocks[`${type.name}_SET`])
          {
            variables.forEach((v) => {
              xml.push(Blockly.Xml.textToDom('<sep gap="8"></sep>'));
              xml.push(Blockly.Xml.textToDom(`<block type="${type.name}_SET"><field variabletype='${type.name}' name="NAME">${v.name}</field></block>`));
            });
          }
          else if (Blockly.Blocks[`${type.name}_GET`])
          {
            variables.forEach((v) => {
              xml.push(Blockly.Xml.textToDom('<sep gap="8"></sep>'));
              xml.push(Blockly.Xml.textToDom(`<block type="${type.name}_GET"><field variabletype='${type.name}' name="NAME">${v.name}</field></block>`));
            });
          }
        }
      });
      return xml;
    });

    function registerUse(partName, instanceName)
    {
      if (Blockly.JavaScript._currentActivity)
      {
        const id = `${partName}/${instanceName}`;
        const parts = Blockly.JavaScript._parts[Blockly.JavaScript._currentActivity];
        if (!parts[id])
        {
          parts[id] = { name: partName, instance: instanceName };
        }
      }
    }

    // Mixins
    const TransitionMixin =
    {
      mutationToDom: function()
      {
        const container = document.createElement('mutation');
        container.setAttribute('mode', this.getFieldValue('FUNC'));
        return container;
      },

      domToMutation: function(xmlElement)
      {
        this._updateShape(xmlElement.getAttribute('mode'));
      },

      _updateShape: function(mode)
      {
        if (mode === 'velocity')
        {
          this.removeInput('TIME');
          this.removeInput('milliseconds');
          if (!this.getInput('START_VELOCITY'))
          {
            this.appendValueInput('START_VELOCITY').setCheck('Number').appendField('from');
            this.appendValueInput('END_VELOCITY').setCheck('Number').appendField('to');
          }
        }
        else
        {
          this.removeInput('START_VELOCITY');
          this.removeInput('END_VELOCITY');
          if (!this.getInput('TIME'))
          {
            this.appendValueInput('TIME').setCheck('Number').appendField('over');
            this.appendDummyInput('milliseconds').appendField('milliseconds');
          }
        }
      }
    };
    function TransitionExt()
    {
      this.getField('FUNC').setValidator(function(option)
      {
        this.sourceBlock_._updateShape(option);
      });
    }
    Blockly.Extensions.ALL_['transition_mixin'] || Blockly.Extensions.registerMutator('transition_mixin', TransitionMixin, TransitionExt);

    // PWM Channels
    const channels = Object.keys(Blockly.Blocks).filter((key) => {
      return key.indexOf('/set_pulse') !== -1;
    }).map((key) => {
      return [ Blockly.Blocks[key].__friendlyName || key, key ];
    });
    // HBRIDGE Channels
    const hbridges = Object.keys(Blockly.Blocks).filter((key) => {
      return key.indexOf('/set_duty') !== -1;
    }).map((key) => {
      return [ Blockly.Blocks[key].__friendlyName || key, key ];
    });

    // SERVO
    Blockly.Blocks['Servo_CONFIG'] =
    {
      __category: 'Config',
      __baseblock: true,
      __tool: (workspace) => {
        const variables = workspace.getVariablesOfType('Servo');
        if (channels.length && variables.length)
        {
          return `<block type="Servo_CONFIG"><field variabletype="Servo" name="NAME">${variables[0].name}</field></block>`;
        }
        else
        {
          return null;
        }
      },
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.CONFIG,
          message0: 'Configure Servo',
          message1: 'name %1',
          args1:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'Servo',
              variableTypes: [ 'Servo' ]
            }
          ],
          message2: 'channel %1',
          args2:
          [
            {
              type: 'field_dropdown',
              name: 'CHANNEL',
              options: channels.length ? channels : EmptyOptions
            }
          ],
          message3: 'angle %1 persist %2',
          args3:
          [
            {
              type: 'field_number',
              name: 'ANGLE',
              value: 0
            },
            {
              type: 'field_checkbox',
              name: 'PRESIST',
              value: true,
            }
          ],
          message4: 'reverse %1',
          args4:
          [
            {
              type: 'field_checkbox',
              name: 'REV',
              value: false
            }
          ],
          message5: 'microseconds per degree %1',
          args5:
          [
            {
              type: 'field_number',
              name: 'USDEG',
              value: 11.11
            }
          ],
          message6: 'trim %1 cw %2 ccw %3',
          args6:
          [
            {
              type: 'field_number',
              name: 'TRIM',
              value: 0
            },
            {
              type: 'field_number',
              name: 'CW',
              value: -90
            },
            {
              type: 'field_number',
              name: 'CCW',
              value: 90
            }
          ]
        });
      }
    };
    Blockly.JavaScript['Servo_CONFIG'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const channel = block.getFieldValue('CHANNEL');
      const angle = block.getFieldValue('ANGLE');
      const persist = block.getFieldValue('PERSIST') === 'TRUE';
      const rev = block.getFieldValue('REV') === 'TRUE';
      const usdeg = block.getFieldValue('USDEG');
      const trim = block.getFieldValue('TRIM');
      const min = block.getFieldValue('CW');
      const max = block.getFieldValue('CCW');
      if (channel == EmptyOptions[0][1])
      {
        return null;
      }
      return `App.registerConfiguration(function(activity)
      {
        return activity.partConfig('Servo', '${name}', { channel: '${channel}', angle: ${angle}, rev: ${rev}, usdeg: ${usdeg}, trim: ${trim}, cw: ${min}, ccw: ${max}, persit: ${persist} });
      });\n`;
    };

    Blockly.Blocks['Servo_SET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.SET,
          message0: 'for servo %1 set angle to %2 with %3 transition',
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'Servo',
              variableTypes: [ 'Servo' ]
            },
            {
              type: 'input_value',
              name: 'ANGLE'
            },
            {
              type: 'field_dropdown',
              name: 'FUNC',
              options: VTransitionFunctions
            }
          ],
          previousStatement: null,
          nextStatement: null,
          inputsInline: true,
          mutator: 'transition_mixin'
        });
      }
    };
    Blockly.JavaScript['Servo_SET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const angle = Blockly.JavaScript.valueToCode(block, 'ANGLE', Blockly.JavaScript.ORDER_NONE) || undefined;
      let time = Blockly.JavaScript.valueToCode(block, 'TIME', Blockly.JavaScript.ORDER_NONE) || undefined;
      const vstart = Blockly.JavaScript.valueToCode(block, 'START_VELOCITY', Blockly.JavaScript.ORDER_NONE) || undefined;
      const vend = Blockly.JavaScript.valueToCode(block, 'END_VELOCITY', Blockly.JavaScript.ORDER_NONE) || undefined;
      const func = block.getFieldValue('FUNC');
      if (vstart === undefined && vend === undefined && time === undefined)
      {
        time = 0;
      }
      const code = `activity.part('Servo', '${name}', { angle: ${angle}, time: ${time}, vstart: ${vstart}, vend: ${vend}, func: '${func}' });`
      return code;
    };
    registerPart({ name: 'Servo' }, workspace);

    // CONTINUOUS SERVO
    Blockly.Blocks['ContinuousServo_CONFIG'] =
    {
      __category: 'Config',
      __baseblock: true,
      __tool: (workspace) => {
        const variables = workspace.getVariablesOfType('ContinuousServo');
        if (channels.length && variables.length)
        {
          return `<block type="ContinuousServo_CONFIG"><field variabletype="ContinuousServo" name="NAME">${variables[0].name}</field></block>`;
        }
        else
        {
          return null;
        }
      },
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.CONFIG,
          message0: 'Configure Continuous Servo',
          message1: 'name %1',
          args1:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'ContinuousServo',
              variableTypes: [ 'ContinuousServo' ]
            }
          ],
          message2: 'channel %1',
          args2:
          [
            {
              type: 'field_dropdown',
              name: 'CHANNEL',
              options: channels.length ? channels : EmptyOptions
            }
          ],
          message3: 'reverse %1',
          args3:
          [
            {
              type: 'field_checkbox',
              name: 'REV',
              value: false
            }
          ],
          message4: 'max backward %1 milliseconds',
          args4:
          [
            {
              type: 'field_number',
              name: 'MIN',
              value: 1.0
            },
          ],
          message5: 'neutral %1 to %2 milliseconds',
          args5:
          [
            {
              type: 'field_number',
              name: 'LOW_NEUTRAL',
              value: 1.5
            },
            {
              type: 'field_number',
              name: 'HIGH_NEUTRAL',
              value: 1.5
            }
          ],
          message6: 'max forward %1 milliseconds',
          args6:
          [
            {
              type: 'field_number',
              name: 'MAX',
              value: 2.0
            }
          ]
        });
      }
    };
    Blockly.JavaScript['ContinuousServo_CONFIG'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const channel = block.getFieldValue('CHANNEL');
      const rev = block.getFieldValue('REV') === 'TRUE';
      const min = block.getFieldValue('MIN');
      const lowneutral = block.getFieldValue('LOW_NEUTRAL');
      const highneutral = block.getFieldValue('HIGH_NEUTRAL');
      const max = block.getFieldValue('MAX');
      if (channel == EmptyOptions[0][1])
      {
        return null;
      }
      return `App.registerConfiguration(function(activity)
      {
        return activity.partConfig('ContinuousServo', '${name}', { channel: '${channel}', rev: ${rev}, min: ${min}, lowneutral: ${lowneutral}, highneutral: ${highneutral}, max: ${max} });
      });\n`;
    };
    Blockly.Blocks['ContinuousServo_SET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.SET,
          message0: 'for continuous servo %1 set velocity to %2 with %4 transition over %3 milliseconds ',
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'ContinuousServo',
              variableTypes: [ 'ContinuousServo' ]
            },
            {
              type: 'input_value',
              name: 'VELOCITY'
            },
            {
              type: 'input_value',
              name: 'TIME'
            },
            {
              type: 'field_dropdown',
              name: 'FUNC',
              options: TransitionFunctions
            }
          ],
          previousStatement: null,
          nextStatement: null,
          inputsInline: true
        });
      }
    };
    Blockly.JavaScript['ContinuousServo_SET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const velocity = Blockly.JavaScript.valueToCode(block, 'VELOCITY', Blockly.JavaScript.ORDER_NONE) || undefined;
      const time = Blockly.JavaScript.valueToCode(block, 'TIME', Blockly.JavaScript.ORDER_NONE) || undefined;
      const func = block.getFieldValue('FUNC');
      const code = `activity.part('ContinuousServo', '${name}', { velocity: ${velocity}, time: ${time}, func: '${func}' });`
      return code;
    };
    registerPart({ name: 'ContinuousServo' }, workspace);


    // ESC MOTOR
    Blockly.Blocks['EscMotor_CONFIG'] =
    {
      __category: 'Config',
      __baseblock: true,
      __tool: (workspace) => {
        const variables = workspace.getVariablesOfType('EscMotor');
        if (channels.length && variables.length)
        {
          return `<block type="EscMotor_CONFIG"><field variabletype="EscMotor" name="NAME">${variables[0].name}</field></block>`;
        }
        else
        {
          return null;
        }
      },
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.CONFIG,
          message0: 'Configure Esc Motor',
          message1: 'name %1',
          args1:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'EscMotor',
              variableTypes: [ 'EscMotor' ]
            }
          ],
          message2: 'channel %1',
          args2:
          [
            {
              type: 'field_dropdown',
              name: 'CHANNEL',
              options: channels.length ? channels : EmptyOptions
            }
          ],
          message3: 'reverse %1',
          args3:
          [
            {
              type: 'field_checkbox',
              name: 'REV',
              value: false
            }
          ],
          message4: 'max backward %1 milliseconds',
          args4:
          [
            {
              type: 'field_number',
              name: 'MIN',
              value: 1.0
            },
          ],
          message5: 'neutral %1 to %2 milliseconds',
          args5:
          [
            {
              type: 'field_number',
              name: 'LOW_NEUTRAL',
              value: 1.5
            },
            {
              type: 'field_number',
              name: 'HIGH_NEUTRAL',
              value: 1.5
            }
          ],
          message6: 'max forward %1 milliseconds',
          args6:
          [
            {
              type: 'field_number',
              name: 'MAX',
              value: 2.0
            }
          ],
          message7: 'rate change base %1 milliseconds',
          args7:
          [
            {
              type: 'field_number',
              name: 'RATE',
              value: 500
            }
          ],
          message8: 'neutral transition %1 milliseconds',
          args8:
          [
            {
              type: 'field_number',
              name: 'TRANSITION',
              value: 500
            }
          ]
        });
      }
    };
    Blockly.JavaScript['EscMotor_CONFIG'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const channel = block.getFieldValue('CHANNEL');
      const rev = block.getFieldValue('REV') === 'TRUE';
      const lowneutral = block.getFieldValue('LOW_NEUTRAL');
      const highneutral = block.getFieldValue('HIGH_NEUTRAL');      
      const min = block.getFieldValue('MIN');
      const max = block.getFieldValue('MAX');
      const rate = block.getFieldValue('RATE');
      const transition = block.getFieldValue('TRANSITION');
      if (channel == EmptyOptions[0][1])
      {
        return null;
      }
      return `App.registerConfiguration(function(activity)
      {
        return activity.partConfig('EscMotor', '${name}', { channel: '${channel}', rev: ${rev}, min: ${min}, lowneutral: ${lowneutral}, highneutral: ${highneutral}, max: ${max}, rate: ${rate}, transition: ${transition} });
      });\n`;
    };
    Blockly.Blocks['EscMotor_SET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.SET,
          message0: 'for esc motor %1 set velocity to %2 with %4 transition over %3 milliseconds',
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'EscMotor',
              variableTypes: [ 'EscMotor' ]
            },
            {
              type: 'input_value',
              name: 'VELOCITY'
            },
            {
              type: 'input_value',
              name: 'TIME'
            },
            {
              type: 'field_dropdown',
              name: 'FUNC',
              options: TransitionFunctions
            }
          ],
          previousStatement: null,
          nextStatement: null,
          inputsInline: true
        });
      }
    };
    Blockly.JavaScript['EscMotor_SET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const velocity = Blockly.JavaScript.valueToCode(block, 'VELOCITY', Blockly.JavaScript.ORDER_NONE) || undefined;
      const time = Blockly.JavaScript.valueToCode(block, 'TIME', Blockly.JavaScript.ORDER_NONE) || undefined;
      const func = block.getFieldValue('FUNC');
      const code = `activity.part('EscMotor', '${name}', { velocity: ${velocity}, time: ${time}, func: '${func}' });`
      return code;
    };
    registerPart({ name: 'EscMotor' }, workspace);

    // MOTOR
    Blockly.Blocks['Motor_CONFIG'] =
    {
      __category: 'Config',
      __baseblock: true,
      __tool: (workspace) => {
        const variables = workspace.getVariablesOfType('Motor');
        if (hbridges.length && variables.length)
        {
          return `<block type="Motor_CONFIG"><field variabletype="Motor" name="NAME">${variables[0].name}</field></block>`;
        }
        else
        {
          return null;
        }
      },
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.CONFIG,
          message0: 'Configure Motor',
          message1: 'name %1',
          args1:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'Motor',
              variableTypes: [ 'Motor' ]
            }
          ],
          message2: 'channel %1',
          args2:
          [
            {
              type: 'field_dropdown',
              name: 'CHANNEL',
              options: hbridges.length ? hbridges : EmptyOptions
            }
          ],
          message3: 'reverse %1',
          args3:
          [
            {
              type: 'field_checkbox',
              name: 'REV',
              value: false
            }
          ],
          message4: 'min %1',
          args4:
          [
            {
              type: 'field_number',
              name: 'MIN',
              value: -1
            },
          ],
          message5: 'max %1',
          args5:
          [
            {
              type: 'field_number',
              name: 'MAX',
              value: 1
            }
          ]
        });
      }
    };
    Blockly.JavaScript['Motor_CONFIG'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const channel = block.getFieldValue('CHANNEL');
      const rev = block.getFieldValue('REV') === 'TRUE';
      const min = block.getFieldValue('MIN');
      const max = block.getFieldValue('MAX');
      if (channel == EmptyOptions[0][1])
      {
        return null;
      }
      return `App.registerConfiguration(function(activity)
      {
        return activity.partConfig('Motor', '${name}', { channel: '${channel}', rev: ${rev}, min: ${min}, max: ${max} });
      });\n`;
    };
    Blockly.Blocks['Motor_SET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.SET,
          message0: 'for motor %1 set velocity to %2 with %4 transition over %3 seconds',
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'Motor',
              variableTypes: [ 'Motor' ]
            },
            {
              type: 'input_value',
              name: 'VELOCITY'
            },
            {
              type: 'input_value',
              name: 'TIME'
            },
            {
              type: 'field_dropdown',
              name: 'FUNC',
              options: TransitionFunctions
            }
          ],
          previousStatement: null,
          nextStatement: null,
          inputsInline: true
        });
      }
    };
    Blockly.JavaScript['Motor_SET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const velocity = Blockly.JavaScript.valueToCode(block, 'VELOCITY', Blockly.JavaScript.ORDER_NONE) || undefined;
      const time = Blockly.JavaScript.valueToCode(block, 'TIME', Blockly.JavaScript.ORDER_NONE) || undefined;
      const func = block.getFieldValue('FUNC');
      const code = `activity.part('Motor', '${name}', { velocity: ${velocity}, time: ${time}, func: '${func}' });`
      return code;
    };
    registerPart({ name: 'Motor' }, workspace);

    // TANK
    Blockly.Blocks['Tank_SET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.SET,
          message0: `for tank %1 set x to %2 and y to %3`,
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'Tank',
              variableTypes: [ 'Tank' ]
            },
            {
              type: 'input_value',
              name: 'X'
            },
            {
              type: 'input_value',
              name: 'Y'
            }
          ],
          previousStatement: null,
          nextStatement: null,
          inputsInline: true,
        });
      }
    };
    Blockly.Blocks['Tank_GET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.GET,
          message0: `%2 velocity of tank %1`,
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'Tank',
              variableTypes: [ 'Tank' ]
            },
            {
              type: 'field_dropdown',
              name: 'OUTPUT',
              options: [ [ 'left', 'left' ], [ 'right', 'right' ] ]
            }
          ],
          output: null,
          inputsInline: true,
        });
      }
    };
    Blockly.JavaScript['Tank_SET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const x = Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_NONE) || undefined;
      const y = Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_NONE) || undefined;
      const code = `activity.part('Tank', '${name}', { x: ${x}, y: ${y} });`;
      return code;
    };
    Blockly.JavaScript['Tank_GET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const output = block.getFieldValue('OUTPUT');
      const code = `activity.part('Tank', '${name}').${output}`;
      registerUse('Tank', name);
      return [ code, Blockly.JavaScript.ORDER_NONE ];
    };
    registerPart({ name: 'Tank' }, workspace);

    // CAR
    Blockly.Blocks['Car_SET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.SET,
          message0: `for car %1 set x to %2 and y to %3`,
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'Car',
              variableTypes: [ 'Car' ]
            },
            {
              type: 'input_value',
              name: 'X'
            },
            {
              type: 'input_value',
              name: 'Y'
            }
          ],
          previousStatement: null,
          nextStatement: null,
          inputsInline: true,
        });
      }
    };
    Blockly.Blocks['Car_GET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.GET,
          message0: `%2 of car %1`,
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'Car',
              variableTypes: [ 'Car' ]
            },
            {
              type: 'field_dropdown',
              name: 'OUTPUT',
              options: [ [ 'velocity', 'velocity' ], [ 'steering angle', 'angle' ] ]
            }
          ],
          output: null,
          inputsInline: true,
        });
      }
    };
    Blockly.JavaScript['Car_SET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const x = Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_NONE) || undefined;
      const y = Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_NONE) || undefined;
      const code = `activity.part('Car', '${name}', { x: ${x}, y: ${y} });`;
      return code;
    };
    Blockly.JavaScript['Car_GET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const output = block.getFieldValue('OUTPUT');
      const code = `activity.part('Car', '${name}').${output}`;
      registerUse('Car', name);
      return [ code, Blockly.JavaScript.ORDER_NONE ];
    };
    registerPart({ name: 'Car' }, workspace);

    // PID
    Blockly.Blocks['PID_CONFIG'] =
    {
      __category: 'Config',
      __baseblock: true,
      __tool: (workspace) => {
        const variables = workspace.getVariablesOfType('PID');
        if (hbridges.length && variables.length)
        {
          return `<block type="PID_CONFIG"><field variabletype="PID" name="NAME">${variables[0].name}</field></block>`;
        }
        else
        {
          return null;
        }
      },
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.CONFIG,
          message0: 'Configure PID',
          message1: 'name %1',
          args1:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'PID',
              variableTypes: [ 'PID' ]
            }
          ],
          message2: 'type %1',
          args2:
          [
            {
              type: 'field_dropdown',
              name: 'TYPE',
              check: 'String',
              options: [ [ 'linear', 'linear' ], [ 'circular', 'circular' ] ]
            }
          ],
          message3: 'P %1 I %2 D %3',
          args3:
          [
            {
              type: 'field_number',
              name: 'P',
              value: 1
            },
            {
              type: 'field_number',
              name: 'I',
              value: 0
            },
            {
              type: 'field_number',
              name: 'D',
              value: 0
            }
          ],
          message4: 'neutral %1 to %2',
          args4:
          [
            {
              type: 'field_number',
              name: 'LOW_NEUTRAL',
              value: 0
            },
            {
              type: 'field_number',
              name: 'HIGH_NEUTRAL',
              value: 0
            }
          ],
          message5: 'limit %1 to %2',
          args5:
          [
            {
              type: 'field_number',
              name: 'MIN',
              value: -1
            },
            {
              type: 'field_number',
              name: 'MAX',
              value: 1
            }
          ]
        });
      }
    };
    Blockly.Blocks['PID_GET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.GET,
          message0: `%2 of PID %1`,
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'PID',
              variableTypes: [ 'PID' ]
            },
            {
              type: 'field_dropdown',
              name: 'FIELD',
              check: 'String',
              options: [ [ 'Output', 'output' ], [ 'Setpoint', 'setpoint' ] ]
            },
          ],
          output: null,
          inputsInline: true,
        });
      }
    };
    Blockly.Blocks['PID_SET'] =
    {
      __category: 'Part',
      init: function()
      {
        this.jsonInit(
        {
          colour: COLOR.PART.SET,
          message0: `for PID %1 set %2 to %3`,
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'PID',
              variableTypes: [ 'PID' ]
            },
            {
              type: 'field_dropdown',
              name: 'FIELD',
              check: 'String',
              options: [ [ 'Input', 'input' ], [ 'Setpoint', 'setpoint' ] ]
            },
            {
              type: 'input_value',
              name: 'VALUE'
            }
          ],
          previousStatement: null,
          nextStatement: null,
          inputsInline: true,
        });
      }
    };
    Blockly.JavaScript['PID_CONFIG'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const type = block.getFieldValue('TYPE');
      const P = block.getFieldValue('P');
      const I = block.getFieldValue('I');
      const D = block.getFieldValue('D');
      const min = block.getFieldValue('MIN');
      const max = block.getFieldValue('MAX');
      const neutralMin = block.getFieldValue('LOW_NEUTRAL');
      const neutralMax = block.getFieldValue('HIGH_NEUTRAL');
      return `App.registerConfiguration(function(activity)
      {
        return activity.partConfig('PID', '${name}', { type: '${type}', P: ${P}, I: ${I}, D: ${D}, min: ${min}, max: ${max}, neutralMin: ${neutralMin}, neutralMax: ${neutralMax} });
      });\n`;
    }
    Blockly.JavaScript['PID_SET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_NONE) || undefined;
      const field = block.getFieldValue('FIELD');
      const code = `activity.part('PID', '${name}', { ${field}: ${value} });`;
      return code;
    }
    Blockly.JavaScript['PID_GET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const field = block.getFieldValue('FIELD');
      const code = `activity.part('PID', '${name}').${field}`;
      registerUse('PID', name);
      return [ code, Blockly.JavaScript.ORDER_NONE ];
    }
    registerPart({ name: 'PID' }, workspace);
  }

  // --------------------------------------------------------------------------
  // Kinematics
  // --------------------------------------------------------------------------
  function buildKinematicBlocks(workspace)
  {
    workspace.registerToolboxCategoryCallback('Kinematics', (workspace) => {
      const xml = [];
      xml.push(Blockly.Xml.textToDom(`<button web-class="${COLOR.KINEMATICS.BUTTON_CSS_CLASS}" text="Create Limb..." callbackKey="CALLBACK_IKChain"></button>`));
      const variables = workspace.getVariablesOfType('IKChain');
      if (variables.length)
      {
        xml.push(Blockly.Xml.textToDom('<sep gap="8"></sep>'));
        xml.push(Blockly.Xml.textToDom(`<block type="IKChain_CONFIG"><field variabletype="IKChain" name="NAME">${variables[0].name}</field></block>`));
        xml.push(Blockly.Xml.textToDom('<sep gap="8"></sep>'));
        xml.push(Blockly.Xml.textToDom(`<block type="IKChain_SET"><field variabletype="IKChain" name="NAME">${variables[0].name}</field></block>`));
        xml.push(Blockly.Xml.textToDom('<sep gap="8"></sep>'));
        xml.push(Blockly.Xml.textToDom(`<block type="IKChain_GET"><field variabletype="IKChain" name="NAME">${variables[0].name}</field><value name="INDEX"><block type="math_number"><field name="NUM">0</field></block></value></block>`));
        xml.push(Blockly.Xml.textToDom(`<block type="IKHingeBone"></block>`));
      }
      return xml;
    });

    workspace.registerButtonCallback('CALLBACK_IKChain', (button) => {
      Blockly.Variables.createVariable(button.getTargetWorkspace(), null, 'IKChain');
    });

    Blockly.Blocks['IKChain_CONFIG'] =
    {
      __category: 'Kinematics',
      __baseblock: true,
      init: function()
      {
        this.jsonInit(
        {
          message0: 'Configure Limb',
          message1: 'name %1',
          args1:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'IKChain',
              variableTypes: [ 'IKChain' ]
            }
          ],
          message2: 'bones %1',
          args2:
          [
            {
              type: 'input_statement',
              name: 'BONES'
            }
          ],
          colour: COLOR.KINEMATICS.CONFIG
        });
      }
    };
    Blockly.Blocks['IKChain_SET'] =
    {
      __category: 'Kinematics',
      init: function()
      {
        this.jsonInit(
        {
          message0: 'set limb %1 target to x %2 y %3 z %4',
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'IKChain',
              variableTypes: [ 'IKChain' ]
            },
            {
              type: 'input_value',
              name: 'X'
            },
            {
              type: 'input_value',
              name: 'Y'
            },
            {
              type: 'input_value',
              name: 'Z'
            },
          ],
          inputsInline: true,
          colour: COLOR.KINEMATICS.CHAIN,
          previousStatement: null,
          nextStatement: null
        });
      }
    };
    Blockly.Blocks['IKChain_GET'] =
    {
      __category: 'Kinematics',
      init: function()
      {
        this.jsonInit(
        {
          message0: 'bone %2 angle in limb %1',
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'IKChain',
              variableTypes: [ 'IKChain' ]
            },
            {
              type: 'input_value',
              name: 'INDEX'
            }
          ],
          colour: COLOR.KINEMATICS.CHAIN,
          output: null
        });
      }
    };
    Blockly.Blocks['IKChainError_GET'] =
    {
      __category: 'Kinematics',
      init: function()
      {
        this.jsonInit(
        {
          message0: 'error for limb %1',
          args0:
          [
            {
              type: 'field_variable',
              name: 'NAME',
              variable: 'Default',
              defaultType: 'IKChain',
              variableTypes: [ 'IKChain' ]
            }
          ],
          colour: COLOR.KINEMATICS.CHAIN,
          output: null
        });
      }
    };
    Blockly.JavaScript['IKChain_CONFIG'] = function(block)
    {
      const name = block.getField('NAME').getText();
      let bones = Blockly.JavaScript.statementToCode(block, 'BONES') || null;
      if (bones)
      {
        bones = bones.replace(/}{/g, '},{');
      }
      const code = `App.registerConfiguration(function(activity)
      {
        return activity.partConfig('IKChain', '${name}', { bones: [ ${bones} ] });
      });\n`;
      return code;
    };
    Blockly.JavaScript['IKChain_SET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const X = Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_NONE) || undefined;
      const Y = Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_NONE) || undefined;
      const Z = Blockly.JavaScript.valueToCode(block, 'Z', Blockly.JavaScript.ORDER_NONE) || undefined;
      const code = `activity.part('IKChain', '${name}', { target: { x: ${X}, y: ${Y}, z: ${Z} }});`;
      return code;
    };
    Blockly.JavaScript['IKChain_GET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const INDEX = Blockly.JavaScript.valueToCode(block, 'INDEX', Blockly.JavaScript.ORDER_NONE) || 0;
      const code = `activity.part('IKChain', '${name}', {}).angles[${INDEX}]`;
      return [ code, Blockly.ORDER_NONE ];
    };
    Blockly.JavaScript['IKChainError_GET'] = function(block)
    {
      const name = block.getField('NAME').getText();
      const code = `activity.part('IKChain', '${name}', {}).error`;
      return [ code, Blockly.ORDER_NONE ];
    };
  
    Blockly.Blocks['IKHingeBone'] =
    {
      __category: 'Kinematics',
      init: function()
      {
        this.jsonInit(
        {
          message0: 'Hinged Bone',
          message1: 'bone length %1 direction (%2,%3,%4)',
          args1:
          [
            {
              type: 'field_number',
              name: 'LENGTH',
              value: 1
            },
            {
              type: 'field_number',
              name: 'XDIR',
              value: 0
            },
            {
              type: 'field_number',
              name: 'YDIR',
              value: 0
            },
            {
              type: 'field_number',
              name: 'ZDIR',
              value: 1
            }
          ],
          message2: 'hinge axis (%1,%2,%3) limit cw %4 ccw %5',
          args2:
          [
            {
              type: 'field_number',
              name: 'XHINGE',
              value: 1
            },
            {
              type: 'field_number',
              name: 'YHINGE',
              value: 0
            },
            {
              type: 'field_number',
              name: 'ZHINGE',
              value: 0
            },
            {
              type: 'field_number',
              name: 'CW', // -ve
              value: -90
            },
            {
              type: 'field_number',
              name: 'CCW', // +ve
              value: 90
            },
          ],
          colour: COLOR.KINEMATICS.BONE,
          previousStatement: null,
          nextStatement: null
        });
      }
    };
    Blockly.JavaScript['IKHingeBone'] = function(block)
    {
      const bone =
      {
        type: 'HingeBone',
        length: parseFloat(block.getFieldValue('LENGTH')),
        direction: { x: parseFloat(block.getFieldValue('XDIR')), y: parseFloat(block.getFieldValue('YDIR')), z: parseFloat(block.getFieldValue('ZDIR')) },
        hingeAxis: { x: parseFloat(block.getFieldValue('XHINGE')), y: parseFloat(block.getFieldValue('YHINGE')), z: parseFloat(block.getFieldValue('ZHINGE')) },
        cwLimit: parseFloat(block.getFieldValue('CW')),
        ccwLimit: parseFloat(block.getFieldValue('CCW'))
      }
      return JSON.stringify(bone);
    };
  
  }

  // --------------------------------------------------------------------------
  // Visual Controls
  // --------------------------------------------------------------------------
  function registerOneControlEvent(id, name, friendlyName, schema)
  {
    Blockly.Blocks[id] =
    {
      __category: 'Controls',
      __subcategory: 'Event',
      __friendlyName: friendlyName,
      init: function()
      {
        this.jsonInit(
        {
          message0: `${friendlyName} value %1`,
          args0:
          [
            {
              type: 'field_dropdown',
              name: 'PROPERTY',
              options: Object.keys(schema).map((key) => [ key, key ])
            }
          ],
          output: null,
          colour: COLOR.CONTROL.EVENT
        });
      }
    };
    Blockly.JavaScript[id] = function(block)
    {
      const property = block.getFieldValue('PROPERTY');
      const code = `activity.get('/controller/${name}', '${property}')`;
      if (Blockly.JavaScript._currentActivity)
      {
        const topics = Blockly.JavaScript._topics[Blockly.JavaScript._currentActivity];
        if (!topics[name])
        {
          topics[name] = { name: `/controller/${name}` };
        }
      }
      return [ code, Blockly.JavaScript.ORDER_ADDITION ];
    }
  }

  function registerOneControlAction(id, name, properties)
  {
    let json =
    {
      message0: `set ${name}`,
      previousStatement: null,
      nextStatement: null,
      inputsInline: true,
      colour: COLOR.CONTROL.ACTION
    };
    let count = 0;
    for (let key in properties)
    {
      const tprop = typeof properties[key];
      switch (tprop) {
        case 'number':
        case 'boolean':
        case 'string':
          json[`args${count}`]= [
          {
            type: 'input_value',
            name: key,
            check: tprop.charAt(0).toUpperCase() + tprop.slice(1),
            align: 'RIGHT'
          }];
          break;
        default:
          break;
      }
      if (count === 0)
      {
        json.message0 = `for ${name} set ${key} to %1`;
      }
      else
      {
        json[`message${count}`] = `${key} to %1`;
      }
      count++;
    }
    if (count > 0)
    {
      Blockly.Blocks[id] =
      {
        __category: 'Controls',
        __friendlyName: name,
        init: function()
        {
          this.jsonInit(json);
        }
      }
      Blockly.JavaScript[id] = function(block)
      {
        let args = [];
        for (let key in json)
        {
          if (key.indexOf('args') === 0)
          {
            const name = json[key][0].name;
            switch (json[key][0].type)
            {
              case 'input_value':
                const value = Blockly.JavaScript.valueToCode(block, name, Blockly.JavaScript.ORDER_NONE);
                if (value)
                {
                  args.push(`${name}: ${value}`);
                }
                break;
              case 'field_dropdown':
                args.push(`${name}: '${block.getFieldValue(name)}'`);
                break;
              default:
                break;
            }
          }
        }
        const code = `activity.service('/controller/${name}', { async: true })({${args.join(', ')}});\n`;
        return code;
      }
    }
  }

  APP.registerComponent = function(workspace, component)
  {
    if (component.type.schemas.events) {
      const events = component.type.schemas.events;
      for (let topic in events) {
        registerOneControlEvent(`${component.id}/${topic}`, `${component.properties.name}/${topic}`, `${component.properties.name} ${topic}`, events[topic]);
      }
    }
    registerOneControlAction(`${component.id}`, `${component.properties.name}`, component.type.schemas.action || component.type.properties);
  }

  APP.unregisterComponent = function(workspace, component)
  {
    delete Blockly.Blocks[component.id];
    if (component.type.schemas.events) {
      const events = component.type.schemas.events;
      for (let topic in events) {
        delete Blockly.Blocks[`${component.id}/${topic}`];
      }
    }
  }

  // --------------------------------------------------------------------------
  // Toolbox
  // --------------------------------------------------------------------------

  function registerToolboxCategory(category, workspace)
  {
    workspace.registerToolboxCategoryCallback(category, (workspace) => {
      const xml = [];
      for (let name in Blockly.Blocks)
      {
        if (Blockly.Blocks[name].__category === category)
        {
          if ('__tool' in Blockly.Blocks[name])
          {
            const tool = Blockly.Blocks[name].__tool(workspace);
            if (tool)
            {
              xml.push(Blockly.Xml.textToDom(tool));
              xml.push(Blockly.Xml.textToDom('<sep gap="12"></sep>'));
            }
          }
          else
          {
            xml.push(Blockly.Xml.textToDom(`<block type="${name}"></block>`));
            xml.push(Blockly.Xml.textToDom('<sep gap="12"></sep>'));
          }
        }
      }
      return xml;
    });
  }

  // --------------------------------------------------------------------------
  // API to app container
  // --------------------------------------------------------------------------

  APP.deployRobotWorkspace = function(workspace)
  {
    const workspaceText = Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(workspace));

    Blockly.JavaScript._currentActivity = '';
    Blockly.JavaScript._topics = {};
    Blockly.JavaScript._parts = {};
    Blockly.JavaScript._timers = {};

    // Monkey-patch: we only want the activity and config blocks as roots for the code.
    const _getTopBlocks = workspace.getTopBlocks;
    workspace.getTopBlocks = function(ordered)
    {
      return _getTopBlocks.call(workspace, ordered).filter((block) => {
        return block.__baseblock;
      });
    }
    const code = Blockly.JavaScript.workspaceToCode(workspace);
    workspace.getTopBlocks = _getTopBlocks;
  
    const tcode = Object.keys(Blockly.JavaScript._topics).map((activity) => {
      return Object.values(Blockly.JavaScript._topics[activity]).map((topic) => {
        return `App.subscribeTopic('${activity}', '${topic.name}');`;
      }).join('');
    }).join('');
    const pcode = Object.keys(Blockly.JavaScript._parts).map((activity) => {
      return Object.values(Blockly.JavaScript._parts[activity]).map((part) => {
        return `App.subscribePart('${activity}', '${part.name}', '${part.instance}');`;
      }).join('');
    }).join('');
    const mcode = Object.keys(Blockly.JavaScript._timers).map((activity) => {
      return Object.values(Blockly.JavaScript._timers[activity]).map((timer) => {
        return `App.subscribeTimer('${activity}', '${timer.name}', ${timer.time});`;
      }).join('');
    }).join('');
    const jscode = code || tcode || pcode || mcode ? `${code};${tcode};${pcode};${mcode};App.run();` : '';

    console.log(jscode);
  
    const CONFIG = NODE.proxy({ service: '/app/config' });
    CONFIG({ source: workspaceText, code: jscode, ui: JSON.stringify(UI.serialized()) }).then(() => {
      NODE.unproxy({ service: '/app/config' });
    });
  }

  const LIST = NODE.proxy({ service: '/list' });

  APP.loadApp = function(workspace, app)
  {
    try
    {
      app = JSON.parse(app);
      const dom = Blockly.Xml.textToDom(app.source);
      workspace.clear();
      UI.clear();
      if (app.ui)
      {
        JSON.parse(app.ui).grid.components.forEach((component) => {
          UI.embed(component);
        });
      }
      // We load twice. The first time we setup any config hardware, then we reload once that's done.
      Blockly.Xml.appendDomToWorkspace(dom, workspace);
      setTimeout(() => {
        LIST({}).then((list) => {
          return Promise.all([
            buildConfigBlocks(sort(list.services.filter((service) => filter(service.name) && service.name.endsWith('/config') && service.schema ))),
            buildEventBlocks(sort(list.topics.filter((topic) => filter(topic.name) && topic.schema ))),
            buildActionBlocks(sort(list.services.filter((service) => filter(service.name) && !service.name.endsWith('/config') && service.schema ))),
            buildPartBlocks(workspace)
          ]);
        }).then(() => {
          workspace.clear();
          Blockly.Xml.appendDomToWorkspace(dom, workspace);
        });
      }, 0);
    }
    catch (e)
    {
      console.log('Invalid source: ', e);
    }
  }

  // --------------------------------------------------------------------------
  // Generate blocks
  // --------------------------------------------------------------------------

  function reloadWorkspace(workspace)
  {
    // Delete any blocks which no longer have any associated info.
    workspace.getAllBlocks().forEach((block) => {
      if (!Blockly.Blocks[block.type])
      {
        block.dispose(false);
      }
    });
    const dom = Blockly.Xml.workspaceToDom(workspace);
    workspace.clear();
    Blockly.Xml.appendDomToWorkspace(dom, workspace);
  }

  function rebuildEventsActionBlocksParts(workspace)
  {
    LIST({}).then((list) => {
      return Promise.all([
        buildEventBlocks(sort(list.topics.filter((topic) => filter(topic.name) && topic.schema ))),
        buildActionBlocks(sort(list.services.filter((service) => filter(service.name) && !service.name.endsWith('/config') && service.schema ))),
        buildPartBlocks(workspace)
      ]);
    }).then(() => {
      reloadWorkspace(workspace);
    });
  }

  APP.loadWorkspace = function(workspace)
  {
    // Load the app
    const CONFIG = NODE.proxy({ service: '/app/config' });
    let config = null;
    return CONFIG({}).then((c) => {
      NODE.unproxy({ service: '/app/config' });
      config = c;

      // Create any UI elements
      if (config.ui)
      {
        JSON.parse(config.ui).grid.components.forEach((component) => {
          UI.embed(component);
        });
      }

      // List the services
      return LIST({});
    }).then((list) => {
      // And create them
      return Promise.all([
        buildConfigBlocks(sort(list.services.filter((service) => filter(service.name) && service.name.endsWith('/config') && service.schema ))),
        buildEventBlocks(sort(list.topics.filter((topic) => filter(topic.name) && topic.schema ))),
        buildActionBlocks(sort(list.services.filter((service) => filter(service.name) && !service.name.endsWith('/config') && service.schema ))),
      ]);
    }).then(() => {
      buildProgramBlocks();
      buildPartBlocks(workspace);
      buildKinematicBlocks(workspace);
      registerToolboxCategory('Program', workspace);
      registerToolboxCategory('Config', workspace);
      registerToolboxCategory('Action', workspace);
      registerToolboxCategory('Event', workspace);
      registerToolboxCategory('Controls', workspace);
  
      // Finally create the program
      if (config.source)
      {
        workspace.clear();
        Blockly.Xml.appendDomToWorkspace(Blockly.Xml.textToDom(config.source), workspace);
      }
    });
  }

  APP.reloadWorkspace = reloadWorkspace;

  APP.getApp = function(workspace)
  {
    return JSON.stringify({
      source: Blockly.Xml.domToPrettyText(Blockly.Xml.workspaceToDom(workspace)),
      ui: JSON.stringify(UI.serialized())
    });
  }

});
 