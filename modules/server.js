'use strict';

console.info('Loading Server.');

const http = require('http');
const fs = require('fs');
const path = require('path');
const url = require('url');

const SERVICE_ADD_PAGE = { service: 'add_page', schema: { from: 'String', to: 'String', options: 'Hash' } };
const SERVICE_REMOVE_PAGE = { service: 'remove_page', schema: { from: 'String' } };

function file2type(file)
{
  switch (path.extname(file))
  {
    case '.html':
      return 'text/html; charset=utf-8';
    case '.js':
      return 'application/x-javascript';
    case '.png':
      return 'image/png';
    default:
      return undefined;
  }
}

function incoming(request, response)
{
  const url = request.url;
  if (url.indexOf('..') != -1)
  {
    response.writeHead(404);
    response.end();
    return;
  }
  let page = this._pages[url];
  switch (page ? page.type : 'unknown')
  {
    case 'file':
      const headers =
      {
        'Access-Control-Allow-Origin': '*'
      };
      const options = page.options;
      if (options.cache !== undefined)
      {
        if (options.cache === 0 || SIMULATOR)
        {
          headers['Cache-Control'] = 'no-cache, no-store, must-revalidate';
        }
        else
        {
          headers['Cache-Control'] = `max-age=${options.cache}`;
        }
      }
      if (options.contentType !== undefined)
      {
        headers['Content-Type'] = options.contentType;
      }
      let buffer = Buffer.alloc(0);
      page.to.forEach(function(to)
      {
        buffer = Buffer.concat([ buffer, fs.readFileSync(to) ]);
      });
      headers['Content-Length'] = buffer.length,
      response.writeHead(200, headers);
      response.write(buffer);
      response.end();
      break;

    case 'redirect':
      response.writeHead(302, { Location: page.to });
      response.end();
      break;

    case 'unknown':
    case 'directory':
    default:
      response.writeHead(404);
      response.end();
      break;
  }
}

function Server(config)
{
  this._name = config.name;
  this._node = Node.init(config.name);
  this._enabled = 0;
  this._port = config.port || 80;
  this._pages = {};
}

Server.prototype =
{
  enable: function()
  {
    if (this._enabled++ === 0)
    {
      let webserver = http.createServer(incoming.bind(this));
      webserver.listen(this._port);
      global.webserver = webserver;

      this._node.service(SERVICE_ADD_PAGE, (request) =>
      {
        const from = request.from;
        const to = request.to;
        const options = request.options;
        if (from in this._pages && this._pages[from] != to)
        {
          throw new Error(`Page mismatch: ${from}`);
        }
        try
        {
          if (typeof to === 'string')
          {
            if (fs.lstatSync(to).isDirectory())
            {
              const names = fs.readdirSync(to);
              names.forEach((name) => {
                this._pages[`${from}${name}`] = { type: 'file', to: [ `${to}${name}` ], options: Object.assign({ contentType: file2type(name) }, options) };
              });
              this._pages[from] = { type: 'directory', names: names };
            }
            else
            {
              fs.accessSync(to);
              this._pages[from] = { type: 'file', to: [ to ], options: Object.assign({ contentType: file2type(to) }, options) };
            }
          }
          else
          {
            this._pages[from] = { type: 'file', to: to, options: options };
          }
        }
        catch (_)
        {
          if (url.parse(to).protocol === 'http:')
          {
            this._pages[from] = { type: 'redirect', to: to, options: options };
          }
          else
          {
            console.warn(`Unknown page ${from} to ${to}.`);
          }
        }
        return true;
      });
      this._node.service(SERVICE_REMOVE_PAGE, (request) =>
      {
        const from = request.from;
        if (from in this._pages)
        {
          if (this._pages[from].type === 'directory')
          {
            this._pages[from].names.forEach((name) => {
              delete this._pages[`${from}${name}`];
            });
          }
          delete this._pages[from];
        }
        else
        {
          //console.warn(`Unknown page ${from}.`);
        }
        return true;
      });
    }
    return this;
  },
  
  disable: function()
  {
    if (--this._enabled === 0)
    {
      this._node.unservice(SERVICE_ADD_PAGE);
      this._node.unservice(SERVICE_REMOVE_PAGE);
      global.webserver = null;
    }
    return this;
  }
}

module.exports = Server;
